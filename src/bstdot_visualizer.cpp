#include "bstdot_visualizer.h"

#include <atomic>
#include <fstream>
#include <iosfwd>
#include <ostream>
#include <sstream>
#include <vector>

#include "FlagNodePtr.h"
#include "FlagNodePtr.hpp"
#include "IDType.h"

void
BSTDOTVisualizer::bstToList(std::set<ANodePtr>& inOutList, ANodePtr curr) {
	if (curr == nullptr)
		return;

	//insert into the output List
	inOutList.insert(curr);

	//observer new nodes
	//check if all nodes are alreade in the list
	std::vector<ANodePtr> nodesToBeChecked {
			curr->leftChild.load().get(),
			curr->rightChild.load().get(),
			curr->backLink.load(),
			curr->preLink.load()
	};
	for( const auto& it : nodesToBeChecked) {
		if(it == nullptr)
			continue;

		const bool nodeInTheList = inOutList.count(it);
		if (not nodeInTheList)
			bstToList(inOutList, it);
	}
}

std::string
BSTDOTVisualizer::getDOTNodeOutgoingOnly(ANodePtr node) {
	std::ostringstream outt;

	//create node
	outt << "\t" << node->k.getInt() << "\t";
	const bool isNegInfity = (node->k.getInt() == IDType::NEG_INFITY);
	const bool isPosInfity = (node->k.getInt() == IDType::POS_INFITY);
	if (isNegInfity)
		outt << "[label=\"-inf\"]";
	else if (isPosInfity)
		outt << "[label=\"inf\"]";
	outt << "; \n";


	auto flagsToString = [](FlagNodePtr<ANode_t>& fNode) {
		std::ostringstream flagsString;
		flagsString << fNode.isFlagged() << fNode.isMarked() << fNode.isThreaded();
		return flagsString.str();
	};
	//create links
	if (node->leftChild.load().get()) {
		FlagNodePtr<ANode_t> leftChild = node->leftChild.load();
		outt << "\t" << node->k.getInt() << "->" << leftChild.get()->k.getInt();
		outt << "\t [label=L" << flagsToString(leftChild) << "]; \n";
	}
	if (node->rightChild.load().get()) {
		FlagNodePtr<ANode_t> rightChild = node->rightChild.load();
		outt << "\t" << node->k.getInt() << "->" << rightChild.get()->k.getInt();
		outt << "\t [label=R" << flagsToString(rightChild) << "]; \n";
	}
	if (node->backLink.load()) {
		ANodePtr backLink = node->backLink.load();
		outt << "\t" << node->k.getInt() << "->" << backLink->k.getInt();
		outt << "\t [color=lightblue, label=Back]; \n";
	}
	if (node->preLink.load()) {
		ANodePtr preLink = node->preLink.load();
		outt << "\t" << node->k.getInt() << "->" << preLink->k.getInt();
		outt << "\t [color=lightblue, label=Pre]; \n";
	}
	return outt.str();
}

void BSTDOTVisualizer::readInBst(const ANodePtr bstRoot0) {
	BSTDOTVisualizer::bstToList(nodesList, bstRoot0);

	std::cout << "Found the nodes [";
	for( auto each = nodesList.rbegin(); each!=nodesList.rend(); ++each ) {
		std::cout << (*each)->k << ",";
	}
	std::cout << "]" << std::endl;
}


void BSTDOTVisualizer::exportToFile(const std::string pathAndFilename) const {
	if (nodesList.empty()) {
		std::cerr << "nodesList is empty, please use readInBst(..) first" << std::endl;
		return;
	}

	//generate
	std::ostringstream outt;
	outt << "digraph BST {";
	outt << "\t graph [ranksep=0.5]; \n";
	outt << "\t node [fontname=\"Arial\", shape=box]; \n";
	for( auto dqEl = nodesList.rbegin(); dqEl!=nodesList.rend(); ++dqEl ) {
		outt << BSTDOTVisualizer::getDOTNodeOutgoingOnly(*dqEl);
	}
	outt << "}";

	std::ofstream fs;
	fs.open(pathAndFilename);
	fs << outt.str() << std::endl;
	fs.close();
}
