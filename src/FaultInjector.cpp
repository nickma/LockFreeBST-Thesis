#include "FaultInjector.h"
#include "testHelper/optionalTraceF.h"

#include <algorithm>
#include <iterator>
#include <mutex>

//std::map<std::thread::id, int> FaultProducer::_threadsAliases;

void FaultProducer::enable() {
	std::cout << "Enabling FaultProducer" << std::endl;
	_enabled = true;
}

void FaultProducer::handle(const FaultPos& pos, const unsigned int threadAlias) const {
	if(!_enabled)
		return;

	//search for a matching FaultCase
	auto potentialFaultcases = std::find(_faultCases.begin(), _faultCases.end(), FaultCase(pos, threadAlias));
	if (potentialFaultcases == std::end(_faultCases)) {
		//no faultCase found
		return;
	}
	auto foundFaultCase = potentialFaultcases[0];

	_printFoundFaultCaseData(pos, threadAlias, foundFaultCase);

	std::this_thread::sleep_for(std::chrono::milliseconds(foundFaultCase.delayTimeMS));
}

void FaultProducer::handle(const FaultPos& pos) const {
	if(!_enabled)
		return;
	const int threadAlias = getThisThreadAlias();

	handle(pos, threadAlias);
}

void FaultProducer::_printFoundFaultCaseData(const FaultPos &pos, const int alias, const FaultCase &foundFaultCase) const {
	std::stringstream outt;
	outt << "FOUND thread aliased:" << alias
	<< ", state:" << pos
	<< ", injecting a delay..." << foundFaultCase.delayTimeMS << "[ms]";
	const std::string outt_str = outt.str();

	tracef(outt_str.c_str());
	std::cout << outt_str << std::endl;
}

void FaultProducer::registerThreadAlias(const std::thread::id &threadID, const unsigned int alias) {
	std::cout << "registering threadsID:" << threadID << " with alias:" << alias << std::endl;
	_threadsAliases[threadID] = alias;
}


void FaultProducer::addFaultCase(const FaultCase& faultCase) {
	auto result1 = std::find(_faultCases.begin(), _faultCases.end(), faultCase);
	if (result1 != std::end(_faultCases))
		throw std::invalid_argument("this faultCase is already registered");

	_faultCases.push_back(faultCase);
}

int FaultProducer::getThisThreadAlias() const {
	const auto this_id = std::this_thread::get_id();
	return _findThreadAlias(this_id);
}

int FaultProducer::_findThreadAlias(const std::thread::id &this_id) const {
	const auto searchAlias = _threadsAliases.find(this_id);
	if (searchAlias != _threadsAliases.end())
		return searchAlias->second;

	return -1;
}

int FaultProducer::getThreadAlias(std::thread::id id) const {
	return _findThreadAlias(id);
}

void FaultProducer::reset() {
	this->_enabled = false;
	this->_faultCases.clear();
	FaultProducer::_threadsAliases.clear();
}

FaultProducer::FaultProducer() {
	this->reset();
}
