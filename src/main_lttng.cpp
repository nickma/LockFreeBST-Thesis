#define TRACEPOINT_CREATE_PROBES
#define TRACEPOINT_DEFINE
#include "lttngHelper/hello_lttng.h"

#include <gtest/gtest.h>
#include <lttng/tracef.h>
#include <LttngLockFreeBST.h>

#include "lttngHelper/LttngUST_RAII.h"
#include "testHelper/ConcurrentExecuter.h"
#include "mockHelper/TestBaseClasses.h"
#include "testHelper/TraceF_RAII.h"
#include "bstdot_visualizer.h"


class LTTngInspection : public BasicEmptyBSTwithoutExpectation {
};

TEST_F(LTTngInspection, rmCat3Alot) {
    const int kToBeRemoved = 6;
    constexpr int threadCount = 20;

    for(int i=0; i<10000; ++i) {
        LttngLockFreeBST bst;

        this->preFillBST(bst);
        this->extractFilledNodePtr(bst);
        tracef("i:%d; Done adding nodes, starting with remove", i);

        ConcurrentExecuter cEx(bst, threadCount);
        cEx.everyoneRemovesSame({kToBeRemoved});

        //NodeNegInf
        ASSERT_EQ(3, this->nodeNegInf->rightChild.load().get()->k.getInt());
        //Node3
        ASSERT_EQ(neg_inf, node3->backLink.load()->k.getInt());
        ASSERT_EQ(2, this->node3->leftChild.load().get()->k.getInt());
        ASSERT_EQ(8, this->node3->rightChild.load().get()->k.getInt());
        ASSERT_EQ(nullptr, node3->preLink.load());

        //Node2
        ASSERT_EQ(3, node2->backLink.load()->k.getInt());
        ASSERT_EQ(1, this->node2->leftChild.load().get()->k.getInt());
        ASSERT_EQ(3, this->node2->rightChild.load().get()->k.getInt());
        ASSERT_EQ(nullptr, node2->preLink.load());

        //Node1
        ASSERT_EQ(2, node1->backLink.load()->k.getInt());
        ASSERT_EQ(1, this->node1->leftChild.load().get()->k.getInt());
        ASSERT_EQ(2, this->node1->rightChild.load().get()->k.getInt());
        ASSERT_EQ(nullptr, node1->preLink.load());

        //Node8
        ASSERT_EQ(3, node8->backLink.load()->k.getInt());
        ASSERT_EQ(7, this->node8->leftChild.load().get()->k.getInt());
        ASSERT_EQ(pos_inf, this->node8->rightChild.load().get()->k.getInt());
        ASSERT_EQ(nullptr, node8->preLink.load());

        //Node7
        ASSERT_EQ(8, node7->backLink.load()->k.getInt());
        ASSERT_EQ(7, this->node7->leftChild.load().get()->k.getInt());
        ASSERT_EQ(8, this->node7->rightChild.load().get()->k.getInt());
        ASSERT_EQ(nullptr, node7->preLink.load());

        //this->emptyBST(bst);
        tracef("i:%d; Done removing", i);
    }
}

TEST_F(LTTngInspection, rmCat2_removeALot) {
    const int kToBeRemoved = 2;
    const int threadCount = 20;

    for(int i = 0; i < 10000; ++i) {
        LttngLockFreeBST bst;
        this->preFillBST(bst);
        this->extractFilledNodePtr(bst);
        tracef("i:%d; Done adding nodes, starting with remove", i);

        ConcurrentExecuter cEx(bst, threadCount);
        cEx.everyoneRemovesSame({kToBeRemoved});
        EXPECT_FALSE(bst.contains(kToBeRemoved));

        //NodeNegInf
        EXPECT_EQ(6, this->nodeNegInf->rightChild.load().get()->k.getInt());
        //Node6
        EXPECT_EQ(neg_inf, node6->backLink.load()->k.getInt());
        EXPECT_EQ(1, this->node6->leftChild.load().get()->k.getInt());
        EXPECT_EQ(8, this->node6->rightChild.load().get()->k.getInt());
        EXPECT_EQ(nullptr, node6->preLink.load());    //NodeNegInf
        //Node1
        EXPECT_EQ(6, node1->backLink.load()->k.getInt());
        EXPECT_EQ(1, this->node1->leftChild.load().get()->k.getInt());
        EXPECT_EQ(3, this->node1->rightChild.load().get()->k.getInt());
        EXPECT_EQ(nullptr, node1->preLink.load());
        //Node3
        EXPECT_EQ(1, node3->backLink.load()->k.getInt()); //this failed in the initial version
        EXPECT_EQ(3, this->node3->leftChild.load().get()->k.getInt());
        EXPECT_EQ(6, this->node3->rightChild.load().get()->k.getInt());
        EXPECT_EQ(nullptr, node3->preLink.load());

        //this->emptyBST(bst);
        tracef("i:%d; Done removing", i);
    }
}


TEST_F(LTTngInspection, rmCat2_delayed) {
    const int kToBeRemoved = 2;
    const int threadCount = 2;

    for(int i = 0; i < 300; ++i) {
        LttngLockFreeBST bst;
        this->preFillBST(bst);
        this->extractFilledNodePtr(bst);
        tracef("i:%d; Done adding nodes, starting with remove", i);


        const unsigned int unifiedBase = 50;
        const unsigned int delay1 = i % unifiedBase;
        const unsigned int delay2 = unifiedBase - delay1;
        bst.fi.enable();
        bst.fi.addFaultCase({{CASUId::FLAG_CHILDLNK, CASPrefix::PRE_CAS}, 0, delay1});
        bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 1, delay2});

        ConcurrentExecuter cEx(bst, threadCount);
        cEx.everyoneRemovesSame({kToBeRemoved}); //cat2 remove
        ASSERT_FALSE(bst.contains(kToBeRemoved));
    }
}

TEST_F(LTTngInspection, cat3_interrupSinglePosition) {
    std::vector<CASUId> interruptiblePosition {
            CASUId::FLAG_CHILDLNK, //OK
            CASUId::CLEANFLAG_THR_PRELINK,
            CASUId::CLEANFLAG_THR_RIGHT, //Hängt manchmal bei CLEANFLAG_UTHR_V
            CASUId::CM_FLAG_PRES_PARENT,
            CASUId::CLEANFLAG_UTHR_V,
            CASUId::TRYMARK
    };

    for(int i = 0; i < 100; ++i) {
        for (auto &eachPosition: interruptiblePosition) {
            std::cout << "Interrupting single postion:" << eachPosition << std::endl;
            LttngLockFreeBST bst;
            this->preFillBST(bst);
            this->extractFilledNodePtr(bst);

            constexpr int delayBaseMS = 30;
            constexpr int thread1 = 0;
            constexpr int threadM = 1;
            bst.fi.enable();
            bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, threadM, delayBaseMS});
            bst.fi.addFaultCase({{eachPosition, CASPrefix::PRE_CAS}, thread1, 2 * delayBaseMS});
            bst.fi.addFaultCase({{eachPosition, CASPrefix::POST_CAS}, threadM, 3 * delayBaseMS}); //65535});

            constexpr int threadCount = 2;
            ConcurrentExecuter cEx(bst, threadCount);
            cEx.everyoneRemovesSame({6});
            EXPECT_FALSE(bst.contains(6));

            std::cout << "done with position:" << eachPosition << std::endl;
            //this->emptyBST(bst);
        }
    }
}

TEST_F(LTTngInspection, rmMixed) {
    size_t threadCount = 30; //containAfterSetup.size();

    for(int i = 0; i < 300; ++i) {
        LttngLockFreeBST bst;
        this->preFillBST(bst);
        this->extractFilledNodePtr(bst);
        tracef("i:%d; Done adding nodes, starting with remove", i);

        ConcurrentExecuter cEx(bst, threadCount);
        std::vector<TimedSingleOpResult> resultStore;
        resultStore = cEx.everyoneRemovesSame(containAfterSetup);

        tracef("removing done, resultStore contains %u elements", resultStore.size());
        for(auto& each : resultStore) {
            if (each.exception) {
                std::rethrow_exception(each.exception);
            }
        }

        ASSERT_FALSE(bst.contains(6));
        ASSERT_FALSE(bst.contains(2));
        ASSERT_FALSE(bst.contains(1));
        ASSERT_FALSE(bst.contains(3));
        ASSERT_FALSE(bst.contains(8));
        ASSERT_FALSE(bst.contains(7));
    }
}

TEST_F(LTTngInspection, rm_tryFlagEndless) {

    //for(int i = 0; i < 300; ++i) {
    const int i=0;
        LttngLockFreeBST bst;
        this->preFillBST(bst);
        this->extractFilledNodePtr(bst);
        tracef("i:%d; Done adding nodes, starting with remove", i);

        constexpr int thread0 = 0;
        constexpr int thread1 = 1;
        constexpr int delayBaseMS = 100;
        bst.fi.enable();
        bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, thread1, 1 * delayBaseMS});
        bst.fi.addFaultCase({{CASUId::CLEANFLAG_UTHR_V, CASPrefix::POST_CAS}, thread0,  2* delayBaseMS});
        bst.fi.addFaultCase({{CASUId::TRYFLAG_REMOVE_I, CASPrefix::POST_CAS}, thread1,  3* delayBaseMS});
        bst.fi.addFaultCase({{CASUId::TRYMARK_VII, CASPrefix::POST_CAS}, thread0,  4* delayBaseMS});
        bst.fi.addFaultCase({{CASUId::TRYMARK_MARKED_POS, CASPrefix::PRE_CAS}, thread1,  5* delayBaseMS});
        bst.fi.addFaultCase({{CASUId::CLRMARK_O_XII, CASPrefix::POST_CAS}, thread0,  6* delayBaseMS});

        const int threadCount = 2;
        ConcurrentExecuter cEx(bst, threadCount);
        std::vector<TimedSingleOpResult> resultStore;
        resultStore = cEx.everyoneRemovesSame({6});

        tracef("removing done, resultStore contains %u elements", resultStore.size());
    //}
}

TEST_F(LTTngInspection, lemma10_a_flagged) {
    LttngLockFreeBST bst;
    this->preFillGivenValues(bst, {6,8});

    const unsigned int delayBase = 200;
    bst.fi.enable();
    bst.fi.addFaultCase({{CASUId::CLEANMARK_RIGHT_V, CASPrefix::POST_CAS}, 0, delayBase*2});
    bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 1, delayBase*1});

    const int threadCount = 2;
    ConcurrentExecuter cEx(bst, threadCount);
    std::vector<std::vector<IDType>> kToBeRemoved {{8},{6}};
    auto resultStore = cEx.threadsRemoveEveryoneDifferentValues(kToBeRemoved);

    for (auto &element: resultStore) {
        std::cout << element << std::endl;
    }

    ASSERT_FALSE(bst.contains(6));
    ASSERT_FALSE(bst.contains(8));
}

TEST_F(LTTngInspection, example_rm_cat3) {
    LttngLockFreeBST bst;
    this->preFillGivenValues(bst, {6,2,8,1,3,7});

    const int threadCount = 1;
    ConcurrentExecuter cEx(bst, threadCount);
    std::vector<std::vector<IDType>> kToBeRemoved {{6}};
    cEx.threadsRemoveEveryoneDifferentValues(kToBeRemoved);

    ASSERT_FALSE(bst.contains(6));
}

TEST_F(LTTngInspection, lemma10_a_marked) {
    LttngLockFreeBST bst;
    this->preFillGivenValues(bst, {6,8});
    //BSTDOTVisualizer visualizer;
    //visualizer.readInBst(bst.getRoot());
    //visualizer.exportToFile("/tmp/dotVisualizerTest.dot");

    const unsigned int delayBase = 200;
    bst.fi.enable();
    bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 0, delayBase*1});
    bst.fi.addFaultCase({{CASUId::CLEANFLAG_THR_RIGHT, CASPrefix::POST_CAS}, 1, delayBase*2});

    const int threadCount = 2;
    ConcurrentExecuter cEx(bst, threadCount);
    std::vector<std::vector<IDType>> kToBeRemoved {{8},{6}};
    auto resultStore = cEx.threadsRemoveEveryoneDifferentValues(kToBeRemoved);

    for (auto &element: resultStore) {
        std::cout << element << std::endl;
    }

    ASSERT_FALSE(bst.contains(6));
    ASSERT_FALSE(bst.contains(8));
}

TEST_F(LTTngInspection, lemma10_b) {
    LttngLockFreeBST bst;
    this->preFillGivenValues(bst, {6,2});

    const unsigned int delayBase = 200;
    bst.fi.enable();
    bst.fi.addFaultCase({{CASUId::FLAG_CHILDLNK, CASPrefix::POST_CAS}, 0, delayBase*2});
    bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 1, delayBase*1});

    const int threadCount = 2;
    ConcurrentExecuter cEx(bst, threadCount);
    std::vector<std::vector<IDType>> kToBeRemoved {{6},{2}};
    auto resultStore = cEx.threadsRemoveEveryoneDifferentValues(kToBeRemoved);

    for (auto &element: resultStore) {
        std::cout << element << std::endl;
    }

    ASSERT_FALSE(bst.contains(6));
    ASSERT_FALSE(bst.contains(2));
}

TEST_F(LTTngInspection, lemma10_c) {
    LttngLockFreeBST bst;
    this->preFillGivenValues(bst, {6,2,4,3});

    const unsigned int delayBase = 200;
    bst.fi.enable();
    bst.fi.addFaultCase({{CASUId::CLEANFLAG_UTHR_V,  CASPrefix::POST_CAS}, 0, delayBase*2});
    bst.fi.addFaultCase({{CASUId::CLEANMARK_RIGHT_V, CASPrefix::POST_CAS}, 0, delayBase*2});
    bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 1, delayBase*1});

    const int threadCount = 2;
    ConcurrentExecuter cEx(bst, threadCount);
    std::vector<std::vector<IDType>> kToBeRemoved {{3},{6}};
    auto resultStore = cEx.threadsRemoveEveryoneDifferentValues(kToBeRemoved);

    for (auto &element: resultStore) {
        std::cout << element << std::endl;
    }

    ASSERT_FALSE(bst.contains(3));
    ASSERT_FALSE(bst.contains(6));
    ASSERT_TRUE(bst.contains(2));
    ASSERT_TRUE(bst.contains(4));
}

int main(int argc, char **argv) {
    auto setupLTTNG = LttngUST_RAII();

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}