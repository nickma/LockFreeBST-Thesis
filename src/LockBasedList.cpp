#include "LockBasedList.h"

#include <algorithm>

bool LockBasedList::contains(const IDType k) {
    int x = k.getInt();

    std::lock_guard<std::mutex> guard(container_access_mutex);
    if(_container.count(x))
        return true;
    else
        return false;
}

bool LockBasedList::remove(const IDType k) {
    throw std::runtime_error("not impleented");
}

/**
 * performs a high level add on
 * the instances BST
 * @return whether the element with IDType k
 * 			has been added successfully or not
 */
bool LockBasedList::add(const IDType k) {
    int newElement = k.getInt();

    std::lock_guard<std::mutex> guard(container_access_mutex);
    auto result_2 = _container.insert(newElement);
    if (result_2.second)
        return true;
    else
        return false;
}


void* LockBasedList::getRoot() {
    std::cerr << "called getRoot on a LockBasedList" << std::endl;
    return nullptr;
}