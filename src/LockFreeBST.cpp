#include "LockFreeBST.h"

#include <atomic>
#include <iostream>
#include <stdexcept>
#include <memory>

#include "FlagNodePtr.h"
#include "FlagNodePtr.hpp"

LockFreeBST::~LockFreeBST() {};
LockFreeBST::LockFreeBST() {};

ANodePtr LockFreeBST::getRoot() {
	return &root[0];
}

bool LockFreeBST::contains(const IDType k) {
	ANodePtr prev = &root[1];
	ANodePtr curr = &root[0];
	const cmpRC_e dir = locate(prev, curr, k);
	if (dir == cmpRC_e::EQUAL)
		return true;
	return false;
}

bool LockFreeBST::remove(const IDType k) {
	tracef("Starting remove of k:%d thr[alias:%d]", k.getInt(), fi.getThisThreadAlias());
	ANodePtr prev = &root[1];
	ANodePtr curr = &root[0];
	/**
	 * locate the order-link
	 * If locate(..) terminates, then successor of k_curr is the
	 * desired node to remove if k matches with its key
	 */
	const cmpRC_e dir = locate(prev, curr, {k, NEGATIVE});
	auto nonAtomicNodePtr = curr->selectLRChild(dir).load();
	ANodePtr next = nonAtomicNodePtr.get();

	bool result = false;
	if (k != next->k) {
		// suc(x(k_curr)) [aka next] is *not* the desired node to be removed
		return false;
	} else {
		/** Paper section 3.2.2
		 * suc(x(k_curr)) is the right node to remove if
		 *   k matches with the searched key k.
		 * suc(x(k_curr)) is the node pointed by the threaded
		 *   right-link of x(k_curr) and this link is indeed
		 *   the order-link of x(k)
		 */
		// (I) flag the incoming order-link
		result = tryFlag(curr, next, prev, true);
		fi.handle({CASUId::TRYFLAG_REMOVE_I, CASPrefix::POST_CAS});

		// REMOVE checks whether the target node is still there [p.326,s.3.2.2]
		//		  first thing being cas'd is the prev->child
			//const auto dir = cmp(curr->k, prev->k); //TODO check if it's paper conform, anyway it seems to be good
			//ANodePtr lhs = prev->selectLRChild(dir).load().get();
			//const bool targetNodeStillThere = (lhs == curr);
		const bool targetNodeStillThere = cmp(curr->selectLRChild(dir).load().get()->k, k) == cmpRC_e::EQUAL;
		if (targetNodeStillThere) {
			cleanFlag(curr, next, prev, true);
		}
	}
	return result;
}



bool LockFreeBST::add(const IDType k) {
	ANodePtr prev = &root[1];
	ANodePtr curr = &root[0];
	// Initializing a new node with supplied key and left-link
	// 	threaded and pointing to itself
	std::unique_ptr<ANode_t> node;
	node.reset(new ANode_t{k, {nullptr,0,0,1}, {nullptr,0,0,1}, &root[1], nullptr});
	node->leftChild = FlagNodePtr<ANode_t>(node.get(), 0, 0, 1);

	CONTENTION_WHILE_COUNTER (true) {
		//locate the interval [k_i, k_j] that k belongs to, associated
		//  with the threaded link.
		const cmpRC_e dir = locate(prev, curr, k);
		if (dir == cmpRC_e::EQUAL)
			return false;

		auto nonAtomicNodePtr = curr->selectLRChild(dir).load(); //atomic load
		ANodePtr r = nonAtomicNodePtr.get(); //!< link to which the new node must be connected to
		//The located link is threaded. Set the right-link
		// of the adding node to copy this value
		cas(node->rightChild, node->rightChild.load(), {r,0,0,1}, node->k, CASUId::ADD_RIGHT);
		cas(node->backLink, node->backLink, curr, node->k, CASUId::ADD_BACK_LNK); //the paper says: curr;

		// Try inserting the new node
		bool result = cas(curr->selectLRChild(dir), {r,0,0,1}, {node.get(),0,0,0}, curr->k, CASUId::ADD_CHILD);
		if (result) {
			// release the unique_ptr from its duty of freeing the memory on exit
			node.release();
			return true;
		} else {
			/* If the CAS fails, check if the link has been
			 * marked, flagged or a new node has been inserted.
			 * If marked or flagged, first help. */
			auto nonAtomicNodePtr = curr->selectLRChild(dir).load(); //atomic load
			ANodePtr newR = nonAtomicNodePtr.get();
			const bool f = nonAtomicNodePtr.isFlagged();
			const bool m = nonAtomicNodePtr.isMarked();
			if (newR == r) {
				auto newCurr = prev;
				if (m) {
					cleanMark(curr, dir);
				} else if (f) {
					cleanFlag(curr, r, prev, true);
				}
				curr = newCurr;
				prev = newCurr->backLink.load(); //atomic load
			}
		}
	}

	return false;
}
