/*
 * FaultCase.cc
 *
 *  Created on: Jan 29, 2016
 *      Author: nickma
 */

#include "FaultCase.h"

//FaultCase::FaultCase(
//		const std::function<bool(const CASPosition& pos)> &posFilter,
//		const int& threadAlias) :
//		_positionCondition(posFilter), threadAlias(threadAlias) {
//}
FaultCase::FaultCase(const FaultPos& pos, const int& threadAlias) :
		positionFilter(pos), threadAlias(threadAlias) { }
FaultCase::FaultCase(const FaultPos& pos, const int& threadAlias, const unsigned int delayTimeMS) :
		positionFilter(pos), threadAlias(threadAlias), delayTimeMS(delayTimeMS) { }

bool operator==(const FaultCase& lhs, const FaultCase& rhs) {
	const bool faultPositionsAreEqual = (lhs.positionFilter == rhs.positionFilter);

	return (faultPositionsAreEqual && (lhs.threadAlias == rhs.threadAlias));
}


