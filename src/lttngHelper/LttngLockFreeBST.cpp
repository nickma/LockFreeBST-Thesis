#include "LttngLockFreeBST.h"

#include <iomanip>

#include "hello_lttng.h"

bool LttngLockFreeBST::cas(std::atomic<FlagNodePtr<ANode_t>>& casObject,
		FlagNodePtr<ANode_t> expected,
		FlagNodePtr<ANode_t> desired,
		const IDType srcNodeID,
		const CASUId& position)
{
	FlagNodePtr<ANode_t> originalObject = casObject.load();
	FlagNodePtr<ANode_t> originalExpected = expected;

	//do not take the original LockFreeBST::cas, because it delays the tracer
	//  output in case of a CASPrefix::POST_CAS fault-injection
	//  bool rc = LockFreeBST::cas(casObject, expected, desired, srcNodeID, position);
	fi.handle({position, CASPrefix::PRE_CAS});
	const bool rc = casObject.compare_exchange_strong( expected, desired );

    std::stringstream streamPosition; streamPosition << position;
    std::stringstream casData;
    casData << "[was:" << originalObject
            << ", is:" << casObject.load()
            << ", expected:" << originalExpected
            << ", desired:" << desired << "]";
    tracepoint(tpProvider, casLog,
               streamPosition.str().c_str(),
               rc,
               fi.getThisThreadAlias(),
               srcNodeID.getInt(),
               casData.str().c_str()
    );

	fi.handle({position, CASPrefix::POST_CAS});

	return rc;
}

bool LttngLockFreeBST::cas(std::atomic<ANodePtr>& casObject,
		ANodePtr expected,
		ANodePtr desired,
		const IDType srcNodeID,
		const CASUId& position)
{
	ANodePtr originalObject = casObject.load();

	//do not take the original LockFreeBST::cas, because it delays the tracer
	//  output in case of a CASPrefix::POST_CAS fault-injection
    //bool rc = LockFreeBST::cas(casObject, expected, desired, srcNodeID, position);
	fi.handle({position, CASPrefix::PRE_CAS});
	const bool rc = casObject.compare_exchange_strong( expected, desired );

    std::stringstream streamPosition; streamPosition << position;
    std::stringstream casData;
    casData << "[was:" << std::setw(5) << originalObject->k
            << ", is:" << std::setw(5) << *casObject.load()
            << ", expected:" << std::setw(5)  <<  *expected
            << ", desired:"  << std::setw(5) << *desired << "]";
    tracepoint(tpProvider, casLog,
               streamPosition.str().c_str(),
               rc,
               fi.getThisThreadAlias(),
               srcNodeID.getInt(),
               casData.str().c_str()
    );

	fi.handle({position, CASPrefix::POST_CAS});
	
	return rc;
}

