#!/usr/bin/python3
from datetime import datetime
from collections import Counter
import babeltrace
import sys
import logging



import os
from enum import Enum

# gets the youngest trade folder out of the lttng-trace root directory
class LttngTraceFolderIdentifyer:
	class FolderIdentity(Enum):
		LTTNG_ROOT = 0
		LTTNG_TRACE = 1
		UNKNOWN = -1

	folderIdentity = FolderIdentity.UNKNOWN
	userPathGiven = ''

	def _resolveHomeFolder(self, folderPath):
		homePath = os.path.expanduser('~')
		folderPath = folderPath.replace('~', homePath)
		return folderPath

	def _identifyFoldersLTTNGContent(self, folderPath):
		self.folderIdentity = self.FolderIdentity.UNKNOWN
		if not os.path.isdir(folderPath):
			pass
		
		elif ('index' in os.listdir(folderPath)):
			self.folderIdentity = self.FolderIdentity.LTTNG_TRACE
		else:
			folderContent = os.listdir(folderPath)
			for each in folderContent:
				hasTwoMinuses = each.count('-') == 2
				lastPartDigitsOnly = each.split('-')[-1].isdigit()

				if hasTwoMinuses and lastPartDigitsOnly:
					self.folderIdentity = self.FolderIdentity.LTTNG_ROOT
					break
		
		logging.debug('folderID: %s', self.folderIdentity)
		return self.folderIdentity

	def _getYoungesTraceFolderOfPath(self,
				absoluteFolderPath = '/home/nickma/lttng-traces'):
		returnFolder = ''

		if (self.folderIdentity == self.FolderIdentity.LTTNG_ROOT):
			folderContent = os.listdir(absoluteFolderPath)
			filteredFolderContent = [folder for folder in folderContent if folder.count('-')==2]

			filteredFolderContent.sort(reverse=True)
			logging.debug('filtered folder content: %s', filteredFolderContent)
			returnFolder = filteredFolderContent[0]

		logging.debug('youngestSubFolder: %s', returnFolder)
		return returnFolder


	def _getOneLTTngTraceFileSubDir(self):
		absoluteFolderPath = self._resolveHomeFolder(self.userPathGiven)
		self._identifyFoldersLTTNGContent(absoluteFolderPath)

		# throw if unknown content
		contentUnknown = (self.folderIdentity == self.FolderIdentity.UNKNOWN)
		if contentUnknown:
			logging.error('Neither an lttng "index" folder, nor any folder with a date in it found')
			raise Exception('following given folder has unknown content: ' + self.userPathGiven)

		# select a folder
		youngestSubFolder = self._getYoungesTraceFolderOfPath(absoluteFolderPath)
		
		return youngestSubFolder
		

	# @var[in] userPathGiven: either the trace root path: '~/lttng-trace' or 
	#			 the full path '~/lttng-traces/myConsoleSession-20050101-133700/./ust/uid/1000/64-bit'
	def getAbsoluteFolder(self, 
				userPathGiven,
				postSubfolderPath = './ust/uid/1000/64-bit'):
		self.userPathGiven = userPathGiven

		folderPrefix = self._resolveHomeFolder(userPathGiven)
		folderSub = self._getOneLTTngTraceFileSubDir()
		
		resultingPath = folderPrefix + '/' + folderSub
		if (self.folderIdentity == self.FolderIdentity.LTTNG_ROOT):
			resultingPath += '/' +postSubfolderPath
		
		return resultingPath
		

def tracetoStdOut(folder):
	## See: http://diamon.org/babeltrace/docs/python/examples/#reader-api-examples

	# a trace collection holds one or more traces
	col = babeltrace.TraceCollection()

	# add the trace provided by the user (first command line argument)
	# (LTTng traces always have the 'ctf' format)
	if col.add_trace(folder, 'ctf') is None:
	    raise RuntimeError('Cannot add trace')

	# this counter dict will hold execution times:
	#
	#   task command name -> total execution time (ns)
	exec_times = Counter()

	# this holds the last `sched_switch` timestamp
	last_ts = None

    # iterate on events
	for event in col.events:
		#print(', '.join(event.keys())) #print all keys

		#extract human readable timestamp
		readableTime = ''
		if 'timestamp_begin' in event:
			tmpTime = datetime.fromtimestamp(event.timestamp // 1000000000)
			readableTime = tmpTime.strftime('%H:%M:%S')
			readableTime += '.' + str(int(event.timestamp % 1000000000)).zfill(9)


		#for a fprint: timestamp_begin, pthread_id, msg
		#for a cas: rc, casData, timestamp_begin, position, pthread_id, threadAlias, k
		if 'tpProvider' in event.name:
			readableRC = ' true' if (event['rc']==1) else 'false'

			print("[%s] CAS: (:%d)[k:%d]%s=%s, pos=%s, thr[alias:%d, id:%d]" %
				(readableTime, event['threadAlias'],
					event['k'], readableRC, event['casData'], event['position'],
					event['threadAlias'], event['pthread_id']
				))
			
			continue

		if 'tracef' in event.name:
			print("[%s] Msg: %s" % (readableTime, event['msg']))

			continue

def main():
	logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
	logging.info('Main is up')

	
	userPathGiven = sys.argv[1]

	traceFolderfinder = LttngTraceFolderIdentifyer()
	lttngTracefileTargetFolder = traceFolderfinder.getAbsoluteFolder(userPathGiven)
	logging.info('Found trace folder: %s', lttngTracefileTargetFolder)

	tracetoStdOut(lttngTracefileTargetFolder)

if __name__ == "__main__":
    main()

