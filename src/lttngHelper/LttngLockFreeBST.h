#ifndef SRC_TESTHELPER_MOCKEDLOCKFREEBST_H_
#define SRC_TESTHELPER_MOCKEDLOCKFREEBST_H_

#include <iostream>

#include "../ANode_s.h"
#include "../FaultPos.h"
#include "../FlagNodePtr.h"
#include "../IDType.h"
#include "../LockFreeBST.h"


/**
 * This child of LockFreeBST has additional lttng/tracepoint(...) calls
 * implemented in its CAS operation.
 *
 * @requires -llttng-ust -ldl
 */
class LttngLockFreeBST : public LockFreeBST {
public:
	/**
	 * Mocking CAS wrapper
	 */
	virtual bool cas(std::atomic<FlagNodePtr<ANode_t>>& casObject,
				FlagNodePtr<ANode_t> expected,
				FlagNodePtr<ANode_t> desired,
				const IDType srcNodeID,
				const CASUId& position);


	virtual bool cas(std::atomic<ANodePtr>& casObject,
			ANodePtr expected,
			ANodePtr desired,
			const IDType srcNodeID,
			const CASUId& position);
};



#endif /* SRC_TESTHELPER_MOCKEDLOCKFREEBST_H_ */
