//============================================================================
// Name        : efficient_LF_BST.cpp
// Author      : Nick Ma
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stddef.h>
#include <atomic>
#include <exception>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

#include "ANode_s.h"
#include "bstdot_visualizer.h"
#include "greatest/greatest.h"
#include "LockFreeBST.h"
#include "testHelper/ConcurrentExecuter.h"

using namespace std;

node_t testingRoot[2] = {
	{IDType::NEG_INFITY, {&testingRoot[0], 0,0,1}, {&testingRoot[1], 0,0,1}, &testingRoot[1], nullptr},
	{IDType::POS_INFITY, {&testingRoot[0], 0,0,0}, {nullptr , 0,0,1}, nullptr , nullptr}
};
TEST basic_node_t_capabilites() {
	ASSERT_EQ(&testingRoot[0], testingRoot[0].leftChild.get());
	ASSERT_EQ(false, testingRoot[0].leftChild.isFlagged());
	ASSERT_EQ(true,  testingRoot[0].leftChild.isThreaded());
	PASS();
}


TEST basic_flagNodePtr() {
	FlagNodePtr<node_t> nodePtrTest(&testingRoot[0], 0,0,0);
	testingRoot[0].k = 42;
	testingRoot[1].k = 0;
	ASSERT_EQ(42, testingRoot[0].k.getInt());

	ASSERT_EQ(cmpRC_e::LESS,	cmp(testingRoot[1].k, testingRoot[0].k)); //root1.k is less than root0.k
	ASSERT_EQ(cmpRC_e::GREATER,	cmp(testingRoot[0].k, testingRoot[1].k)); //root0.k is greater than root1.k
	ASSERT_EQ(cmpRC_e::EQUAL,	cmp(testingRoot[0].k, testingRoot[0].k)); //root0.k is equal to root0.k

	//assure that the size doesn't grow
	ASSERT_EQ(sizeof(void*), sizeof(FlagNodePtr<node_t>(nullptr,0,0,0)));
	PASS();
}

TEST cas_flagNodePtr_test() {
	std::atomic<FlagNodePtr<node_t>> bar{{nullptr, true,false,false}};
	//ASSERT_EQ(true, bar.is_lock_free());
	ASSERT_EQ(false, bar.load().isThreaded());

	// manipulate bar via a CAS
	// therefor: 1. copy 'bar',
	//			 2. manipulate the copy
	//			 3. execute the CAS
	FlagNodePtr<node_t> barExpected, barCopy;
	barExpected = bar.load();
	barCopy.reset(nullptr, true, true, true);
	bool casRC = bar.compare_exchange_strong(barExpected, barCopy);
	ASSERT_EQ(true, casRC);

	ASSERT_EQ(true, bar.load().isFlagged());
	ASSERT_EQ(true, bar.load().isMarked());
	ASSERT_EQ(true, bar.load().isThreaded());
	PASS();
}

//TEST node_t_constrainsts() {
//	const size_t wordSize = sizeof(void*);
//	std::cout << "WordSize:" << wordSize << ", sizeof(node_t):" << sizeof(node_t) << std::endl;
//			ASSERT_EQ(5*wordSize, sizeof(node_t));
//			PASS();
//}
//TEST aNode_t_constrainsts() {
//	const size_t wordSize = sizeof(void*);
//	ASSERT_EQ(5*wordSize, sizeof(ANode_t));
//	PASS();
//}

TEST idType_constraints() {
	IDType k3{3};
	IDType k4{4};
	ASSERT(k3 < k4);
	ASSERT_FALSE(k4 < k3);

	EpsilonedIDType ek3{k3, NEGATIVE};
	EpsilonedIDType ek4{k4, NEGATIVE};
	ASSERT(ek3 < ek4);
	ASSERT_FALSE(ek4 < ek3);
	ASSERT(ek3 < k4);

	ASSERT_EQ(cmpRC_e::LESS,	cmp(ek3,k3));
	ASSERT_EQ(cmpRC_e::GREATER,	cmp(EpsilonedIDType(k3, POSITIVE), k3));
	ASSERT_EQ(cmpRC_e::LESS,	cmp(ek3, EpsilonedIDType(k3, NEGATIVE)));
	ASSERT(EpsilonedIDType(k3, NEGATIVE) < EpsilonedIDType(k3, POSITIVE));
	PASS();
}

ANode_t aTestingRoot[2] = {
	{IDType::NEG_INFITY, {&aTestingRoot[0], 0,0,1}, {&aTestingRoot[1], 0,0,1}, &aTestingRoot[1], nullptr},
	{IDType::POS_INFITY, {&aTestingRoot[0], 0,0,0}, {nullptr , 0,0,1}, nullptr , nullptr}
};
TEST basic_ANode_t_capabilites() {
	ASSERT_EQ(&aTestingRoot[0], aTestingRoot[0].leftChild.load().get());
	ASSERT_EQ(false, aTestingRoot[0].leftChild.load().isFlagged());
	ASSERT_EQ(true,  aTestingRoot[0].leftChild.load().isThreaded());

	PASS();
}

SUITE(bst_environment) {
	RUN_TEST(basic_node_t_capabilites);
//	RUN_TEST(node_t_constrainsts);
	RUN_TEST(basic_flagNodePtr);
	RUN_TEST(cas_flagNodePtr_test);
//	RUN_TEST(aNode_t_constrainsts);
	RUN_TEST(idType_constraints);
	RUN_TEST(basic_ANode_t_capabilites);
}

TEST add_first_node() {
	LockFreeBST bst;
	ASSERT(bst.add(6));
	ASSERT(bst.add(3));
	ASSERT(bst.add(9));
	ASSERT(bst.add(2));
	ASSERT(bst.add(5));
	ASSERT(bst.add(1));
	ASSERT(bst.add(4));
	ASSERT(bst.add(7));
	ASSERT(bst.add(10));
	ASSERT(bst.add(8));

	BSTDOTVisualizer visualizer;
	visualizer.readInBst(bst.getRoot());
	visualizer.exportToFile("graphs/firstNode.dot");
	PASS();
}

TEST contains_node() {
	{
		LockFreeBST bst;
		bst.add(3);
		ASSERT_EQ(true, bst.contains(3));
		ASSERT_EQ(false,bst.contains(42));
	}
	{
		LockFreeBST bst;
		bst.add(2);
		ASSERT_EQ(false, bst.contains(3));
	}
	PASS();
}
TEST nodeCategory_ANode_t_capabilites() {
	LockFreeBST bst;
	bst.add(5);
	// because root is -inf
	auto usersRootNode = bst.getRoot()->selectLRChild(RIGHT).load().get();
	ASSERT_EQ(5, usersRootNode->k.getInt());

	ASSERT_EQ(1, usersRootNode->getNodeCategory());
	bst.add(42);
	ASSERT_EQ(1, usersRootNode->getNodeCategory());

	// make it a cat2 node
	bst.add(2);
	ASSERT_EQ(2, usersRootNode->getNodeCategory());
	bst.add(1);
	ASSERT_EQ(2, usersRootNode->getNodeCategory());

	// make it a cat3 node
	bst.add(3);
	ASSERT_EQ(3, usersRootNode->getNodeCategory());

	PASS();
}

TEST tryFlag() {
	LockFreeBST bst;
	LockFreeBSTHelper bstHelper;
	ASSERT(bst.add(3));
	ASSERT(bst.add(4));

	//locate two specific nodes (3 and 4)
	auto root = bst.getRoot();
	ANodePtr prev = &root[1], curr = &root[0];
	bstHelper.locate(prev, curr, 3);
	ASSERT_EQ(3, curr->k.getInt());
	ANodePtr next = curr->rightChild.load().get(); //atomic load
	ASSERT_EQ(4, next->k.getInt());


	ASSERT(bstHelper.tryFlag(curr, next, prev, false));
	//check if flagged correctly
	ASSERT( curr->rightChild.load().isFlagged() );
	PASS();
}

/**
 * testCases for correctly locating order-nodes
 * for each and every node-category
 */
TEST locate_orderNodes() {
	LockFreeBST bst;
	ASSERT(bst.add(6)); //category 3
	ASSERT(bst.add(2)); //category 2
	ASSERT(bst.add(1)); //category 1
	ASSERT(bst.add(3)); //category 1

	LockFreeBSTHelper bstH;
	ANodePtr root = bst.getRoot();
	/**
	 * recall: order nodes are those where the threaded link
	 * emanate from.
	 */
	{	// Assumptions for nodes of category 1
		ANodePtr prev1 = &root[1], curr1 = &root[0];
		const int k1 = 3;
		const cmpRC_e dir1 = bstH.locate(prev1, curr1, {k1, NEGATIVE});  //!< locate order-link (as in remove(..))
		// check if curr is the order node
		ASSERT_EQ(k1, curr1->k.getInt());
		// check if suc(k_curr) is the tesired node
		//  (thats because threaded link of order node is the desired one)
		ASSERT(curr1->selectLRChild(dir1).load().isThreaded());
		ASSERT_EQ(k1, curr1->selectLRChild(dir1).load().get()->k.getInt());
	}
	{	// Assumptions for nodes of category 2
		ANodePtr prev2 = &root[1], curr2 = &root[0];
		const int k2 = 2;
		const cmpRC_e dir2 = bstH.locate(prev2, curr2, {k2, NEGATIVE});  //!< locate order-link (as in remove(..))
		ASSERT_EQ(1, curr2->k.getInt());
		ASSERT(curr2->selectLRChild(dir2).load().isThreaded());
		ASSERT_EQ(k2, curr2->selectLRChild(dir2).load().get()->k.getInt());
	}
	{	// Assumptions for nodes of category 2
		ANodePtr prev3 = &root[1], curr3 = &root[0];
		const int k3 = 6;  //value for a category 3 node
		const cmpRC_e dir3 = bstH.locate(prev3, curr3, {k3, NEGATIVE});  //!< locate order-link (as in remove(..))
		ASSERT_EQ(3, curr3->k.getInt());
		ASSERT(curr3->selectLRChild(dir3).load().isThreaded());
		ASSERT_EQ(k3, curr3->selectLRChild(dir3).load().get()->k.getInt());
	}

	LockFreeBST bstSinglenode;
	bstSinglenode.add(3);
	auto rootSingle = bstSinglenode.getRoot();
	{	// Assumptions for nodes of category 1
		ANodePtr prev = &rootSingle[1], curr = &rootSingle[0];
		const int k = 3;
		const cmpRC_e dir = bstH.locate(prev, curr, {k, NEGATIVE});  //!< locate order-link (as in remove(..))
		// check if curr is the order node
		ASSERT_EQ(k, curr->k.getInt());
		ASSERT_EQ(cmpRC_e::LEFT, dir);
		// check if suc(k_curr) is the tesired node
		//  (thats because threaded link of order node is the desired one)
		ASSERT(curr->selectLRChild(dir).load().isThreaded());
		ASSERT_EQ(k, curr->selectLRChild(dir).load().get()->k.getInt());
	}

	PASS();
}
TEST remove_cat1_node() {
	{
		LockFreeBST bst;
		bst.add(3);
		ASSERT(bst.remove(3));
		ASSERT_FALSE(bst.contains(3));
	}
	{
		LockFreeBST bst;
		bst.add(3);
		bst.add(2);
		ASSERT(bst.remove(2));
		ASSERT_FALSE(bst.contains(2));
	}
	{
		LockFreeBST bst;
		bst.add(3);
		bst.add(-1);
		ASSERT(bst.remove(-1));
		ASSERT_FALSE(bst.contains(-1));
	}
	PASS();
}
TEST remove_cat2_node() {
	{
		LockFreeBST bst;
		bst.add(3);
		bst.add(2);
		ASSERT(bst.remove(3));
		ASSERT_FALSE(bst.contains(3));
	}
	{
		LockFreeBST bst;
		bst.add(3);
		bst.add(2);
		bst.add(4);
		ASSERT(bst.remove(3));
		ASSERT_FALSE(bst.contains(3));
		ASSERT(bst.contains(2) && bst.contains(4));
	}
	{	//cat 2 remove
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		ASSERT(bst.remove(2));
		ASSERT_FALSE(bst.contains(2));
		ASSERT(bst.contains(6));
		ASSERT(bst.contains(1));
		ASSERT(bst.contains(3));
		ASSERT(bst.contains(7));
	}
	PASS();
}
TEST remove_cat3_node() {
	{	//cat 3 remove
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		ASSERT(bst.remove(6));
		ASSERT_FALSE(bst.contains(6));
		ASSERT(bst.contains(2));
		ASSERT(bst.contains(1));
		ASSERT(bst.contains(3));
		ASSERT(bst.contains(7));
	}
	{	//cat 3 remove
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(3)); //category 1

		ASSERT(bst.remove(6));
		ASSERT_FALSE(bst.contains(6));
		ASSERT(bst.contains(2));
		ASSERT(bst.contains(3));
	}
	{	//difficult add/contains checks
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		ASSERT(bst.remove(6));
		ASSERT(bst.remove(2));
		ASSERT_FALSE(bst.contains(2));
		ASSERT_FALSE(bst.contains(6));
		ASSERT(bst.add(6));
		ASSERT(bst.contains(6));

		ASSERT(bst.contains(1));
		ASSERT(bst.contains(3));
		ASSERT(bst.contains(7));
	}
	PASS();
}

SUITE(bst_tests) {
	RUN_TEST(add_first_node);
	RUN_TEST(contains_node);
	RUN_TEST(nodeCategory_ANode_t_capabilites);
	RUN_TEST(tryFlag);
	RUN_TEST(locate_orderNodes);
	RUN_TEST(remove_cat1_node);
	RUN_TEST(remove_cat2_node);
	RUN_TEST(remove_cat3_node);
}

TEST concurrent_add() {
	{
		LockFreeBST bst;
		ConcurrentExecuter cEx(bst, 5);
		const auto result = cEx.everyoneAddsSame({1});

		auto countOnes = [](const TimedSingleOpResult& v) {
			bool k_equal = cmp(v.k, IDType(1)) == cmpRC_e::EQUAL;
			return (k_equal) && (v.rc==true);
		};
		const int oneCount = std::count_if(result.begin(), result.end(), countOnes);
		ASSERT(oneCount == 1);
		ASSERT(bst.contains(1));
	}
	//Now lets inject some failures in the timing
	{
		LockFreeBST bst;
		bst.fi.enable();
		bst.fi.addFaultCase({{CASUId::ADD_CHILD, CASPrefix::PRE_CAS}, 0});
		bst.fi.addFaultCase({{CASUId::ADD_CHILD, CASPrefix::POST_CAS}, 0});

		ConcurrentExecuter cEx(bst, 5);
		const auto result = cEx.everyoneAddsSame({1});
		ASSERT(bst.contains(1));
	}
	{
		LockFreeBST bst;
		bst.fi.enable();
		bst.fi.addFaultCase({{CASUId::ADD_CHILD, CASPrefix::PRE_CAS}, 1});
		bst.fi.addFaultCase({{CASUId::ADD_CHILD, CASPrefix::POST_CAS}, 0});

		ConcurrentExecuter cEx(bst, 5);
		const auto result = cEx.everyoneAddsSame({1});
		ASSERT(bst.contains(1));
	}
	PASS();
}

TEST concurrent_cat3_remove() {
	{
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		ConcurrentExecuter cEx(bst, 100);
		cEx.everyoneRemovesSame({6});
		ASSERT_FALSE(bst.contains(6));
		ASSERT(bst.contains(2));
		ASSERT(bst.contains(1));
		ASSERT(bst.contains(3));
		ASSERT(bst.contains(7));
	}
	PASS();
}
TEST concurrent_cat2_disturbed_remove() {
    //Segfault in this testcase
	{
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		bst.fi.enable();
		bst.fi.addFaultCase({{CASUId::FLAG_CHILDLNK, CASPrefix::PRE_CAS}, 0, 400});
		bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 1, 40});
		ConcurrentExecuter cEx(bst, 2);
		cEx.everyoneRemovesSame({2}); //cat2 remove
		ASSERT_FALSE(bst.contains(2));
	}
	{
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		bst.fi.enable();
		bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 1});
		bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 0, 500});
		ConcurrentExecuter cEx(bst, 2);
		cEx.everyoneRemovesSame({2}); //cat2 remove
		ASSERT_FALSE(bst.contains(2));
	}
	PASS();
}

TEST concurrent_remove() {
	{
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		bst.fi.enable();
		bst.fi.addFaultCase({{CASUId::THREAD_STARTUP, CASPrefix::ANY}, 1, 10});
		bst.fi.addFaultCase({{CASUId::FLAG_CHILDLNK, CASPrefix::PRE_CAS}, 0, 500});
		ConcurrentExecuter cEx(bst, 5);
		cEx.everyoneRemovesSame({6});
		ASSERT_FALSE(bst.contains(6));
		ASSERT(bst.contains(2));
		ASSERT(bst.contains(1));
		ASSERT(bst.contains(3));
		ASSERT(bst.contains(7));
	}
	std::cout << "concurrent_remove 1" << std::endl;
	{
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		ConcurrentExecuter cEx(bst, 5);
		cEx.everyoneRemovesSame({6,2,1,3,7});
		ASSERT_FALSE(bst.contains(6));
		ASSERT_FALSE(bst.contains(2));
		ASSERT_FALSE(bst.contains(1));
		ASSERT_FALSE(bst.contains(3));
		ASSERT_FALSE(bst.contains(7));
	}
	std::cout << "concurrent_remove 2" << std::endl;
	{
		LockFreeBST bst;
		ASSERT(bst.add(6)); //category 3
		ASSERT(bst.add(2)); //category 2
		ASSERT(bst.add(1)); //category 1
		ASSERT(bst.add(3)); //category 1
		ASSERT(bst.add(7)); //category 1

		ConcurrentExecuter cEx(bst, 5);
		cEx.everyoneRemovesSame({6,2,1,3,7});
		ASSERT_FALSE(bst.contains(6));
		ASSERT_FALSE(bst.contains(2));
		ASSERT_FALSE(bst.contains(1));
		ASSERT_FALSE(bst.contains(3));
		ASSERT_FALSE(bst.contains(7));
	}
	std::cout << "concurrent_remove 3" << std::endl;

	PASS();
}

SUITE(bst_multithreaded) {
	RUN_TEST(concurrent_add);
	RUN_TEST(concurrent_cat3_remove);
	RUN_TEST(concurrent_cat2_disturbed_remove);
	RUN_TEST(concurrent_remove);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();
int main(int argc, char **argv) {
	GREATEST_MAIN_BEGIN();      /* command-line arguments, initialization. */
	greatest_info.flags |= GREATEST_FLAG_VERBOSE;    /* enable verbose mode by default */

	try {
		RUN_SUITE(bst_environment);
		RUN_SUITE(bst_tests);
		RUN_SUITE(bst_multithreaded);
	} catch( const std::exception& e) {
		std::cerr << " a standard exception was caught, with message '"
				<< e.what() << "'" << std::endl;
	}
	GREATEST_MAIN_END();        /* display results */
}

