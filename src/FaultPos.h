#ifndef SRC_FAULTPOS_H_
#define SRC_FAULTPOS_H_

#include <iostream>
#include <sstream>
#include <vector>

/**
 * the following structure is used to be able to print out
 * the enum class CASPosition as value plus their name using operator<<
 *
 * @note insert new CAS-position as macro below in the
 * 		form CAS_POSITION(NAME,VALUE)
 * @note take care to not use the same value twice
 * @ref http://stackoverflow.com/questions/3342726/c-print-out-enum-value-as-text/24876694#24876694
 */
#define CAS_POSITIONS\
	ELEMENT_CONVERTER(ADD_RIGHT, 0)\
	ELEMENT_CONVERTER(ADD_BACK_LNK, 1)\
	ELEMENT_CONVERTER(ADD_CHILD, 2)\
	ELEMENT_CONVERTER(FLAG_CHILDLNK, 3)\
	ELEMENT_CONVERTER(TRYMARK, 4)\
	ELEMENT_CONVERTER(CLEANFLAG_THR_PRELINK, 6)\
	ELEMENT_CONVERTER(CLEANFLAG_THR_RIGHT, 7)\
	ELEMENT_CONVERTER(CLEANFLAG_UTHR_CHILD0, 8)\
	ELEMENT_CONVERTER(CLEANFLAG_UTHR_CHILD1, 9)\
	ELEMENT_CONVERTER(CLEANFLAG_UTHR_CHILD2, 10)\
	ELEMENT_CONVERTER(CLEANFLAG_UTHR_BACK, 11)\
	ELEMENT_CONVERTER(CLEANFLAG_UTHR_V, 12)\
	ELEMENT_CONVERTER(CLEANMARK_RIGHT_V, 13)\
	ELEMENT_CONVERTER(CM_FLAG_PRES_PARENT, 15)\
	ELEMENT_CONVERTER(CLRMARK_O_X, 16)\
	ELEMENT_CONVERTER(CLRMARK_O_XI, 17)\
	ELEMENT_CONVERTER(CLRMARK_O_XII, 18)\
	ELEMENT_CONVERTER(CLRMARK_O_XIII, 19)\
	ELEMENT_CONVERTER(CLRMARK_O_XIV, 20)\
	ELEMENT_CONVERTER(CLRMARK_O_XV, 21)\
	ELEMENT_CONVERTER(CLRMARK_O_XVI, 22)\
	ELEMENT_CONVERTER(CLRMARK_O_XVII, 23)\
	\
	ELEMENT_CONVERTER(TRYMARK_MARKED_POS, 50)\
	ELEMENT_CONVERTER(TRYFLAG_REMOVE_I, 51)\
	ELEMENT_CONVERTER(TRYMARK_VII, 52)\
	\
	ELEMENT_CONVERTER(THREAD_STARTUP, 500)\
	ELEMENT_CONVERTER(PRE_THR_STARTUP, 501)\
	ELEMENT_CONVERTER(UNKNOWN, -1)


//@TODO rename to FaultUId
enum class CASUId : int {
#define ELEMENT_CONVERTER(NAME,VALUE) NAME=VALUE,
	CAS_POSITIONS
#undef ELEMENT_CONVERTER
};
const std::vector<CASUId> CASUIdElementList {
#define ELEMENT_CONVERTER(NAME, VALUE) CASUId::NAME,
		CAS_POSITIONS
#undef ELEMENT_CONVERTER
};

/**
 * Magical conversion from '#define CAS_POSITIONS..' to
 * an operator<< for printout reasons
 *
 * For production code #define PRODUCTION_BUILD, which
 * is disabling the name output
 *
 * @see http://stackoverflow.com/questions/11421432/how-can-i-output-the-value-of-an-enum-class-in-c11
 */
inline std::ostream& operator<<(std::ostream& os, const CASUId& val) {
	auto castedVal = static_cast<std::underlying_type<CASUId>::type>(val);
	switch (val) {
#ifndef PRODUCTION_BUILD // Don't print out names in production builds
#define ELEMENT_CONVERTER(NAME,VALUE) \
	case CASUId::NAME: return os << #NAME "(" << castedVal << ")";
	CAS_POSITIONS
#undef ELEMENT_CONVERTER
#endif //PRODUCTION_BUILD
	default:
	return os << castedVal;
	}
}

inline size_t getStringSize(const CASUId& val) {
	std::stringstream probedString;
	probedString << val;
	return probedString.str().size();
}

/*
 * Defines the position relative to the CAS
 */
enum class CASPrefix : int {
	PRE_CAS,
	POST_CAS,
	ANY
};
inline std::ostream& operator<<(std::ostream& os, const CASPrefix& val) {
	switch (val) {
	case CASPrefix::PRE_CAS:
		return os << "PRE";
	case CASPrefix::POST_CAS:
		return os << "POST";
	case CASPrefix::ANY:
		return os << "ANY";
	default:
		return os << "what the fuck are you doing?";
	}
}

/**
 * Holds a CAS Position identifier which with a printable interface
 * Usage: 	CASPosition pos{CASPosition::UId::ADD};
 * 		 or CASPosition pos{CASPosition::UId::ADD, CASPosition::Prefix::PRE_CAS};
 */
class FaultPos {
protected:
	// Fault position
	CASUId id;
	// pre or post the fault postion
	CASPrefix prefix = CASPrefix::ANY;
public:
	FaultPos(const CASUId& id, const CASPrefix& prefix):
			id(id), prefix(prefix) {};

	/**
	 * to move the methods to cpp: declare method header/identifyer extern in
	 * this file and move the method implementation to the cpp file
	 */
	friend bool operator==(const FaultPos& lhs, const FaultPos& rhs) {
		if (lhs.id != rhs.id)
			return false;
		const bool lORrMatchingAny =
				(lhs.prefix == CASPrefix::ANY) || (rhs.prefix == CASPrefix::ANY);
		return lORrMatchingAny || (lhs.prefix == rhs.prefix);
	}
	friend std::ostream& operator<<(std::ostream& os, const FaultPos& val) {
		return os << val.id << "_" << val.prefix;
	}
};


//int main(int argc, char **argv) {
//	FaultPos pos{ CASUId::ADD, CASPrefix::PRE_CAS};
//	FaultPos pos2{ CASUId::CLEANFLAG_UTHR_51};
//
//	std::cout << pos << std::endl;
//
//	std::cout << (pos == pos2) << std::endl;
//	return 0;
//}


#endif /* SRC_FAULTPOS_H_ */
