#ifndef LOCKFREEBST_H_
#define LOCKFREEBST_H_

#include <cstdbool>
#include <array>

#include "ANode_s.h"
#include "BSTInterface.h"
#include "IDType.h"
#include "LockFreeBSTHelper.h"


class LockFreeBST : public BSTInterface, public LockFreeBSTHelper {
public:
	virtual ~LockFreeBST();

	LockFreeBST();
	/**
	 * Deny calling the copy constructor because the internal
	 * data are not trivial. Handling could be implemented
	 * for future use.
	 */
	LockFreeBST(const LockFreeBST&) = delete;

	/**
	 * performs a high level contains check on
	 * the instances BST
	 * @return whether the element with IDType k
	 * 			is in the list or not
	 */
	virtual bool contains(const IDType k);

	/**
	 * performs a high level remove on
	 * the instances BST
	 * @return whether the element with IDType k
	 * 			has been removed successfully or not
	 */
	virtual bool remove(const IDType k);

	/**
	 * performs a high level add on
	 * the instances BST
	 * @return whether the element with IDType k
	 * 			has been added successfully or not
	 */
	virtual bool add(const IDType k);

	/**
	 * @return the pointer to the instances BST root[0]
	 */
	ANodePtr getRoot();
private:
	ANode_t root[2] = {
			{IDType::NEG_INFITY, {&root[0], 0,0,1}, {&root[1], 0,0,1}, &root[1], nullptr},
			{IDType::POS_INFITY, {&root[0], 0,0,0}, {nullptr , 0,0,1}, nullptr , nullptr}
	};

//	std::array<ANode_t,2> root = {{
//			//regarding to cppreference.com c++11 needs double brace, c++14 not anymore
//			{IDType::NEG_INFITY, {&root[0], 0,0,1}, {&root[1], 0,0,1}, &root[1], nullptr},
//			{IDType::POS_INFITY, {&root[0], 0,0,0}, {nullptr , 0,0,1}, nullptr , nullptr}
//	}};

};

#endif /* LOCKFREEBST_H_ */
