#include <atomic>
#include <iostream>
#include <string>
#include <vector>
#include <limits>
#include <sstream>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "ANode_s.h"
#include "FlagNodePtr.h"
#include "IDType.h"
#include "LockFreeBST.h"
#include "testHelper/ConcurrentExecuter.h"
#include "mockHelper/MockedLockFreeBST.h"
#include "mockHelper/MockMatcher.h"
#include "mockHelper/TestBaseClasses.h"
#include "testHelper/PerThreadCounter.h"
#include "testHelper/NaivePerThreadValueStore.h"
#include "bstdot_visualizer.h"
#include "benchmark/SimpleCSVWriter.h"
#include "benchmark/GenerateRandomValues.h"
#include "benchmark/ResultStoreCSVConverter.h"
#include "LockBasedList.h"

using ::testing::AtLeast;
using ::testing::_;
using ::testing::AnyNumber;
using ::testing::Gt;
using ::testing::Return;
using ::testing::ResultOf;
using ::testing::Eq;
using ::testing::AllOf;
using ::testing::AnyOf;

testing::ExpectationSet expect_rmMark_V(MockedLockFreeBST& bst,
										const int rmK,
										const int parentsK) {
	// add every expectation to ex to be able to expect expectations ".After" those
	// expectations
	testing::ExpectationSet ex;

	// Algo.line 107 to 117
	auto m_rmK = m_FlagNodePtr_k(rmK);
	auto m_flag_t = m_FlagNodePtr_flag(true);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(_,_, AllOf(m_flag_t,m_rmK), parentsK, AnyOf(CASUId::FLAG_CHILDLNK, CASUId::CLEANFLAG_UTHR_V) ))
			.Times(1);

	return ex;
}

/**
 * adds expectation concerning thee insert of a Node
 */
testing::ExpectationSet expect_addNode(MockedLockFreeBST& bst,
									   const int backLinkK,
									   const int newK,
									   const int rightDesired) {
	testing::Sequence s;
	testing::ExpectationSet ex;

	auto m_desired_k = m_FlagNodePtr_k(rightDesired);
	ex += EXPECT_CALL(bst, casMock_FlagNodePtr(testing::_, testing::_, m_desired_k, newK, CASUId::ADD_RIGHT))
			.Times(1)
			.InSequence(s);

	auto m_desired_back = m_ANodePtr_k(backLinkK);
	ex += EXPECT_CALL(bst, casMock_ANodePtr(testing::_, testing::_, m_desired_back, newK, CASUId::ADD_BACK_LNK))
			.Times(1)
			.InSequence(s);

	auto m_expected_right = m_FlagNodePtr_k(newK);
	ex += EXPECT_CALL(bst, casMock_FlagNodePtr(testing::_, testing::_, m_expected_right, backLinkK, CASUId::ADD_CHILD))
			.Times(1)
			.InSequence(s);
	return ex;
}

testing::ExpectationSet expect_rmMark_ItoIII(MockedLockFreeBST& bst,
											 const int rmK,
											 const int orderNodeK,
											 const int preLinkTargetK,
											 const int rightK) {
	// add every expectation to ex to be able to expect expectations ".After" those
	// expectations
	testing::ExpectationSet ex;

	// (I) flagging incoming order link
	//MockCAS-FlagNodePtr ... position:FLAG_CHILDLNK(3), used actual rc:true, object [k:10][was:10;00T, is:10;F0T, expected:10;00T, desired:10;F0T]
	auto m_rmK = m_FlagNodePtr_k(rmK);
	auto m_flag_order = m_FlagNodePtr_flag(true);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, testing::_, AllOf(m_flag_order, m_rmK), orderNodeK, CASUId::FLAG_CHILDLNK))
			.Times(1);

	// (II) set the prelink
	//MockCAS-ANodePtr    ... position:CLEANFLAG_THR_PRELINK(6), used actual rc:true,
	//						  object [k:10][was:-1, is: 10, expected:IDType-nullptr, desired:10]
	auto m_desired_back = m_ANodePtr_k(preLinkTargetK);
	ex += EXPECT_CALL(bst,
					  casMock_ANodePtr(testing::_, testing::_, m_desired_back, rmK, CASUId::CLEANFLAG_THR_PRELINK))
			.Times(1);

	// (III) mark the outgooin right link
	//MockCAS-FlagNodePtr ... position:CLEANFLAG_THR_RIGHT(7), used actual rc:true,
	// 						  object [k:10][was:20;000, is:20;0M0, expected:20;000, desired:20;0M0]
	auto m_right_k    = m_FlagNodePtr_k(rightK);
	auto m_right_thread = m_FlagNodePtr_threaded(true);
	auto m_right_mark = m_FlagNodePtr_mark(true);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, m_right_k, AllOf(m_right_k, m_right_mark), rmK, CASUId::CLEANFLAG_THR_RIGHT))
			.Times(1);
	return ex;
}

testing::ExpectationSet
expect_rmCat2(MockedLockFreeBST& bst,
			  const int kToBeRemoved,
			  const int kOrderNode,
			  const bool kHasRightChild,
			  const int kRightChild,
			  const int kBackLink) {
	testing::ExpectationSet ex2;
	const int kOldOrderRightTarget = kToBeRemoved;

	// Paper l.103
	auto m_orderNodeK = m_FlagNodePtr_k(kOldOrderRightTarget);
	auto m_new_orderNodeK = m_FlagNodePtr_k(kRightChild);
	ex2 += EXPECT_CALL(bst, casMock_FlagNodePtr(testing::_, m_orderNodeK, m_new_orderNodeK, kOrderNode,
												testing::_)).Times(1);
	// Paper l.104
	if(kHasRightChild) {
		// if the right child exists, the backLink has to be altered
		auto m_k = m_ANodePtr_k(kToBeRemoved);
		auto m_orderNode_k = m_ANodePtr_k(kOrderNode);
		ex2 += EXPECT_CALL(bst, casMock_ANodePtr(testing::_, m_k, m_orderNode_k, kRightChild, CASUId::CLEANFLAG_UTHR_BACK)).Times(1);
	}
	// Paper l.105
	auto m_k = m_FlagNodePtr_k(kToBeRemoved);
	auto m_orderNode = m_FlagNodePtr_k(kOrderNode);
	ex2 += EXPECT_CALL(bst, casMock_FlagNodePtr(testing::_, m_k, m_orderNode, kBackLink, testing::_)).Times(1);

	//Paper l.106
	auto m_k_ANode = m_ANodePtr_k(kToBeRemoved);
	auto m_backlink_k = m_ANodePtr_k(kBackLink);
	ex2 += EXPECT_CALL(bst, casMock_ANodePtr(testing::_, m_k_ANode, m_backlink_k, kOrderNode,
											 CASUId::CLEANFLAG_UTHR_BACK)).Times(1);

	return ex2;
}

/**
 * IV - expect flagging the predecessors incoming parent-link
 * @note only necessary during remove of CAT3 nodes
 */
testing::ExpectationSet expect_rmMark_IV(MockedLockFreeBST& bst,
										 const int kPreNode,
										 const int kPreNodesParent) {
	// add every expectation to ex to be able to expect expectations ".After" those
	// expectations
	testing::ExpectationSet ex;

	auto m_kPredecessor = m_FlagNodePtr_k(kPreNode);
	auto m_flag_t = m_FlagNodePtr_flag(true);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, m_kPredecessor, AllOf(m_flag_t, m_kPredecessor), kPreNodesParent, CASUId::CM_FLAG_PRES_PARENT))
			.Times(1);

	return ex;
}

testing::ExpectationSet expect_rmMark_VItoVII(MockedLockFreeBST& bst,
											  const int kToBeRemoved,
											  const int kLeftLinkOfRemove,
											  const int kPredecessor,
											  const int kLeftLinkOfPredecessor) {
	// add every expectation to ex to be able to expect expectations ".After" those
	// expectations
	testing::ExpectationSet ex;

	//(VI) mark the outgoing left-link
	auto m_kLeft = m_FlagNodePtr_k(kLeftLinkOfRemove);
	auto m_mark_t = m_FlagNodePtr_mark(true);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, m_kLeft, AllOf(m_mark_t, m_kLeft), kToBeRemoved, CASUId::TRYMARK))
			.Times(1);

	//(VII) mark the outgoing left-link of the predecessor
	auto m_kLeftPredecessor = m_FlagNodePtr_k(kLeftLinkOfPredecessor);
	auto m_kPredecessor = m_FlagNodePtr_k(kPredecessor);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, m_kLeftPredecessor, AllOf(m_mark_t, m_kLeftPredecessor), kPredecessor, CASUId::TRYMARK))
			.Times(1);
	return ex;
}

testing::ExpectationSet expect_rmCat3(MockedLockFreeBST& bst,
									  const int kPreParent,	// delete->preLink->BackLink
									  const int kPre,			// delete->preLink
									  const int kPreLeft,
									  const int kDelete,		//delNode
									  const int kDeleteParent,//delNode->backLink
									  const int kLeft,		//left of delNode
									  const int kRight		//right of delNode
) {
	const bool preLeftThreaded = (kPre == kPreLeft);
	const bool deleteRightThreaded = (kRight == pos_inf);
	// add every expectation to ex to be able to expect expectations ".After" those
	// expectations
	testing::ExpectationSet ex;

	//(X) copy the threaded state to the preParent.right link
	// - affects	preParent.right.Threaded
	// - requires 	preParent.left.isFlagged
	// - sets		preParent.right.isThreaded (to same as preParent.left)
	auto m_kparent = m_FlagNodePtr_k(kPre);
	auto m_kparentLeft = m_FlagNodePtr_k(kPreLeft);
	auto m_parentLeft_t = m_FlagNodePtr_threaded(preLeftThreaded);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, m_kparent, AllOf(m_parentLeft_t, m_kparentLeft), kPreParent, CASUId::CLRMARK_O_X));

	// (XI)
	if(not preLeftThreaded) {
		auto m_pre_k = m_ANodePtr_k(kPre);
		auto m_kPreParent = m_ANodePtr_k(kPreParent);
		ex += EXPECT_CALL(bst,
						  casMock_ANodePtr(testing::_, m_pre_k, m_kPreParent, kPreLeft, CASUId::CLRMARK_O_XI));
	}
	// (XII)
	auto m_kDelnodeLeft = m_FlagNodePtr_k(kLeft);
	auto m_marked_t = m_FlagNodePtr_mark(true);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, AllOf(m_kparentLeft, m_marked_t, m_parentLeft_t), m_kDelnodeLeft, kPre, CASUId::CLRMARK_O_XII));

	// (XIII)
	auto m_delNode_k = m_ANodePtr_k(kDelete);
	auto m_preNode_k = m_ANodePtr_k(kPre);
	ex += EXPECT_CALL(bst, casMock_ANodePtr(testing::_, m_delNode_k, m_preNode_k, kLeft, CASUId::CLRMARK_O_XIII));

	// (XIV)
	auto m_delNode_k2 = m_FlagNodePtr_k(kDelete);
	auto m_flagged = m_FlagNodePtr_flag(true);
	auto m_threaded = m_FlagNodePtr_threaded(true);
	auto m_threaded_drT = m_FlagNodePtr_threaded(deleteRightThreaded);
	auto m_delNodeR_k = m_FlagNodePtr_k(kRight);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, AllOf(m_delNode_k2, m_flagged, m_threaded), AllOf(m_delNodeR_k, m_threaded_drT), kPre, CASUId::CLRMARK_O_XIV));
	// (XV)
	if(not deleteRightThreaded) {
		ex += EXPECT_CALL(bst, casMock_ANodePtr(testing::_, m_delNode_k, m_preNode_k, kRight, CASUId::CLRMARK_O_XV));
	}

	// (XVI)
	auto m_flagged_true  = m_FlagNodePtr_flag(true);
	auto m_flagged_false = m_FlagNodePtr_flag(false);
	ex += EXPECT_CALL(bst,
					  casMock_FlagNodePtr(testing::_, AllOf(m_delNode_k2, m_flagged_true), AllOf(m_kparent, m_flagged_false), kDeleteParent, CASUId::CLRMARK_O_XVI));

	// (XVII)
	auto m_preParent_k = m_ANodePtr_k(kPreParent);
	auto m_delParent_k = m_ANodePtr_k(kDeleteParent);
	ex += EXPECT_CALL(bst, casMock_ANodePtr(testing::_, m_preParent_k, m_delParent_k, kPre, CASUId::CLRMARK_O_XVII));

	return ex;
}


//////////////// TestCases
TEST(Environment, contention_while) {
	GlobalDebugManager _globalDebugManager;
	_globalDebugManager.setDebugLogging(false);
	FaultProducer fi;
	int testCounter = 0;
	CONTENTION_WHILE_COUNTER(true) {
		testCounter++;
		break;
	}
	EXPECT_EQ(1, testCounter);
}

TEST(Environment, debugContentionLoop) {
	GlobalDebugManager _globalDebugManager;
	_globalDebugManager.setDebugLogging(false);

	const int threadAlias = 42;
	EXPECT_FALSE(_globalDebugManager.is_enContentionLogging());
	EXPECT_FALSE(_globalDebugManager.is_contentionLimitExceeded(threadAlias));
	EXPECT_FALSE(_globalDebugManager.is_recursionDeepnessExceeded(threadAlias));

	// debug limit
	DebugDataPerLoop perLoopData;
	for(int i=0; i<=_globalDebugManager.contentionDebugLimit; ++i) {
		EXPECT_FALSE(_globalDebugManager.is_enContentionLogging());
		_globalDebugManager.increaseLoopCounter(perLoopData);
		_globalDebugManager.detectContention(threadAlias, perLoopData);
	}
	EXPECT_TRUE(_globalDebugManager.is_enContentionLogging());
	EXPECT_FALSE(_globalDebugManager.is_contentionLimitExceeded(threadAlias));

	// another thread
	const int anotherThreadAlias = 43;
	DebugDataPerLoop perLoopData2;
	for(int i=0; i<=_globalDebugManager.contentionLimit-1; ++i) {
		_globalDebugManager.increaseLoopCounter(perLoopData2);
		_globalDebugManager.detectContention(anotherThreadAlias, perLoopData2);
	}
	EXPECT_TRUE(_globalDebugManager.is_enContentionLogging());
	EXPECT_FALSE(_globalDebugManager.is_contentionLimitExceeded(anotherThreadAlias));

	//topped with contention limit
	for(int i=0; i<=_globalDebugManager.contentionLimit-_globalDebugManager.contentionDebugLimit; ++i) {
		_globalDebugManager.increaseLoopCounter(perLoopData);
		_globalDebugManager.detectContention(threadAlias, perLoopData);
	}
	EXPECT_TRUE(_globalDebugManager.is_enContentionLogging());
	EXPECT_TRUE(_globalDebugManager.is_contentionLimitExceeded(threadAlias));
	EXPECT_FALSE(_globalDebugManager.is_contentionLimitExceeded(threadAlias-1));
}

TEST(Environment, debugRecursionLoop) {
	GlobalDebugManager _globalDebugManager;
	_globalDebugManager.setDebugLogging(false);

	const int threadAlias = 42;
	EXPECT_FALSE(_globalDebugManager.is_enContentionLogging());
	EXPECT_FALSE(_globalDebugManager.is_contentionLimitExceeded(threadAlias));
	EXPECT_FALSE(_globalDebugManager.is_recursionDeepnessExceeded(threadAlias));

	DebugDataPerLoop perLoopData;
	for(int i=0; i<=_globalDebugManager.recursionDebugLimit; ++i) {
		EXPECT_FALSE(_globalDebugManager.is_enContentionLogging());
		_globalDebugManager.incRecursion(threadAlias);
		_globalDebugManager.detectContention(threadAlias, perLoopData);
	}
	EXPECT_TRUE(_globalDebugManager.is_enContentionLogging());
	EXPECT_FALSE(_globalDebugManager.is_recursionDeepnessExceeded(threadAlias));
}

TEST(Environment, threadedUsageOfVector) {
	const int threadCount = 10;

	std::vector<int> testVector(threadCount, 0);
	auto addTimesToAliasPosition = [&testVector](const size_t threadAlias) {
		for(size_t i=0; i<100; ++i) testVector[threadAlias]++;
	};

	std::vector<std::thread> testThreads(threadCount);
	for(int i=0; i<threadCount; ++i) {
		testThreads[i] = std::thread(addTimesToAliasPosition, i);
	}

	for(auto& thread : testThreads){
		thread.join();
	}
	for(auto& testValue: testVector) {
		EXPECT_EQ(100, testValue);
	}
}

TEST(Environment, PerThreadValueStoreTest) {
	const bool initValue = false;
	PerThreadValueStore<bool> vStore(3, initValue);

	EXPECT_EQ(3u, vStore.size());

	vStore.set(3, true);
	EXPECT_EQ(initValue, vStore.get(0));
	EXPECT_EQ(initValue, vStore.get(1));
	EXPECT_EQ(initValue, vStore.get(2));
	EXPECT_TRUE(vStore.get(3));
	EXPECT_EQ(4u, vStore.size());

	EXPECT_EQ(initValue, vStore.get(-1));
	EXPECT_EQ(4u, vStore.size());
}

TEST(Environment, DotVisualizer) {
	LockFreeBST bst;
	const std::vector<IDType> containAfterSetup {6,2,1,4,3};
	for(auto &it: containAfterSetup) {
		EXPECT_TRUE(bst.add(it));
		EXPECT_TRUE(bst.contains(it));
	}

	BSTDOTVisualizer visualizer;
	visualizer.readInBst(bst.getRoot());
	visualizer.exportToFile("/tmp/dotVisualizerTest.dot");
}

TEST(BSTAdd, SingleNode) {
	MockedLockFreeBST bst;
	expect_addNode(bst, neg_inf, 6, pos_inf);
	EXPECT_TRUE(bst.add(6));
}
TEST(BSTAdd, Cat2Node) {
	MockedLockFreeBST bst;
	expect_addNode(bst, neg_inf, 6, pos_inf);
	EXPECT_TRUE(bst.add(6));

	expect_addNode(bst, 6, 1, 6);
	EXPECT_TRUE(bst.add(1));
}
TEST(BSTAdd, Cat3Node) {
	/**
	 *     -inf   inf
	 *       |    /
	 *       6---
	 *      /
	 *     1
	 *      \
	 *       2
	 */
	MockedLockFreeBST bst;
	expect_addNode(bst, neg_inf, 6, pos_inf);
	EXPECT_TRUE(bst.add(6));

	expect_addNode(bst, 6, 1, 6);
	EXPECT_TRUE(bst.add(1));

	expect_addNode(bst, 1, 2, 6);
	EXPECT_TRUE(bst.add(2));
}

TEST(BSTLinks, Cat3Links) {
	testing::NiceMock<MockedLockFreeBST> bst;
	EXPECT_TRUE(bst.add(6));
	EXPECT_TRUE(bst.add(1));
	EXPECT_TRUE(bst.add(2));
	bst.remove(1);
	bst.remove(2);
	EXPECT_TRUE(bst.add(1));
	EXPECT_TRUE(bst.add(2));

	ANodePtr rootNode = bst.getRoot();
	EXPECT_EQ(neg_inf, rootNode->k.getInt());

	ANodePtr node6 = rootNode->rightChild.load().get();
	EXPECT_EQ(6, node6->k.getInt());
	EXPECT_TRUE(node6->rightChild.load().isThreaded());
	EXPECT_EQ(pos_inf, node6->rightChild.load().get()->k.getInt());
	EXPECT_FALSE(node6->leftChild.load().isThreaded());
	EXPECT_EQ(1, node6->leftChild.load().get()->k.getInt());
	EXPECT_EQ(neg_inf, node6->backLink.load()->k.getInt());
	EXPECT_EQ(nullptr, node6->preLink);

	ANodePtr node1 = node6->leftChild.load().get();
	EXPECT_EQ(1, node1->k.getInt());
	EXPECT_FALSE(node1->rightChild.load().isThreaded());
	EXPECT_EQ(2, node1->rightChild.load().get()->k.getInt());
	EXPECT_TRUE(node1->leftChild.load().isThreaded());
	EXPECT_EQ(1, node1->leftChild.load().get()->k.getInt()); //points to itself
	EXPECT_EQ(6, node1->backLink.load()->k.getInt());
	EXPECT_EQ(nullptr, node1->preLink);

	ANodePtr node2 = node1->rightChild.load().get();
	EXPECT_EQ(2, node2->k.getInt());
	EXPECT_TRUE(node2->rightChild.load().isThreaded());
	EXPECT_EQ(6, node2->rightChild.load().get()->k.getInt());
	EXPECT_TRUE(node2->rightChild.load().isThreaded());
	EXPECT_EQ(2, node2->leftChild.load().get()->k.getInt()); //points to itself
	EXPECT_EQ(1, node2->backLink.load()->k.getInt());
	EXPECT_EQ(nullptr, node2->preLink);
}

class BasicFilledBSTwithoutExpectation : public BasicEmptyBSTwithoutExpectation {
protected:
    ::testing::NiceMock<MockedLockFreeBST> bst;

    ~BasicFilledBSTwithoutExpectation() {
    }

    virtual void SetUp() {
        preFillBST(bst);
        extractFilledNodePtr(bst);

        std::cout << "BasicFilledBST: Done adding nodes.." << std::endl;
    }
};

class BasicFilledBST : public BasicFilledBSTwithoutExpectation {
protected:
    ~BasicFilledBST() {
    }

	/**
	 *      6
	 *    /   \
	 *   2     8
	 *  / \   /
	 * 1   3 7
	 */
	virtual void SetUp() {
		{	//insert k==6, cat3
			const int newK = 6;
			expect_addNode(bst, neg_inf, newK, pos_inf);
		}
		{	//insert k==2, cat 2
			const int newK = 2;
			expect_addNode(bst, 6, newK, 6);
		}
		{	//insert k==1, cat 1
			const int newK = 1;
			expect_addNode(bst, 2, newK, 2);
		}
		{	//insert k==3, cat 1
			const int newK = 3;
			expect_addNode(bst, 2, newK, 6);
		}
		{	//insert k==8, cat 2
			const int newK = 8;
			expect_addNode(bst, 6, newK, pos_inf);
		}
		{	//insert k==7, cat 1
			const int newK = 7;
			expect_addNode(bst, 8, newK, 8);
		}

		BasicFilledBSTwithoutExpectation::SetUp();
	}

};

class BSTAlternativeCat3RM_XI : public testing::Test {
protected:
	/**
	 *      6
	 *    /   \
	 *   2     8
	 *  / \   /
	 * 1   3 7
	 */
	const std::vector<IDType> containAfterSetup {6,2,1,4,3};
	::testing::NiceMock<MockedLockFreeBST> bst;

	virtual void SetUp() {
		//disable stdout
		testing::internal::CaptureStdout();

		{
			//insert k==6, cat3
			const int newK = 6;
			expect_addNode(bst, neg_inf, newK, pos_inf);
			EXPECT_TRUE(bst.add(newK));
		}
		{
			//insert k==2, cat 2
			const int newK = 2;
			expect_addNode(bst, 6, newK, 6);
			EXPECT_TRUE(bst.add(newK));
		}
		{
			//insert k==1, cat 1
			const int newK = 1;
			expect_addNode(bst, 2, newK, 2);
			EXPECT_TRUE(bst.add(newK));
		}
		{
			const int newK = 4;
			expect_addNode(bst, 2, newK, 6);
			EXPECT_TRUE(bst.add(newK));
		}

		{
			//insert k==3, cat 1
			const int newK = 3;
			expect_addNode(bst, 4, newK, 4);
			EXPECT_TRUE(bst.add(newK));
		}

		for(auto it: containAfterSetup) {
			EXPECT_TRUE(bst.contains(it));
		}

		//enable stdout again
		testing::internal::GetCapturedStdout();
		std::cout << "BasicFilledBST: Done adding nodes.." << std::endl;
	}

};

class RemoveBasicNode : public BasicFilledBST {
};

TEST_F(RemoveBasicNode, rmCat1_twoTimes) {
	EXPECT_CALL(bst, casMock_FlagNodePtr(_,_,_,_,_)).Times(AnyNumber());
	EXPECT_CALL(bst, casMock_ANodePtr(_,_,_,_,_)).Times(AnyNumber());

	EXPECT_TRUE(bst.remove(1));
	EXPECT_FALSE(bst.remove(1));
}
TEST_F(RemoveBasicNode, rmCat2_twoTimes) {
	EXPECT_CALL(bst, casMock_FlagNodePtr(_,_,_,_,_)).Times(AnyNumber());
	EXPECT_CALL(bst, casMock_ANodePtr(_,_,_,_,_)).Times(AnyNumber());

	EXPECT_TRUE(bst.remove(2));
	EXPECT_FALSE(bst.remove(2));
}
TEST_F(RemoveBasicNode, rmCat2_variation_twoTimes) {
	EXPECT_CALL(bst, casMock_FlagNodePtr(_,_,_,_,_)).Times(AnyNumber());
	EXPECT_CALL(bst, casMock_ANodePtr(_,_,_,_,_)).Times(AnyNumber());

	EXPECT_TRUE(bst.remove(8)); //That makes the variation
	EXPECT_FALSE(bst.remove(8));
}
TEST_F(RemoveBasicNode, rmCat3_twoTimes) {
	EXPECT_CALL(bst, casMock_FlagNodePtr(_,_,_,_,_)).Times(AnyNumber());
	EXPECT_CALL(bst, casMock_ANodePtr(_,_,_,_,_)).Times(AnyNumber());

	EXPECT_TRUE(bst.remove(6));
	EXPECT_FALSE(bst.remove(6));
}
TEST_F(RemoveBasicNode, removeAll) {
	EXPECT_CALL(bst, casMock_FlagNodePtr(_,_,_,_,_)).Times(AnyNumber());
	EXPECT_CALL(bst, casMock_ANodePtr(_,_,_,_,_)).Times(AnyNumber());

	for (auto &it: containAfterSetup) {
		bst.remove(it);
	}
}

TEST_F(RemoveBasicNode, rmCat2_rightChild) {
	const int kToBeRemoved = 2;

	expect_rmMark_ItoIII(bst, kToBeRemoved, 1, 1, 3);
	expect_rmMark_V(bst, kToBeRemoved, 6);
	expect_rmCat2(bst, kToBeRemoved, 1, true, 3, 6);
	//EXPECT_CALL(bst, casMock_ANodePtr(_,_,_,_,_)).Times(AnyNumber()).After(ex1,ex2,ex3);

	EXPECT_TRUE(bst.remove(kToBeRemoved));
	EXPECT_FALSE(bst.contains(kToBeRemoved));

	//NodeNegInf
	EXPECT_EQ(6, this->nodeNegInf->rightChild.load().get()->k.getInt());
	//Node6
	EXPECT_EQ(neg_inf, node6->backLink.load()->k.getInt());
	EXPECT_EQ(1, this->node6->leftChild.load().get()->k.getInt());
	EXPECT_EQ(8, this->node6->rightChild.load().get()->k.getInt());
	EXPECT_EQ(nullptr, node6->preLink.load());	//NodeNegInf
	//Node1
	EXPECT_EQ(6, node1->backLink.load()->k.getInt());
	EXPECT_EQ(1, this->node1->leftChild.load().get()->k.getInt());
	EXPECT_EQ(3, this->node1->rightChild.load().get()->k.getInt());
	EXPECT_EQ(nullptr, node1->preLink.load());
	//Node3
	EXPECT_EQ(1, node3->backLink.load()->k.getInt()); //this failed in the initial version
	EXPECT_EQ(3, this->node3->leftChild.load().get()->k.getInt());
	EXPECT_EQ(6, this->node3->rightChild.load().get()->k.getInt());
	EXPECT_EQ(nullptr, node3->preLink.load());
}

TEST_F(RemoveBasicNode, rmCat2_no_rightChild) {
	const int kToBeRemoved = 8;

	expect_rmMark_ItoIII(bst, kToBeRemoved, 7, 7, pos_inf);
	expect_rmMark_V(bst, kToBeRemoved, 6);
	expect_rmCat2(bst, kToBeRemoved, 7, false, pos_inf, 6);

	EXPECT_TRUE(bst.remove(kToBeRemoved));
	EXPECT_FALSE(bst.contains(kToBeRemoved));
}

TEST_F(RemoveBasicNode, rmCat3) {
	const int kToBeRemoved = 6;
	auto ex1 = expect_rmMark_ItoIII(bst, kToBeRemoved, 3, 3, 8);
	auto ex2 = expect_rmMark_IV(bst, 3, 2);
	auto ex3 = expect_rmMark_V(bst, kToBeRemoved, neg_inf);
	auto ex4 = expect_rmMark_VItoVII(bst, kToBeRemoved, 2, 3, 3);

	auto ex5 = expect_rmCat3(bst, 2, 3, 3, kToBeRemoved, neg_inf, 2, 8);

	EXPECT_TRUE(bst.remove(kToBeRemoved));
	EXPECT_FALSE(bst.contains(kToBeRemoved));
}

class LemmaChecking : public BasicEmptyBSTwithoutExpectation {
public:
	LockFreeBST bst;
};

TEST_F(LemmaChecking, rmCat1_lemma10_a_marked) {
	this->preFillGivenValues(bst, {6, 8});

	EXPECT_TRUE(bst.remove(8));
	EXPECT_TRUE(bst.remove(6));
}

TEST_F(BSTAlternativeCat3RM_XI, rmCat3) {
	const int kToBeRemoved = 6;
	auto ex1 = expect_rmMark_ItoIII(bst, kToBeRemoved, 4, 4, pos_inf);
	auto ex2 = expect_rmMark_IV(bst, 4, 2);
	auto ex3 = expect_rmMark_V(bst, kToBeRemoved, neg_inf);
	auto ex4 = expect_rmMark_VItoVII(bst, kToBeRemoved, 2, 4, 3);

	auto ex5 = expect_rmCat3(bst, 2, 4, 3, kToBeRemoved, neg_inf, 2, pos_inf);

//	EXPECT_CALL(bst, casMock_FlagNodePtr(_,_,_,_,_)).Times(AnyNumber()).After(ex1, ex2, ex3, ex4, ex5);
//	EXPECT_CALL(bst, casMock_ANodePtr(_,_,_,_,_)).Times(AnyNumber()).After(ex1   , ex2, ex3, ex4, ex5);

	EXPECT_TRUE(bst.remove(kToBeRemoved));
	EXPECT_FALSE(bst.contains(kToBeRemoved));
}

TEST_F(RemoveBasicNode, rmCat3_postRemovalLinksCheck) {
	EXPECT_CALL(bst, casMock_FlagNodePtr(_,_,_,_,_)).Times(AnyNumber());
	EXPECT_CALL(bst, casMock_ANodePtr(_,_,_,_,_)).Times(AnyNumber());
	// do the actual remove
	const int kToBeRemoved = 6;
	EXPECT_TRUE(bst.remove(kToBeRemoved));
	EXPECT_FALSE(bst.contains(kToBeRemoved));

	//NodeNegInf
	EXPECT_EQ(3, this->nodeNegInf->rightChild.load().get()->k.getInt());
	//Node3
	EXPECT_EQ(neg_inf, node3->backLink.load()->k.getInt());
	EXPECT_EQ(2, this->node3->leftChild.load().get()->k.getInt());
	EXPECT_EQ(8, this->node3->rightChild.load().get()->k.getInt());
	EXPECT_EQ(nullptr, node3->preLink.load());

	//Node2
	EXPECT_EQ(3, node2->backLink.load()->k.getInt());
	EXPECT_EQ(1, this->node2->leftChild.load().get()->k.getInt());
	EXPECT_EQ(3, this->node2->rightChild.load().get()->k.getInt());
	EXPECT_EQ(nullptr, node2->preLink.load());

	//Node1
	EXPECT_EQ(2, node1->backLink.load()->k.getInt());
	EXPECT_EQ(1, this->node1->leftChild.load().get()->k.getInt());
	EXPECT_EQ(2, this->node1->rightChild.load().get()->k.getInt());
	EXPECT_EQ(nullptr, node1->preLink.load());

	//Node8
	EXPECT_EQ(3, node8->backLink.load()->k.getInt());
	EXPECT_EQ(7, this->node8->leftChild.load().get()->k.getInt());
	EXPECT_EQ(pos_inf, this->node8->rightChild.load().get()->k.getInt());
	EXPECT_EQ(nullptr, node8->preLink.load());

	//Node7
	EXPECT_EQ(8, node7->backLink.load()->k.getInt());
	EXPECT_EQ(7, this->node7->leftChild.load().get()->k.getInt());
	EXPECT_EQ(8, this->node7->rightChild.load().get()->k.getInt());
	EXPECT_EQ(nullptr, node7->preLink.load());
}

class ConcurrentRemove : public BasicEmptyBSTwithoutExpectation {
protected:
    //LockFreeBST notProvided; //is NOT provided by the ConcurrentRemove Class

    const int threadCount = 10;
};

TEST_F(ConcurrentRemove, cat3) {
    LockFreeBST bst;
    this->preFillBST(bst);
    this->extractFilledNodePtr(bst);


	ConcurrentExecuter cEx(bst, threadCount);
	cEx.everyoneRemovesSame({6});
	EXPECT_FALSE(bst.contains(6));
	EXPECT_TRUE(bst.contains(2));
	EXPECT_TRUE(bst.contains(1));
	EXPECT_TRUE(bst.contains(3));
	EXPECT_TRUE(bst.contains(7));
}
TEST_F(ConcurrentRemove, cat3_removeALot) {
    const int kToBeRemoved = 6;

	for(int i = 0; i < 1000; ++i) {
        LockFreeBST bst;
        this->preFillBST(bst);
        this->extractFilledNodePtr(bst);

		ConcurrentExecuter cEx(bst, threadCount);
		cEx.everyoneRemovesSame({kToBeRemoved});
		EXPECT_FALSE(bst.contains(kToBeRemoved));

		//NodeNegInf
		EXPECT_EQ(3, this->nodeNegInf->rightChild.load().get()->k.getInt());
		//Node3
		EXPECT_EQ(neg_inf, node3->backLink.load()->k.getInt());
		EXPECT_EQ(2, this->node3->leftChild.load().get()->k.getInt());
		EXPECT_EQ(8, this->node3->rightChild.load().get()->k.getInt());
		EXPECT_EQ(nullptr, node3->preLink.load());

		//Node2
		EXPECT_EQ(3, node2->backLink.load()->k.getInt());
		EXPECT_EQ(1, this->node2->leftChild.load().get()->k.getInt());
		EXPECT_EQ(3, this->node2->rightChild.load().get()->k.getInt());
		EXPECT_EQ(nullptr, node2->preLink.load());

		//Node1
		EXPECT_EQ(2, node1->backLink.load()->k.getInt());
		EXPECT_EQ(1, this->node1->leftChild.load().get()->k.getInt());
		EXPECT_EQ(2, this->node1->rightChild.load().get()->k.getInt());
		EXPECT_EQ(nullptr, node1->preLink.load());

		//Node8
		EXPECT_EQ(3, node8->backLink.load()->k.getInt());
		EXPECT_EQ(7, this->node8->leftChild.load().get()->k.getInt());
		EXPECT_EQ(pos_inf, this->node8->rightChild.load().get()->k.getInt());
		EXPECT_EQ(nullptr, node8->preLink.load());

		//Node7
		EXPECT_EQ(8, node7->backLink.load()->k.getInt());
		EXPECT_EQ(7, this->node7->leftChild.load().get()->k.getInt());
		EXPECT_EQ(8, this->node7->rightChild.load().get()->k.getInt());
		EXPECT_EQ(nullptr, node7->preLink.load());


		//this->emptyBST(bst);
	}
}

TEST_F(ConcurrentRemove, rmCat2_removeALot) {
	const int kToBeRemoved = 2;

	for(int i = 0; i < 5000; ++i) {
        LockFreeBST bst;
        this->preFillBST(bst);
        this->extractFilledNodePtr(bst);

		ConcurrentExecuter cEx(bst, threadCount);
		cEx.everyoneRemovesSame({kToBeRemoved});
		EXPECT_FALSE(bst.contains(kToBeRemoved));

		//NodeNegInf
		EXPECT_EQ(6, this->nodeNegInf->rightChild.load().get()->k.getInt());
		//Node6
		EXPECT_EQ(neg_inf, node6->backLink.load()->k.getInt());
		EXPECT_EQ(1, this->node6->leftChild.load().get()->k.getInt());
		EXPECT_EQ(8, this->node6->rightChild.load().get()->k.getInt());
		EXPECT_EQ(nullptr, node6->preLink.load());    //NodeNegInf
		//Node1
		EXPECT_EQ(6, node1->backLink.load()->k.getInt());
		EXPECT_EQ(1, this->node1->leftChild.load().get()->k.getInt());
		EXPECT_EQ(3, this->node1->rightChild.load().get()->k.getInt());
		EXPECT_EQ(nullptr, node1->preLink.load());
		//Node3
		EXPECT_EQ(1, node3->backLink.load()->k.getInt()); //this failed in the initial version
		EXPECT_EQ(3, this->node3->leftChild.load().get()->k.getInt());
		EXPECT_EQ(6, this->node3->rightChild.load().get()->k.getInt());
		EXPECT_EQ(nullptr, node3->preLink.load());

		//this->emptyBST(bst);
	}
}

//@todo To be implemented: Concurrent ADD, Concurrent locate

TEST_F(ConcurrentRemove, oneThreadIsBehind) {
    const int kToBeRemoved = 6;
    const int threadCount = 2;
    const int threadToBeDelayed = 1; //starting @ zero

    LockFreeBST bst;
    this->preFillBST(bst);
    this->extractFilledNodePtr(bst);

    bst.fi.enable();
    bst.fi.addFaultCase({{CASUId::PRE_THR_STARTUP, CASPrefix::ANY}, threadToBeDelayed, 200});

    ConcurrentExecuter cEx(bst, threadCount);
    cEx.everyoneRemovesSame({kToBeRemoved});
    EXPECT_FALSE(bst.contains(kToBeRemoved));
}

TEST_F(ConcurrentRemove, rmMixed) {
	size_t threadCount = 50; //containAfterSetup.size();

	for(int i = 0; i < 3000; ++i) {
		LockFreeBST bst;
		this->preFillBST(bst);
		this->extractFilledNodePtr(bst);
		std::cout << "i:" << i << "; Done adding nodes, starting with remove" << std::endl;

		ConcurrentExecuter cEx(bst, threadCount);
		auto resultStore = cEx.everyoneRemovesSame(containAfterSetup);
		std::cout << "removing done, resultStore contains " << resultStore.size() <<" elements" << std::endl;
		for(auto& each : resultStore) { //throw exception if one has been occured in one of the threads
			if (each.exception) {
				std::rethrow_exception(each.exception);
			}
		}

		ASSERT_FALSE(bst.contains(6));
		ASSERT_FALSE(bst.contains(2));
		ASSERT_FALSE(bst.contains(1));
		ASSERT_FALSE(bst.contains(3));
		ASSERT_FALSE(bst.contains(8));
		ASSERT_FALSE(bst.contains(7));
	}
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}
