/*
 * FaultInjector.h
 *
 *  Created on: 16 Oct 2015
 *      Author: nickma
 */

#ifndef FAULTINJECTOR_H_
#define FAULTINJECTOR_H_

#include <thread>
#include <map>
#include <vector>
#include <functional>
#include <iostream>

#include "FaultPos.h"
#include "FaultCase.h"

/**
 * Injects faults as delays into the BST methods
 *
 * The main idea is to be able to setup condition filters for
 *  fault injections. To achieve that:
 *  - std::thread::id's are linked/maped to human read-/setup-able values (int)
 *    this is issued due to the method rememberAlias
 *  - the concurrent process call's the handle() interface on
 *    every injectable position
 *  - the user is able to register faults (delays), through addFaultCase
 *
 * Usage: The caller simply has to call/implement the handle(..)
 *  function, at the position(s) which should have the ability
 *  to be disturbed.
 */
class FaultProducer {
	std::vector<FaultCase> _faultCases;
	bool _enabled = false;

	/**
	 * Thread alias's are ment to be used for filtering
	 * by the threads aliases instead of the threads std::thread::id's.
	 */
	std::map<std::thread::id, int> _threadsAliases;
public:
	FaultProducer();
	FaultProducer(const FaultProducer&) = delete;

	/**
	 * Enables the FaultProducer overall, if disabled (default) the
	 * handler() simply returns right after calling it.
	 */
	void enable();

	/**
	 * injects the fault production if enabled. In order to do so it
	 * checks the _faultCases. To be called from every CAS Operation.
	 * @see addFaultCase(..)
	 */
	void handle(const FaultPos& pos) const;

	void handle(const FaultPos& pos, const unsigned int threadAlias) const;

	/**
	 * stores an alias mapped to a thread::id
	 *
	 * this should be called by the ConcurrentExecuter or
	 * the very instance creating the working threads.
	 */
	void registerThreadAlias(const std::thread::id &threadID, const unsigned int alias);

	/**
	 * add's a fault condition/filter to the matching list, to
	 * react on if matched during a handle(..) call
	 */
	void addFaultCase(const FaultCase& faultEntity);

	/**
	 * resets the faultInjector to the initial state
	 */
	void reset();

	/**
	 * looks up in the alias map for this_thread::get_id
	 * @return the thread alias if found in the alias map, -1 otherwise
	 */
	int getThisThreadAlias() const;
	int getThreadAlias(std::thread::id id) const;

	void _printFoundFaultCaseData(const FaultPos &pos, const int alias, const FaultCase &foundFaultCase) const;

	int _findThreadAlias(const std::thread::id &this_id) const;
};

#endif /* FAULTINJECTOR_H_ */
