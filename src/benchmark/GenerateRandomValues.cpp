#include "GenerateRandomValues.h"

#include <unordered_set>
#include <random>
#include <cmath>

std::vector<int> GenerateRandomValues::getIntVector(
        const int targetElementCount, const int valueRange) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<> dist(1., (double)valueRange);

    std::vector<int> outputData;
    for (int i=0; i < targetElementCount; ++i) {
    	const double rndm_value = dist(mt);
    	const int rndm_int_value = (int)round(rndm_value);

        outputData.push_back(rndm_int_value);
    }

    return outputData;
}

unsigned int GenerateRandomValues::countUniqueElements(
        const std::vector<int> notUniqueInput) {
    std::unordered_set<int> s(notUniqueInput.begin(), notUniqueInput.end());

    return s.size();
}