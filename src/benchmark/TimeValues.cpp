#include "TimeValues.h"
#include <iostream>

std::ostream& operator<<(std::ostream& inout, const TimerType& v) {
	switch(v) {
		case ID_CONTAINS:
			inout << "contains";
			break;
		case ID_INSERT:
			inout << "insert";
			break;
		case ID_REMOVE:
			inout << "remove";
			break;
		case ID_OTHERS:
			inout << "others";
			break;
		default:
			inout << "following TimerType unkown:" << (int)v;
			break;
	}
	return inout;
}

TimerValue::TimerValue(const duration_base &duration, const TimerType &type) :
	duration(duration), timerType(type) {}

void TimerValue::printValues() {}

std::ostream& operator<<(std::ostream& os, const TimerValue& obj) {
	// write obj to stream
	os << std::setw(9) << std::setfill(' ') << obj.duration.count()
	<< ", type:" << std::setw(9) << std::setfill(' ') << obj.timerType;
	return os;
}

ThreadTimerValue::ThreadTimerValue(const TimerValue &tv, const unsigned int threadID) :
			TimerValue(tv), threadID(threadID) {}

ThreadTimerValue::ThreadTimerValue(
			const duration_base& duration,
			const TimerType &type,
			const unsigned int threadID) :
			TimerValue(duration, type), threadID(threadID)
{ }

void ThreadTimerValue::printValues() {
	std::cout << "time:" << duration.count() << " type:" << timerType
			<< " id:" << threadID << std::endl;
}


std::ostream& operator<<(std::ostream& os, const ThreadTimerValue& obj) {
	// write obj to stream
	os << std::setw(10) << std::setfill(' ') << (unsigned int)obj.threadID
			<< "," << std::setw(9) << std::setfill(' ') << obj.duration.count()
			<< "," << std::setw(9) << std::setfill(' ') << obj.timerType;
	return os;
}
