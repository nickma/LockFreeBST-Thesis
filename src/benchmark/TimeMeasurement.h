#ifndef SRC_TIMEMEASUREMENT_H_
#define SRC_TIMEMEASUREMENT_H_

#include <chrono>
#include <string>

#include "commonTimeDefs.h"

class StartStopTimer {
public:
	StartStopTimer();

	/**
	 * Starts the time measurement
	 *
	 * @note as a sideeffect start() invalidates previously
	 * measured timeSpans
	 */
	void start();

	/**
	 * Stops the time measurement
	 */
	void stop();

	/**
	 * Returns the period of time measured
	 * @returns a valid duration after calling time_stop(),
	 * 			otherwise duration is set to invalid_duration
	 */
	const duration_base getDuration() const;

private:
	/**
	 * holds the _type of time measured
	 */
	duration_base _duration = invalid_duration;

	bool _measurementRunning = false;
	std::chrono::time_point<std::chrono::high_resolution_clock> _startTime;

	/**
	 * Sets the timespan to invalid
	 */
	void _invalidateResult();

	/**
	 * Checks and sets _measurementRunning for consistency in
	 * order to forbid setting the same state twice
	 *
	 * @var[in] nextState 	state to be set next
	 */
	void _setTimerRunningState(const bool &nextState);
};




#endif /* SRC_TIMEMEASUREMENT_H_ */
