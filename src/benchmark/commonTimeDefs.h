#ifndef SRC_BENCHMARK_COMMONTIMEDEFS_H_
#define SRC_BENCHMARK_COMMONTIMEDEFS_H_

#include <chrono>
#include <array>

typedef std::chrono::nanoseconds duration_base;
constexpr duration_base invalid_duration{0}; //replace through std::chrono::nanoseconds::zero()

// http://stackoverflow.com/questions/8498300/allow-for-range-based-for-with-enum-classes
enum TimerType { ID_INSERT, ID_REMOVE, ID_CONTAINS, ID_OTHERS };
// double-braces required in C++11 (not in C++14) - http://en.cppreference.com/w/cpp/container/array
const std::array<TimerType, 3> allTimerIDs
		{{TimerType::ID_INSERT, TimerType::ID_REMOVE, TimerType::ID_CONTAINS}};

extern std::ostream& operator<<(std::ostream& inout, const TimerType& v);


#endif /* SRC_BENCHMARK_COMMONTIMEDEFS_H_ */
