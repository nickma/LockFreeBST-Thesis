#ifndef EFFICIENT_LF_BST_SIMPLECSVWRITER_H
#define EFFICIENT_LF_BST_SIMPLECSVWRITER_H

#include <sstream>
#include <string>
#include <iostream>
#include <ostream>

// accessed 20160621
//  -- https://stackoverflow.com/questions/1134388/stdendl-is-of-unknown-type-when-overloading-operator/1134467#1134467
//  -- http://www.alejandrosegovia.net/2011/08/11/support-stdendl-in-custom-c-streams/
// this is the type of std::cout
typedef std::basic_ostream<char, std::char_traits<char> > coutType_t;
// this is the function signature of std::endl
typedef std::ostream& (*stdEndl_t)(std::ostream&);

/**
 * @usage: auto csvWriter = SimpleCSVWriter();
 *
 *	csvWriter << "test" << "test2" << "\n";
 *	csvWriter.writeToFile("/tmp/test.csv");
 */
class SimpleCSVWriter {
public:
    SimpleCSVWriter() {};

    std::stringstream outData;
    char seperator = ',';

    bool writeToFile(std::string fileName, bool append = false) const;

    //friend std::ostream& operator<<(std::ostream& inout, const SimpleCSVWriter& v);
    //friend SimpleCSVWriter& operator<<(SimpleCSVWriter& inout, const coutType_t& str);

    // define an operator<< to take in std::endl
    //friend SimpleCSVWriter& operator<<(SimpleCSVWriter& inout, const stdEndl_t);

    template <class T>
    SimpleCSVWriter& operator<<(const T& x) {
        std::stringstream concatStream;
        concatStream << x;
        auto outDataString = outData.str();

        if(!outDataString.empty() && outDataString.back()!='\n' && concatStream.str()!="\n")
            outData << seperator;
        outData << x;

        return *this;
    }
};

#endif //EFFICIENT_LF_BST_SIMPLECSVWRITER_H
