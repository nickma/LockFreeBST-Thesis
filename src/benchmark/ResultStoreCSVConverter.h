//
// Created by nickma on 18.07.16.
//

#ifndef EFFICIENT_LF_BST_RESULTSTORECSVCONVERTER_H
#define EFFICIENT_LF_BST_RESULTSTORECSVCONVERTER_H

#include <vector>

#include "SimpleCSVWriter.h"
#include "../testHelper/OperationResult.h"

class ResultStoreCSVConverter {
public:
    ResultStoreCSVConverter(SimpleCSVWriter& csvWriter);

    std::string getArchitecture();

    void attachHeaderToCSV();
    /**
     * attaches resultStore Data to a SimpleCSVWriter
     *
     * @var[in] resultStore expects a resultStore with results of just one Timer-OperationType in it
     */
    void attachSingleOPVectorToCSV(std::vector<TimedSingleOpResult> resultStore,
                                   const std::string operationSplitting,
                                   const unsigned int keyRange,
                                   const int threadCnt,
                                   const bool keepPositiveResultsOnly = true);

private:
    SimpleCSVWriter& _csvWriter;

    void calculateDurationValues(const std::vector<TimedSingleOpResult> &resultStore,
                                     duration_base &durationMinimum, duration_base &durationAvrg,
                                     duration_base &durationMaximum,
                                     const bool keepPositiveResultsOnly, int &operationsPerSecond) const;
};

#endif //EFFICIENT_LF_BST_RESULTSTORECSVCONVERTER_H
