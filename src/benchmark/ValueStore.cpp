#include "ValueStore.h"

#include "TimeMeasurement.h"

template<class T>
void OldValueStore<T>::push_back(const T& value) {
	measuredValues.push_back(value);
}

template<class T>
const size_t OldValueStore<T>::getSize() const {
	return measuredValues.size();
}
