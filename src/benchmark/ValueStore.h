#ifndef SRC_VALUESTORE_H_
#define SRC_VALUESTORE_H_

#include <stddef.h>
#include <list>
#include <vector>

#include "../LockFreeBST.h"

//http://stackoverflow.com/questions/2795023/c-template-typedef
template <class T>
using ValueStore = std::vector<T>;

/**
 * Every thread gets a TimerStore
 */
template<class T>
class OldValueStore {
	std::list<T> measuredValues;
public:
	void push_back(const T& value);

	const size_t getSize() const;

//	template<typename TT>
//	friend OldValueStore<TT> operator+(const OldValueStore<TT>& lhs, const OldValueStore<TT>& rhs);
};

////template <typename TT>
////extern ValueStore operator+(const ValueStore<TT>& lhs, const ValueStore<TT>& rhs);
//template<typename TT>
//OldValueStore<TT> operator+(const OldValueStore<TT>& lhs, const OldValueStore<TT>& rhs) {
//	OldValueStore<TT> store;
//	for(auto it = lhs.measuredValues.begin(); it != rhs.measuredValues.end(); ++it) {
//		store.add(*it);
//	}
//	for(auto it = rhs.measuredValues.begin(); it != rhs.measuredValues.end(); ++it) {
//		store.add(*it);
//	}
//	return store;
//}

#endif /* SRC_VALUESTORE_H_ */
