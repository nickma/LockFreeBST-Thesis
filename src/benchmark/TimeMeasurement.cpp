#include "TimeMeasurement.h"

#include <fstream>
#include <iostream>

StartStopTimer::StartStopTimer()
{ }

void StartStopTimer::_invalidateResult() {
	_duration = invalid_duration;
}

void StartStopTimer::_setTimerRunningState(const bool &nextState) {
	/* consistency check */
	if(_measurementRunning == nextState)
		throw std::runtime_error(
				std::string("cant set the clock twice to the same value: ") +
				std::string(nextState?"1":"0") );

	_measurementRunning = nextState;
}

void StartStopTimer::start() {
	_setTimerRunningState(true);
	_invalidateResult();

	_startTime = std::chrono::high_resolution_clock::now();
}

void StartStopTimer::stop() {
	auto tmpTimeNow = std::chrono::high_resolution_clock::now();
	_duration = (tmpTimeNow - _startTime);

	_setTimerRunningState(false);
}

const duration_base StartStopTimer::getDuration() const {
	return _duration;
}


//void StartStopTimer::outputToFile(const std::string &pathToFile) {
//	std::cout << "\n" << "going to open file '" << pathToFile
//				<< "' to write the timer value to it" << std::endl;
//
//	std::ofstream myfile;
//	myfile.open(pathToFile);
//
//	// print first line
//	myfile << "Duration" << std::endl;
//	myfile << _timespan.count() << std::endl;
//
//	// saving to the file
//	myfile.close();
//
//}
