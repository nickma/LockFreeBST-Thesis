//
// Created by nickma on 18.07.16.
//

#ifndef EFFICIENT_LF_BST_GENERATEVALUES_H
#define EFFICIENT_LF_BST_GENERATEVALUES_H

#include <vector>

/**
 * @usage: std::vector<int> elements = GenerateRandomValues::getIntVector(5, 100);
 */
class GenerateRandomValues {
public:
    static std::vector<int> getIntVector(const int targetElementCount, const int valueRange);

    static unsigned int countUniqueElements(const std::vector<int> notUniqueInput);
};

#endif //EFFICIENT_LF_BST_GENERATEVALUES_H
