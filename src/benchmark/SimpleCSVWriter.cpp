#include "SimpleCSVWriter.h"

#include <fstream>
#include <ios>

bool SimpleCSVWriter::writeToFile(std::string fileName, bool append) const {
    auto args = std::ios::out | (append ? std::ios::app : std::ios::trunc);

    std::ofstream file;
    file.open(fileName.c_str(), args);
    if(!file.is_open())
        return false;

    if(append)
        file << std::endl;
    file << outData.str();

    file.close();
    return file.good();
}

//SimpleCSVWriter& operator<<(SimpleCSVWriter& inout, const coutType_t& str) {
//    if(!inout.outData.str().empty())
//        inout.outData << inout.seperator;
//    inout.outData << str;
//    return inout;
//}

SimpleCSVWriter& operator<<(SimpleCSVWriter& inout, const stdEndl_t f) {
    f(inout.outData);
    return inout;
}