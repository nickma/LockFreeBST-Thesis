#include "ResultStoreCSVConverter.h"
#include <string>
#include <sstream>
#include <algorithm>
#include <cmath>

#define UNUSED(expr) do { (void)(expr); } while (0)

ResultStoreCSVConverter::ResultStoreCSVConverter(SimpleCSVWriter& csvWriter)
        : _csvWriter(csvWriter) {}

std::string ResultStoreCSVConverter::getArchitecture() {
    // drop a system call
    auto rc = std::system("uname -p > /tmp/tmpArchitecutreFile");
    UNUSED(rc);

    std::ostringstream ss;
    ss << std::ifstream("/tmp/tmpArchitecutreFile").rdbuf();

    std::string outString = ss.str();
    //trim the string (because the input file contains a \n at the end)
    outString.erase(std::find(outString.begin(), outString.end(), '\n'));

    return outString;
}

void ResultStoreCSVConverter::calculateDurationValues(const std::vector<TimedSingleOpResult> &resultStore,
                                                      duration_base &durationMinimum, duration_base &durationAvrg,
                                                      duration_base &durationMaximum,
                                                      const bool keepPositiveResultsOnly, int &operationsPerSecond) const {
    int cntSummedUpDuration = 0;
    int cntInvalidDuration = 0;
    duration_base durationSum{0};

    // calculate min, mean and max duration values
    for(auto& each: resultStore) {
        const auto duration = each.duration.duration;

        const bool duration_accepted = !each.exception && (duration != invalid_duration) && (!keepPositiveResultsOnly || each.rc);
        if (not duration_accepted) {
            //std::cout << "not valid dration value;"
            //        << "exception:" << (bool)each.exception
            //        << ",duration_valid:" << (duration == invalid_duration)
            //        << "rc:" << each.rc
            //        << std::endl;
            cntInvalidDuration++;
            continue;
        }

        durationMinimum = (duration < durationMinimum) ? duration : durationMinimum;
        durationMaximum = (duration > durationMaximum) ? duration : durationMaximum;

        //check if it doesn't exceed the duration_base::max()
        if(durationSum >=  duration_base::max() - duration)
            throw std::runtime_error("duration limit exceeded");

        cntSummedUpDuration++;
        durationSum += duration;
    }
    durationAvrg = durationSum / cntSummedUpDuration;// obtain avrg time needed

    //calc ops per second
    //int timeInSeconds = std::chrono::duration_cast<std::chrono::seconds>(durationSum).count();
    operationsPerSecond = (int)round(((double)cntSummedUpDuration / durationSum.count())*1E9);

    // debug output the results
    std::cout << "added " << cntSummedUpDuration << "/" << resultStore.size() << " duration values "
              << "(" << cntInvalidDuration << " invalid),"
              << " min:" << durationMinimum.count()
              << ",avrg:" << durationAvrg.count()
              << ",max:" << durationMaximum.count() << "[ns]"
              << ", sum:" << durationSum.count() << "[ns]"
              << ", opsPerSec:" << operationsPerSecond << std::endl;
}

void ResultStoreCSVConverter::attachHeaderToCSV() {
    _csvWriter << "threadCnt"     //give
            << "architecture"     //collect
            << "operation type"  //collect
            << "operation splitting" //give
            << "key range"     //give
            << "exceptioncnt"  //collect
            << "min time needed"   //collect
            << "avrg time needed"   //collect
            << "max time needed"   //collect
            << "ops per sec"
            << "\n";
}

void
ResultStoreCSVConverter::attachSingleOPVectorToCSV(std::vector<TimedSingleOpResult> resultStore,
                                                   const std::string operationSplitting,
                                                   const unsigned int keyRange,
                                                   const int threadCnt,
                                                   const bool keepPositiveResultsOnly) {
    if (resultStore.empty())
        return;

    _csvWriter << threadCnt;
    _csvWriter << this->getArchitecture();
    _csvWriter << resultStore[0].perationType;
    _csvWriter << operationSplitting
               << keyRange;

    // obtain exception cnt
    const int exception_cnt = std::count_if(resultStore.begin(), resultStore.end(),
                                   [](TimedSingleOpResult store) {return (bool)store.exception;});
    _csvWriter << exception_cnt;

    // obtain duration values
    duration_base durationMinimum{duration_base::max()};
    duration_base durationAvrg;
    duration_base durationMaximum{duration_base::min()};
    int operationsPerSecond = 0;
    calculateDurationValues(resultStore, durationMinimum, durationAvrg, durationMaximum, keepPositiveResultsOnly,
                            operationsPerSecond);

    _csvWriter << durationMinimum.count() << durationAvrg.count() << durationMaximum.count();
    _csvWriter << operationsPerSecond;
    _csvWriter << "\n";
}

