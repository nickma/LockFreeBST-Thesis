#ifndef SRC_TIMEVALUES_H_
#define SRC_TIMEVALUES_H_

#include <chrono>
#include <iostream>
#include <fstream> //file stream
#include <iomanip>
#include "commonTimeDefs.h"

class TimerValue {
public:
	duration_base	duration = invalid_duration;
	TimerType 		timerType = TimerType::ID_OTHERS;

	TimerValue(const duration_base &duration, const TimerType &type = TimerType::ID_OTHERS);

	virtual ~TimerValue() {};

	virtual void printValues();
	friend std::ostream& operator<<(std::ostream& inout, const TimerValue& v);
};
extern std::ostream& operator<<(std::ostream& inout, const TimerValue& v);


class ThreadTimerValue : public TimerValue {
	const unsigned int	threadID = 0;

	/* no standard constructor available*/
	ThreadTimerValue();
public:
	/**
	 * Constructor from an existing TimerValue object
	 */
	ThreadTimerValue(const TimerValue &tv, const unsigned int threadID);

	ThreadTimerValue(
			const duration_base& duration,
			const TimerType &type,
			const unsigned int threadID);

	virtual ~ThreadTimerValue() {};

	virtual void printValues();

	friend std::ostream& operator<<(std::ostream& os, const ThreadTimerValue& obj);
};
extern std::ostream& operator<<(std::ostream& os, const ThreadTimerValue& obj);


#endif /* SRC_TIMEVALUES_H_ */
