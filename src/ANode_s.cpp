#include <sstream>
#include "ANode_s.h"
#include "testHelper/optionalTraceF.h"

ANode_s::ANode_s(IDType k,
FlagNodePtr<ANode_t> leftCh, FlagNodePtr<ANode_t> rightCh,
ANodePtr backLink, ANodePtr preLink) noexcept :
k(k),
leftChild(leftCh), rightChild(rightCh),
backLink(backLink), preLink(preLink) {};

std::atomic<FlagNodePtr<ANode_t>>&
ANode_s::selectLRChild(const cmpRC_e dir) {
    if ((cmpRC_e::LESS != dir) && (cmpRC_e::GREATER != dir)) {
        std::stringstream tmpErrMsg;
	    tmpErrMsg << "dir value != LESS != GREATER is:" << (int)dir;
        const std::string errMsg = std::string(tmpErrMsg.str());
        tracef(errMsg.c_str());
        throw std::runtime_error(errMsg);
    }
    return (dir == cmpRC_e::LESS) ? leftChild : rightChild;
}

int ANode_s::getNodeCategory() {
    auto getRightChildOfLeftChildPtr = [this]() {
        return (ANodePtr)(leftChild.load().get()->rightChild.load().get());
    };

    //@TODO replace through pointer comparisons
    if (this->k.cmp(leftChild.load().get()->k) == cmpRC_e::EQUAL)
        return 1;
    else if (this->k.cmp(getRightChildOfLeftChildPtr()->k) == cmpRC_e::EQUAL)
        return 2;
    else
        return 3;
}

std::ostream& operator<<(std::ostream& inout, const ANode_s& v) {
    inout << v.k;
    return inout;
}