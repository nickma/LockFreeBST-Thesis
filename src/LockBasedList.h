#ifndef LockBasedList_H_
#define LockBasedList_H_

#include <cstdbool>
#include <set>
#include <mutex>

#include "ANode_s.h"
#include "BSTInterface.h"
#include "IDType.h"
#include "LockFreeBST.h"


class LockBasedList : public LockFreeBST {
public:


	virtual ~LockBasedList() {};

	LockBasedList() {};
	/**
	 * Deny calling the copy constructor because the internal
	 * data are not trivial. Handling could be implemented
	 * for future use.
	 */
	LockBasedList(const LockBasedList&) = delete;

	/**
	 * performs a high level contains check on
	 * the instances BST
	 * @return whether the element with IDType k
	 * 			is in the list or not
	 */
	virtual bool contains(const IDType k);

	/**
	 * performs a high level remove on
	 * the instances BST
	 * @return whether the element with IDType k
	 * 			has been removed successfully or not
	 */
	virtual bool remove(const IDType k);

	/**
	 * performs a high level add on
	 * the instances BST
	 * @return whether the element with IDType k
	 * 			has been added successfully or not
	 */
	virtual bool add(const IDType k);

	/**
	 * @return the pointer to the instances BST root[0]
	 */
	void* getRoot();
private:
	std::set<int> _container;
	std::mutex container_access_mutex;
};

#endif /* LockBasedList_H_ */
