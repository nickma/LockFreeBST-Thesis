
#include "MockedLockFreeBST.h"

#include <sstream>
#include <iomanip>

using ::testing::_;
using ::testing::Return;

static size_t MAXWIDTH = 25;

MockedLockFreeBST::MockedLockFreeBST() {
		ON_CALL(*this, casMock_FlagNodePtr(_, _, _, _,_))
				.WillByDefault(Return(RCTriple::USE_ORIGINAL_VALUE));
		ON_CALL(*this, casMock_ANodePtr(_, _, _, _,_))
				.WillByDefault(Return(RCTriple::USE_ORIGINAL_VALUE));
	}


bool MockedLockFreeBST::cas(std::atomic<FlagNodePtr<ANode_t>>& casObject,
		FlagNodePtr<ANode_t> expected,
		FlagNodePtr<ANode_t> desired,
		const IDType srcNodeID,
		const CASUId& position)
{
	FlagNodePtr<ANode_t> originalObject = casObject.load();
	FlagNodePtr<ANode_t> originalExpected = expected;

	auto internal_rc = casMock_FlagNodePtr(originalObject, expected, desired, srcNodeID.getInt(), position);
	bool rc;
	if (internal_rc == RCTriple::USE_ORIGINAL_VALUE) {
		rc = LockFreeBST::cas(casObject, expected, desired, srcNodeID, position);
	} else {
		rc = (internal_rc == RCTriple::TRUE) ? true : false;
	}

	const std::string rc_source = (internal_rc == RCTriple::USE_ORIGINAL_VALUE) ? "actual" : "mocked";
	std::stringstream outt;
	outt << "MockCAS-FlagNodePtr ... "
			<< "position:" << position << "," << std::setw(MAXWIDTH-getStringSize(position))
			<< " " << rc_source << " rc:" << std::boolalpha << rc
			<< ", thread:" << fi.getThisThreadAlias()
			<< ", object [k:" << std::setw(4) << srcNodeID << "]"
			<< "[was:" << originalObject
			<< ", is:" << casObject.load()
			<< ", expected:" << originalExpected
			<< ", desired:" << desired << "]"
			<< std::endl;
	std::cout << outt.str();
	return rc;
}

bool MockedLockFreeBST::cas(std::atomic<ANodePtr>& casObject,
		ANodePtr expected,
		ANodePtr desired,
		const IDType srcNodeID,
		const CASUId& position)
{
	ANodePtr originalObject = casObject.load();

	auto internal_rc = casMock_ANodePtr(originalObject, expected, desired, srcNodeID.getInt(), position);
	bool rc;
	if (internal_rc == RCTriple::USE_ORIGINAL_VALUE) {
		rc = LockFreeBST::cas(casObject, expected, desired, srcNodeID, position);
	} else {
		rc = (internal_rc == RCTriple::TRUE) ? true : false;
	}

	const std::string rc_source = (internal_rc == RCTriple::USE_ORIGINAL_VALUE) ? "actual" : "mocked (suppressed cas)";
	//Buffer the output first in order to not break it during multithreaded usage
	std::stringstream outt;
	outt << "MockCAS-ANodePtr    ... "
			<< "position:" << position << "," << std::setw(MAXWIDTH-getStringSize(position))
			<< " " << rc_source << " rc:" << std::boolalpha << rc
			<< ", thread:" << fi.getThisThreadAlias()
			<< ", object [k:" << std::setw(4) << srcNodeID << "]"
			<< std::left
			<< "[was:" << std::setw(5) << originalObject->k
			<< ", is:" << std::setw(5) << *casObject.load()
			<< ", expected:" << std::setw(5)  <<  *expected
			<< ", desired:"  << std::setw(5) << *desired << "]"
			<< std::endl;
	std::cout << outt.str();

	return rc;
}

