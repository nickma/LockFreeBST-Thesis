#ifndef SRC_TESTHELPER_MOCKEDLOCKFREEBST_H_
#define SRC_TESTHELPER_MOCKEDLOCKFREEBST_H_

#include <iostream>
#include <string>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../ANode_s.h"
#include "../FaultPos.h"
#include "../FlagNodePtr.h"
#include "../IDType.h"
#include "../LockFreeBST.h"

class MockedLockFreeBST : public LockFreeBST {
	/**
	 * Used to manipulate the returncode, and if so to
	 * disable the actual cas call
	 */
	enum class RCTriple {
		USE_ORIGINAL_VALUE,
		TRUE,
		FALSE
	};
public:
	MockedLockFreeBST();

	MOCK_CONST_METHOD5(casMock_FlagNodePtr,
			RCTriple(const FlagNodePtr<ANode_t>& casObject,
					 const FlagNodePtr<ANode_t>& expected,
					 const FlagNodePtr<ANode_t>& desired,
					 const int& srcNodeID_int,
					 const CASUId& position));
	MOCK_CONST_METHOD5(casMock_ANodePtr,
			RCTriple(const ANodePtr casObject,
					 const ANodePtr expected,
					 const ANodePtr desired,
					 const int& srcNodeID_int,
					 const CASUId& position));

	/**
	 * Mocking CAS wrapper
	 */
	virtual bool cas(std::atomic<FlagNodePtr<ANode_t>>& casObject,
				FlagNodePtr<ANode_t> expected,
				FlagNodePtr<ANode_t> desired,
				const IDType srcNodeID,
				const CASUId& position);


	virtual bool cas(std::atomic<ANodePtr>& casObject,
			ANodePtr expected,
			ANodePtr desired,
			const IDType srcNodeID,
			const CASUId& position);
};



#endif /* SRC_TESTHELPER_MOCKEDLOCKFREEBST_H_ */
