#include "TestBaseClasses.h"

void BasicEmptyBSTwithoutExpectation::extractFilledNodePtr(LockFreeBST &bst) {
    //extract node pointer for debugging reasons
        nodeNegInf = bst.getRoot();
        node6 = nodeNegInf->rightChild.load().get();
        if (!node6) throw std::runtime_error("node6 is missing, cant extract pointer");
        node2 = node6->leftChild.load().get();
        node8 = node6->rightChild.load().get();
        node1 = node2->leftChild .load().get();
        node3 = node2->rightChild.load().get();
        node7 = node8->leftChild .load().get();

        EXPECT_EQ(6, node6->k.getInt());
        EXPECT_EQ(2, node2->k.getInt());
        EXPECT_EQ(1, node1->k.getInt());
        EXPECT_EQ(3, node3->k.getInt());
        EXPECT_EQ(8, node8->k.getInt());
        EXPECT_EQ(7, node7->k.getInt());
    }


void BasicEmptyBSTwithoutExpectation::preFillBST(LockFreeBST &bst) {
    //disable stdout
    testing::internal::CaptureStdout();

    for(auto &it: containAfterSetup) {
        EXPECT_TRUE(bst.add(it));
        EXPECT_TRUE(bst.contains(it));
    }
    //enable stdout again
    testing::internal::GetCapturedStdout();
}

void BasicEmptyBSTwithoutExpectation::preFillGivenValues(LockFreeBST &bst, const std::vector<IDType> fillValues) {

    for(auto &it: fillValues) {
        std::cout << "filling tree with value:" << it.getInt() << std::endl;

        //disable stdout
        testing::internal::CaptureStdout();
        EXPECT_TRUE(bst.add(it));
        EXPECT_TRUE(bst.contains(it));
        //enable stdout again
        testing::internal::GetCapturedStdout();
    }
}

void BasicEmptyBSTwithoutExpectation::emptyBST(LockFreeBST &bst) {
    for(auto &it: containAfterSetup) {
        try {
            bst.remove(it);
        } catch (std::exception &e) {
            tracef("remove threw an exception: %s", e.what());
        }
    }
    bst.fi.reset();
}