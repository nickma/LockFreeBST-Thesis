#ifndef EFFICIENT_LF_BST_TESTBASECLASSES_H
#define EFFICIENT_LF_BST_TESTBASECLASSES_H

#include <limits>
#include <vector>
#include <gtest/gtest.h>
#include <gmock/gmock-generated-nice-strict.h>
#include "../ANode_s.h"
#include "../LockFreeBST.h"
#include "MockedLockFreeBST.h"

const int pos_inf = std::numeric_limits<int>::max();
const int neg_inf = std::numeric_limits<int>::min();

class BasicEmptyBSTwithoutExpectation : public testing::Test {
protected:
    /**
     *      6
     *    /   \
     *   2     8
     *  / \   /
     * 1   3 7
     */
    const std::vector<IDType> containAfterSetup {6,2,1,3,8,7};
    ANodePtr nodeNegInf, node6, node2, node1, node3, node8, node7;

    void extractFilledNodePtr(LockFreeBST &bst);

    void preFillBST(LockFreeBST &bst);

    void preFillGivenValues(LockFreeBST &bst, const std::vector<IDType> fillValues);

    void emptyBST(LockFreeBST &bst);
};

#endif //EFFICIENT_LF_BST_TESTBASECLASSES_H
