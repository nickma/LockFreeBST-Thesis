#ifndef SRC_TESTHELPER_MOCKMATCHER_H_
#define SRC_TESTHELPER_MOCKMATCHER_H_

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../ANode_s.h"
#include "../FlagNodePtr.h"

using ::testing::Field;
using ::testing::Property;

/**
 * a google matcher that matches a FlagNodePtr<ANode_t>.get()->k
 * against the given valueToBeMatched
 *
 * usage: auto m_desired_k = m_FlagNodePtr_k(rightDesiredK);
 */
::testing::Matcher<const FlagNodePtr<ANode_t>&>
m_FlagNodePtr_k(const int valueToBeMatched) {
			return Property(&FlagNodePtr<ANode_t>::get, //FlagNodePtr<..>.get()
					Field(&ANode_s::k, Property(&IDType::getInt, valueToBeMatched))
			);
}
::testing::Matcher<const FlagNodePtr<ANode_t>&>
m_FlagNodePtr_flag(const bool flagged) {
			return Property(&FlagNodePtr<ANode_t>::isFlagged, flagged);
}
::testing::Matcher<const FlagNodePtr<ANode_t>&>
m_FlagNodePtr_mark(const bool marked) {
			return Property(&FlagNodePtr<ANode_t>::isMarked, marked);
}
::testing::Matcher<const FlagNodePtr<ANode_t>&>
m_FlagNodePtr_threaded(const bool threaded) {
			return Property(&FlagNodePtr<ANode_t>::isThreaded, threaded);
}

::testing::Matcher<ANodePtr>
m_ANodePtr_k(const int valueToBeMatched) {
			return Field(&ANode_s::k, Property(&IDType::getInt, valueToBeMatched));
}

#endif /* SRC_TESTHELPER_MOCKMATCHER_H_ */
