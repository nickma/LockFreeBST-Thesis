#include <iostream>
#include "TraceF_RAII.h"
#include "optionalTraceF.h"

TraceFMethodCall::TraceFMethodCall(const char* functionName, const int threadAlias, const int depth) :
    functionName(functionName), threadAlias(threadAlias), depth(depth) {
    tracef("(%s:%d) d:%d called", functionName, threadAlias, depth);
}

TraceFMethodCall::~TraceFMethodCall() {
    tracef("(%s:%d) d:%d done", functionName, threadAlias, depth);
}