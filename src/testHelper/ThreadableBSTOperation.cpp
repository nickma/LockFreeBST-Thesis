#include "ThreadableBSTOperation.h"

#include <iterator>
#include <vector>
#include <functional>

#include "../benchmark/commonTimeDefs.h"
#include "../BSTInterface.h"
#include "../IDType.h"
#include "ThreadSpecificData.h"
#include "../benchmark/TimeMeasurement.h"

void
ThreadableBSTOperation::_haltAtStartIfNeeded(ThreadSpecificData &threadSpecificData, ThreadSharedData &input) {
	if (not input.haltCVOnStart)
		return;
	//std::cout << "Thread " << threadSpecificData.assignedThreadAlias << "is going to wait for mutex" << std::endl;

	std::unique_lock<std::mutex> lk(input.cv_m);
	auto timeout = std::chrono::system_clock::now() + std::chrono::seconds(10);
	input.cv.wait_until(lk, timeout);
}

void
ThreadableBSTOperation::_sleepALittleBit() {
	std::this_thread::sleep_for(std::chrono::microseconds(10));
}

void
ThreadableBSTOperation::_initThread(LockFreeBST &targetBST, ThreadSpecificData &threadSpecificData,
                                   ThreadSharedData &threadSharedData) {
	//following are the different delay types
	targetBST.fi.handle({CASUId::THREAD_STARTUP, CASPrefix::ANY});
	ThreadableBSTOperation::_sleepALittleBit();
	ThreadableBSTOperation::_haltAtStartIfNeeded(threadSpecificData, threadSharedData);

}

void
ThreadableBSTOperation::_addRemoveContains(const AddRemoveContainsSwitch arSwitch,
										   LockFreeBST &targetBST, ThreadSpecificData &input,
										   ThreadSharedData &globalInput,
										   ValueStore<TimedSingleOpResult> &resultStorage) {
	//startup wait for preparation of non threaded action
	//TODO e.g. wait for the mutex provider ThreadSpecificData::cv_m to get unlocked

	for(auto& it : input.kList) {
		TimedSingleOpResult tmpResult(TimerType::ID_OTHERS, input.assignedThreadAlias);
		tmpResult.k  = it;

		std::function<bool(const IDType)> fncToBeExecuted{nullptr};
		switch (arSwitch) {
			case AddRemoveContainsSwitch::ADD:
				tmpResult.perationType = TimerType::ID_INSERT;
				fncToBeExecuted = std::bind(&LockFreeBST::add, &targetBST, std::placeholders::_1);
				break;
			case AddRemoveContainsSwitch::REMOVE:
				tmpResult.perationType = TimerType::ID_REMOVE;
				fncToBeExecuted = std::bind(&LockFreeBST::remove, &targetBST, std::placeholders::_1);
				break;
			case AddRemoveContainsSwitch::CONTAINS:
				tmpResult.perationType = TimerType::ID_CONTAINS;
				fncToBeExecuted = std::bind(&LockFreeBST::contains, &targetBST, std::placeholders::_1);
				break;
			default:
				throw std::runtime_error("You suck");
				break;
		}

		// Starting time measurement
		StartStopTimer durationMeasurement;
		durationMeasurement.start();
		try {
			tmpResult.rc = fncToBeExecuted(it);
		} catch (std::exception &) {
			//catch and safe the exception
			std::cerr << "setting exception" << std::endl;
			globalInput.exceptionOccured = true;
			tmpResult.exception = std::current_exception();

		}

		durationMeasurement.stop();
		tmpResult.duration = durationMeasurement.getDuration();
		//propagate the measurements to the resultStore
		resultStorage.push_back(tmpResult);

		const bool exceptionDetected = globalInput.exceptionOccured;
		if(exceptionDetected) {
			std::cerr << "detected exception returning" << std::endl;
			return;
		}
	}
}

void
ThreadableBSTOperation::add(
		LockFreeBST& targetBST,
		ThreadSpecificData& threadInput,
		ThreadSharedData& globalInput,
		ValueStore<TimedSingleOpResult>& resultStorage) {
	ThreadableBSTOperation::_initThread(targetBST, threadInput, globalInput);

	ThreadableBSTOperation::_addRemoveContains(AddRemoveContainsSwitch::ADD, targetBST, threadInput, globalInput, resultStorage);
}
void
ThreadableBSTOperation::remove(
		LockFreeBST& targetBST,
		ThreadSpecificData& threadInput,
		ThreadSharedData& globalInput,
		ValueStore<TimedSingleOpResult>& resultStorage) {
	ThreadableBSTOperation::_initThread(targetBST, threadInput, globalInput);

	ThreadableBSTOperation::_addRemoveContains(AddRemoveContainsSwitch::REMOVE, targetBST, threadInput, globalInput, resultStorage);
}

void
ThreadableBSTOperation::contains(
		LockFreeBST& targetBST,
		ThreadSpecificData& threadInput,
		ThreadSharedData& globalInput,
		ValueStore<TimedSingleOpResult>& resultStorage) {
	ThreadableBSTOperation::_initThread(targetBST, threadInput, globalInput);

	ThreadableBSTOperation::_addRemoveContains(AddRemoveContainsSwitch::CONTAINS, targetBST, threadInput, globalInput, resultStorage);
}