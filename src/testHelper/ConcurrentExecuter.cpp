#include "ConcurrentExecuter.h"

#include <iterator>
#include <stdexcept>

#include "ThreadableBSTOperation.h"
#include "../benchmark/GenerateRandomValues.h"


ConcurrentExecuter::ConcurrentExecuter(LockFreeBST& bst, const size_t& threadCount) :
		bst(bst), _threadCount(threadCount) {
	//if (_threadCount == 0)
	//	throw new std::invalid_argument("threadCount is null, which is prohibited");

}

std::vector<TimedSingleOpResult>
ConcurrentExecuter::_threadsAddRemoveSameValues(const AddRemoveContainsSwitch arSwitch,
		const std::vector<IDType>& kList) {
	// prepare shared and specific input Data
	ThreadSharedData threadWideInputData;
	ThreadsInputDataManager threadsInputData(_threadCount);
	// prepare output data store
	//a vector for all the threads and another one for results
	std::vector<std::vector<TimedSingleOpResult>> threadsResultStores(_threadCount);

	//run the threads
	std::vector<std::thread> threads(_threadCount);
	for (size_t i=0; i < _threadCount; ++i) {
		//prepare threads input data
		auto &inputData = threadsInputData.at(i);
		inputData.assignedThreadAlias = i;
		inputData.kList = kList; //_stretchKList(_threadCount, kList);
		//prepare threads value store
		auto &resultStorage = threadsResultStores.at(i);

		//spawn threads and execute
		std::cout << "Spawning thread:" << i << " with " << inputData.kList.size() << "entry/ies in kList" << std::endl;
		bst.fi.handle({CASUId::PRE_THR_STARTUP,CASPrefix::ANY}, i);
		switch(arSwitch) {
		case AddRemoveContainsSwitch::ADD:
			threads[i] = std::thread(ThreadableBSTOperation::add,
					std::ref(bst), std::ref(inputData), std::ref(threadWideInputData), std::ref(resultStorage));
			break;
		case AddRemoveContainsSwitch::REMOVE:
			threads[i] = std::thread(ThreadableBSTOperation::remove,
					std::ref(bst), std::ref(inputData), std::ref(threadWideInputData), std::ref(resultStorage));
			break;
		case AddRemoveContainsSwitch::CONTAINS:
			threads[i] = std::thread(ThreadableBSTOperation::contains,
									 std::ref(bst), std::ref(inputData), std::ref(threadWideInputData), std::ref(resultStorage));
			break;
		default:
			std::cerr << "AddRemoveValue is: " << (int)arSwitch << std::endl;
				throw std::runtime_error("AddRemoveSwitch value not supported");
		}

		//register the new thread at the faultInjector
		bst.fi.registerThreadAlias(threads[i].get_id(), i);
	}

	//let them do their job
	this->_waitThreadsDone(threads);
	return compactResultstores(threadsResultStores);
}

std::vector<TimedSingleOpResult>
ConcurrentExecuter::threadsRemoveEveryoneDifferentValues(const std::vector<std::vector<IDType>> &kList) {
	// prepare shared and specific input Data
	ThreadSharedData threadWideInputData;
	ThreadsInputDataManager threadsInputData(_threadCount);
	// prepare output data store
	//a vector for all the threads and another one for results
	std::vector<std::vector<TimedSingleOpResult>> threadsResultStores(_threadCount);

	//run the threads
	std::vector<std::thread> threads(_threadCount);
	for (size_t i=0; i < _threadCount; ++i) {
		//prepare threads input data
		auto &inputData = threadsInputData.at(i);
		inputData.assignedThreadAlias = i;
		inputData.kList = kList[i];
		//prepare threads value store
		auto &resultStorage = threadsResultStores.at(i);

		//spawn threads and execute
		std::cout << "Spawning thread:" << i << " with " << inputData.kList.size() << "entry/ies in kList" << std::endl;
		bst.fi.handle({CASUId::PRE_THR_STARTUP,CASPrefix::ANY}, i);
		threads[i] = std::thread(ThreadableBSTOperation::remove,
			 std::ref(bst), std::ref(inputData), std::ref(threadWideInputData), std::ref(resultStorage));

		//register the new thread at the faultInjector
		bst.fi.registerThreadAlias(threads[i].get_id(), i);
	}

	//let them do their job
	this->_waitThreadsDone(threads);
	return compactResultstores(threadsResultStores);
}

std::vector<TimedSingleOpResult> ConcurrentExecuter::compactResultstores(
        const std::vector<std::vector<TimedSingleOpResult>> &threadsResultStores) const {// accumulate results
	std::vector<TimedSingleOpResult> resultingStore;
	for (auto it: threadsResultStores) {
		resultingStore.insert(resultingStore.end(), it.begin(), it.end());
	}
	std::cout << "Results threadStore:" << threadsResultStores.size()
	<< ", with " << resultingStore.size() << " element(s)" << std::endl;
	return resultingStore;
}

ValueStore<TimedSingleOpResult>
ConcurrentExecuter::addAndLocateRandom_withkeyUpperRange(const std::vector<IDType> &kListAdd,
														 const std::vector<IDType> &kListContains,
														 const std::vector<IDType> &kListRemove,
														 unsigned int addThreadCnt,
														 unsigned int containsThreadCnt, unsigned int removeThreadCnt,
														 const unsigned int keyRangeUpper) {
	//check the given parameters
	if (!addThreadCnt && !containsThreadCnt && !removeThreadCnt) {
		throw std::runtime_error("please provide at least a single thread count");
	}
	const size_t threadCntSum = addThreadCnt + containsThreadCnt + removeThreadCnt;
	const bool haltAllThreadsOnStart = true;


	// prepare shared and specific input Data
	ThreadSharedData threadWideInputData;
	if (haltAllThreadsOnStart)
		threadWideInputData.initHaltOnStart();
	ThreadsInputDataManager threadsInputData(threadCntSum);
	// prepare output data store
	//a vector for all the threads and another one for results
	std::vector<std::vector<TimedSingleOpResult>> threadsResultStores(threadCntSum);

	//run the threads
	std::vector<std::thread> threads(threadCntSum);
	AddRemoveContainsSwitch nextThreadToBeregistered = AddRemoveContainsSwitch::REMOVE;
	for (size_t i=0; i < threadCntSum; ++i) {
		//state transition
		switch (nextThreadToBeregistered) {
			case AddRemoveContainsSwitch::ADD:
				if (containsThreadCnt > 0) {
					nextThreadToBeregistered = AddRemoveContainsSwitch::CONTAINS;
				} else if (removeThreadCnt > 0) {
					nextThreadToBeregistered = AddRemoveContainsSwitch::REMOVE;
				}
				break;
			case AddRemoveContainsSwitch::CONTAINS:
				if (addThreadCnt > 0) {
					nextThreadToBeregistered = AddRemoveContainsSwitch::ADD;
				} else if (removeThreadCnt > 0) {
					nextThreadToBeregistered = AddRemoveContainsSwitch::REMOVE;
				}
				break;
			case AddRemoveContainsSwitch::REMOVE:
				if (addThreadCnt > 0) {
					nextThreadToBeregistered = AddRemoveContainsSwitch::ADD;
				} else if (containsThreadCnt > 0) {
					nextThreadToBeregistered = AddRemoveContainsSwitch::CONTAINS;
				}
				break;
			default:
				std::cerr << "nextThreadToBeRegistered unknown" << std::endl;
				break;
		}

		//prepare threads input data
		auto &inputData = threadsInputData.at(i);
		inputData.assignedThreadAlias = i;
		//prepare threads value store
		auto &resultStorage = threadsResultStores.at(i);
		//generate the keyRange
        std::vector<int> tmpMillionElements = GenerateRandomValues::getIntVector(1E4, keyRangeUpper);
        std::vector<IDType> millionIDTypes;
        millionIDTypes.insert(millionIDTypes.begin(), tmpMillionElements.begin(), tmpMillionElements.end());
        inputData.kList = millionIDTypes;

		//spawn threads and execute
		bst.fi.handle({CASUId::PRE_THR_STARTUP,CASPrefix::ANY}, i);

		switch (nextThreadToBeregistered) {
			case AddRemoveContainsSwitch::ADD:
				//inputData.kList = kListAdd;
				threads[i] = std::thread(ThreadableBSTOperation::add,
										 std::ref(bst), std::ref(inputData), std::ref(threadWideInputData),
										 std::ref(resultStorage));
				addThreadCnt--;
				break;
			case AddRemoveContainsSwitch::CONTAINS:
				//inputData.kList = kListContains;
				threads[i] = std::thread(ThreadableBSTOperation::contains,
										 std::ref(bst), std::ref(inputData), std::ref(threadWideInputData),
										 std::ref(resultStorage));
				containsThreadCnt--;
				break;
			case AddRemoveContainsSwitch::REMOVE:
				//inputData.kList = kListRemove;
				threads[i] = std::thread(ThreadableBSTOperation::remove,
										 std::ref(bst), std::ref(inputData), std::ref(threadWideInputData),
										 std::ref(resultStorage));
				removeThreadCnt--;
				break;
		}
		//register the new thread at the faultInjector
        std::cout << "Spawning thread:" << i << " with " << inputData.kList.size() << "entry/ies in kList" << std::endl;
		bst.fi.registerThreadAlias(threads[i].get_id(), i);
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	threadWideInputData.golbalReleaseHalt();

	//let them do their job
	this->_waitThreadsDone(threads);
	return compactResultstores(threadsResultStores);
}

std::vector<TimedSingleOpResult>
ConcurrentExecuter::everyoneAddsSame(const std::vector<IDType>& kList) {
	auto resultingStore = _threadsAddRemoveSameValues(AddRemoveContainsSwitch::ADD, kList);
	return resultingStore;
}

std::vector<TimedSingleOpResult>
ConcurrentExecuter::everyoneRemovesSame(const std::vector<IDType>& kList) {
	auto resultingStore = _threadsAddRemoveSameValues(AddRemoveContainsSwitch::REMOVE, kList);
	return resultingStore;
}
ValueStore<TimedSingleOpResult>
ConcurrentExecuter::everyoneLocatesSame(const std::vector<IDType>& kList) {
	auto resultingStore = _threadsAddRemoveSameValues(AddRemoveContainsSwitch::CONTAINS, kList);
	return resultingStore;
}


void
ConcurrentExecuter::_waitThreadsDone(std::vector<std::thread>& threads) {
	for(auto &it : threads) {
		const auto threadAlias = bst.fi.getThreadAlias(it.get_id());
		if (not it.joinable())
			throw std::runtime_error("Thead not joinable");
		it.join();
		std::cout << "joined thread:" << threadAlias << std::endl;
	}
}
std::vector<IDType>
ConcurrentExecuter::_stretchKList(const size_t fillCount, const std::vector<IDType>& kList) {
	std::vector<IDType> ioData(fillCount,0);
	for (size_t i=0; i < fillCount; ++i) {
		ioData.at(i) = kList.at(i % kList.size());
	}
	return ioData;
}
