//
// Created by nickma on 11.04.16.
//
#include <iostream>
#include "PerThreadCounter.h"

void PerThreadCounter::_instanciateIfNeeded(const int &threadAliasRequested) {
    int neededAllocations = 1 + threadAliasRequested - _threadCounters.size();
    if(neededAllocations <= 0)
        return;

    std::lock_guard<std::mutex> lk(vectorAddMutex);
    // another thread safe size request please
    neededAllocations = 1 + threadAliasRequested - _threadCounters.size();
    for(int i=0; i<neededAllocations; ++i)
        _threadCounters.push_back(0);
}

int PerThreadCounter::inc(int threadAlias) {
    threadAlias++;
    _instanciateIfNeeded(threadAlias);
    return ++_threadCounters[threadAlias];
}

int PerThreadCounter::dec(int threadAlias) {
    threadAlias++;
    _instanciateIfNeeded(threadAlias);
    return --_threadCounters[threadAlias];
}

int PerThreadCounter::get(int threadAlias) const {
    threadAlias++;
    const int neededAllocations = 1 + threadAlias - _threadCounters.size();
    if(neededAllocations > 0)
        return 0;
    return _threadCounters[threadAlias];
}


void PerThreadCounter::resetAll() {
    _threadCounters.clear();
}
