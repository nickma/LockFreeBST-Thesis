#ifndef SRC_CONCURRENTEXECUTER_H_
#define SRC_CONCURRENTEXECUTER_H_

#include <stddef.h>
#include <vector>
#include <thread>
#include <array>

#include "../benchmark/ValueStore.h"
#include "../IDType.h"
#include "OperationResult.h"
#include "ThreadableBSTOperation.h"
#include "ThreadSpecificData.h"

class BSTInterface;


/**
 * The everyone<doesThat>(..) methods are the main interface
 */
class ConcurrentExecuter {
public:
	ConcurrentExecuter() = delete;
	ConcurrentExecuter(LockFreeBST& bst, const size_t& threadCount);

	/**
	 * runX methods are executing the called operation X
	 * 		on the given bst, with the elements given by
	 * 		kList
	 */
	ValueStore<TimedSingleOpResult>
	everyoneAddsSame(const std::vector<IDType>& kList);
	ValueStore<TimedSingleOpResult>
	everyoneRemovesSame(const std::vector<IDType>& kList);
	ValueStore<TimedSingleOpResult>
	everyoneLocatesSame(const std::vector<IDType>& kList);

	ValueStore<TimedSingleOpResult>
	addAndLocateRandom_withkeyUpperRange(const std::vector<IDType> &kListAdd, const std::vector<IDType> &kListContains,
                                         const std::vector<IDType> &kListRemove, unsigned int addThreadCnt,
                                         unsigned int containsThreadCnt, unsigned int removeThreadCnt,
                                         const unsigned int keyRangeUpper);

	/**
	 * Every threads gets his own std::vector<IDType> filled with a kList of
	 * to be removed k's, resulting in a std::vector<std::vector<IDType>>
	 */
	std::vector<TimedSingleOpResult>
			threadsRemoveEveryoneDifferentValues(const std::vector<std::vector<IDType>> &kList);
private:
	LockFreeBST& bst;
	const size_t _threadCount;

	/**
	 * blocking waits untill the threads are joined
	 */
	void _waitThreadsDone(std::vector<std::thread>& threads);

	/**
	 * prepares the reference wrapper bst.add, bst.remove, bst.contains
	 * maybe not needed anymore due to reconstruction 20151012
	 */
	void _prepareBSTWrapper(BSTInterface& bst);

	/**
	 * fills the output depending on the size of the fillCounter and on the
	 * inbound kList, in a way s.t. the returned list is of size fillCount
	 * and contains no elements not already existing in the inbound list.
	 *
	 * Therefore it rotate over the existing elements in kList and
	 *  inserts them repeatedly into the returned list.
	 * e.g. fillCounter==5, kList=={1,2} -> returnValue={1,2,1,2,1}
	 */
	std::vector<IDType>
	_stretchKList(const size_t fillCount, const std::vector<IDType>& kList);

	ValueStore<TimedSingleOpResult>
	_threadsAddRemoveSameValues(const AddRemoveContainsSwitch arSwitch, const std::vector<IDType>& kList);

	std::vector<TimedSingleOpResult> compactResultstores(
            const std::vector<std::vector<TimedSingleOpResult>> &threadsResultStores) const;
};


#endif /* SRC_CONCURRENTEXECUTER_H_ */
