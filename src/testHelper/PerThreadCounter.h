#ifndef EFFICIENT_LF_BST_PERTHREADCOUNTER_H
#define EFFICIENT_LF_BST_PERTHREADCOUNTER_H

#include <memory>
#include <vector>
#include <mutex>

/**
 * Automatically allocates the needed space
 * intended for a continuous threadAlias space
 */
class PerThreadCounter {
    std::mutex vectorAddMutex;

    std::vector<int> _threadCounters = std::vector<int>(20,0);

    void _instanciateIfNeeded(const int &threadAliasRequested);
public:
    /**
     * @return increments and returns
     */
    int inc(int threadAlias);

    /**
     *  @return decrements and returns
     */
    int dec(int threadAlias);

    int get(int threadAlias) const;

    void resetAll();
};

#endif //EFFICIENT_LF_BST_PERTHREADCOUNTER_H
