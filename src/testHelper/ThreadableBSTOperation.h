/*
 * ThreadableBSTOperation.h
 *
 *  Created on: Oct 18, 2015
 *      Author: nickma
 */

#ifndef SRC_TESTHELPER_THREADABLEBSTOPERATION_H_
#define SRC_TESTHELPER_THREADABLEBSTOPERATION_H_

#include <cstdint>

#include "../benchmark/ValueStore.h"
#include "OperationResult.h"
#include "ThreadSpecificData.h"

struct ThreadSpecificData;

enum class AddRemoveContainsSwitch : uint8_t {
	ADD,
	REMOVE,
	CONTAINS
};

/**
 * These methods are supposed to be called due to a
 * thread creation (e.g. std::thread(ThreadableBSTOperation::add,...))
 *
 * They provide the given data to the add/remove operation selected
 */
class ThreadableBSTOperation {
public:
	/**
	 * each of the IO-data have to stay an a memory
	 *   s.t. the thread doesn't have to take precautions
	 *   to write to it (e.g. single allocated memory for
	 *   each thread)
	 */
	static void add(
			LockFreeBST& targetBST,
			ThreadSpecificData& threadInput,
			ThreadSharedData& globalInput,
			ValueStore<TimedSingleOpResult>& resultStorage);
	static void remove(
			LockFreeBST& targetBST,
			ThreadSpecificData& threadInput,
			ThreadSharedData& globalInput,
			ValueStore<TimedSingleOpResult>& resultStorage);
	static void contains(
			LockFreeBST& targetBST,
			ThreadSpecificData& threadInput,
			ThreadSharedData& globalInput,
			ValueStore<TimedSingleOpResult>& resultStorage);
private:
	/**
	 * cheap version of giving the thread creation a little bit of time
	 * to hand the threads alias's to the faultInjector
	 */
	static void _sleepALittleBit();

	/**
	 * waiting halts a thread if ThreadSpecificData::waitCVOnStart is set to true
	 */
	static void _haltAtStartIfNeeded(ThreadSpecificData &threadSpecificData, ThreadSharedData &input);

	/**
	 * does everything needed at startup of a newly created thread
	 */
	static void _initThread(LockFreeBST &targetBST, ThreadSpecificData &threadSpecificData,
							ThreadSharedData &threadSharedData);

	/**
	 * method actually calling add/remove on the data structure.
	 * @var[in] arSwitch switch for selecting the BST operation
	 */
	static void _addRemoveContains(
			const AddRemoveContainsSwitch arSwitch,
			LockFreeBST &targetBST,
			ThreadSpecificData &input,
			ThreadSharedData &globalInput,
			ValueStore<TimedSingleOpResult> &resultStorage);
};

#endif /* SRC_TESTHELPER_THREADABLEBSTOPERATION_H_ */
