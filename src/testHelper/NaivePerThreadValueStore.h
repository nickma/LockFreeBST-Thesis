#ifndef EFFICIENT_LF_BST_NAIVEPERTHREADVALUESTORE_H
#define EFFICIENT_LF_BST_NAIVEPERTHREADVALUESTORE_H

#include <vector>
#include <mutex>

template<class T>
class PerThreadValueStore {
    const T _defaultValue;
    std::map<int, T> _valueContainer;

    void _threadSafeInstanciate(const int &key) {
        static std::mutex _vectorAddMutex;
        std::lock_guard<std::mutex> lk(_vectorAddMutex);
        // another thread safe size request please
        _valueContainer[key] = _defaultValue;
    }

    inline T& _autoAllocGetValue(const int &key) {
        auto search = _valueContainer.find(key);
        if(search != _valueContainer.end()) {
            return search->second;
        }

        _threadSafeInstanciate(key);
        return _valueContainer[key];
    }
public:
    PerThreadValueStore(const PerThreadValueStore<T>& src) :
            _defaultValue(src._defaultValue), _valueContainer(src._valueContainer){
    }

    PerThreadValueStore(const size_t initSize, const T defaultValue) :
            _defaultValue(defaultValue) {

        for(size_t keyIt=0; keyIt<initSize; ++keyIt)
            _valueContainer[keyIt]=defaultValue;
    }

    void set(const int key, const T value) {
        _autoAllocGetValue(key) = value;
    }

    // get does not auto allocate
    const T get(const int key) const {
        auto search = _valueContainer.find(key);
        if(search == _valueContainer.end())
            return _defaultValue;

        return _valueContainer.at(key);
    }

    const size_t size() const {
        return _valueContainer.size();
    }
};

#endif //EFFICIENT_LF_BST_NAIVEPERTHREADVALUESTORE_H
