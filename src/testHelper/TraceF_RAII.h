#ifndef EFFICIENT_LF_BST_TRACEF_RAII_H
#define EFFICIENT_LF_BST_TRACEF_RAII_H

class TraceFMethodCall {
    const char* functionName;
    const int threadAlias;
    const int depth;
public:
    TraceFMethodCall(const char* functionName, const int threadAlias, const int depth);

    ~TraceFMethodCall();
};

#endif //EFFICIENT_LF_BST_TRACEF_RAII_H
