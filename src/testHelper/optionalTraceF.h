//
// Created by nickma on 11.04.16.
//

#ifndef EFFICIENT_LF_BST_OPTIOINALTRACEF_H
#define EFFICIENT_LF_BST_OPTIOINALTRACEF_H

#ifdef ENABLE_LTTNG
#include <lttng/tracef.h>
#else
#include <stdarg.h>
#define tracef(...)
#endif

#endif //EFFICIENT_LF_BST_OPTIOINALTRACEF_H
