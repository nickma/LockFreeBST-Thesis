#ifndef SRC_TESTHELPER_THREADINPUTDATA_H_
#define SRC_TESTHELPER_THREADINPUTDATA_H_

#include <stddef.h>
#include <vector>
#include <stddef.h>
#include <condition_variable>
#include <memory>
#include <atomic>

#include "../IDType.h"

struct ThreadSpecificData {
	unsigned int assignedThreadAlias;
	std::vector<IDType> kList;


};

class ThreadsInputDataManager {
	std::vector<ThreadSpecificData> _inputData;
public:
	ThreadsInputDataManager(const size_t dataSize) : _inputData(dataSize) {}

	/**
	 * returns a reference to the object at position pos
	 */
	ThreadSpecificData& at(const size_t& pos);
};

/**
 * 	These data are the same for every Thread
 */
struct ThreadSharedData {
	// startup delay locking stuff //TODO move to child of ThreadSpecificData
	std::atomic<bool> haltCVOnStart{false};  //!< activates the conditional variable halting for the ThreadableBSTOps
	std::condition_variable cv; //!< notify channel (same for every thread)
	std::mutex cv_m; //!< mutex provider (same for every thread)

	//!< an exception occured in *any* of the threads
	std::atomic<bool> exceptionOccured{false};

	void initHaltOnStart();
	void golbalReleaseHalt();
};


#endif /* SRC_TESTHELPER_THREADINPUTDATA_H_ */
