/*
 * OperationResult.h
 *
 *  Created on: Oct 18, 2015
 *      Author: nickma
 */

#ifndef SRC_TESTHELPER_OPERATIONRESULT_H_
#define SRC_TESTHELPER_OPERATIONRESULT_H_

#include <cstdbool>

#include "../benchmark/commonTimeDefs.h"
#include "../benchmark/TimeValues.h"
#include "../IDType.h"

// Thread IO datastructures
struct SingleOpResult {
	TimerType perationType;
	unsigned int threadID = 0;

	//!< k which have been executed
	IDType k{0};
	//!< overall function return code
	bool rc = false;
	//!< exception if one occured
	std::exception_ptr exception;

public:
	SingleOpResult(const TimerType &testOperation, const unsigned int& threadID);

	void setKandRC(const IDType& k, const bool& rc);
};

struct TimedSingleOpResult : public SingleOpResult {
    //@TODO rename duration to timer
	TimerValue duration{invalid_duration}; //!< time taken for the action

	TimedSingleOpResult(const TimerType &testOperation, const unsigned int& threadID) :
			SingleOpResult(testOperation, threadID) { }

	friend std::ostream& operator<<(std::ostream& inout, const TimedSingleOpResult& v);
};



#endif /* SRC_TESTHELPER_OPERATIONRESULT_H_ */
