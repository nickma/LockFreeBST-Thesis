/*
 * ThreadInputData.cpp
 *
 *  Created on: Oct 18, 2015
 *      Author: nickma
 */


#include "ThreadSpecificData.h"

#include <stdexcept>

ThreadSpecificData&
ThreadsInputDataManager::at(const size_t& pos) {
	return this->_inputData.at(pos);
}

void ThreadSharedData::golbalReleaseHalt() {
	if (!haltCVOnStart)
		return;
	std::cout << "notifying all to start their concurrent work" << std::endl;
	cv.notify_all();
}

void ThreadSharedData::initHaltOnStart() {
	haltCVOnStart = true;

}
