#include "OperationResult.h"


SingleOpResult::SingleOpResult(const TimerType &testOperation,
		const unsigned int& threadID) :
		perationType(testOperation), threadID(threadID) {
}

void SingleOpResult::setKandRC(const IDType& k, const bool& rc) {
	this->k = k;
	this->rc = rc;
}


std::ostream& operator<<(std::ostream& inout, const TimedSingleOpResult& v) {
	inout << "thrAlias:" << v.threadID << ", rc:" << v.rc << ", duration:" << v.duration << ", exception:" << (bool)v.exception;

	return inout;
}
