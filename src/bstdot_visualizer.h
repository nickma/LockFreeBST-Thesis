/*
 * bst_visualizer.h
 *
 *  Created on: 10 Sep 2015
 *      Author: nickma
 */

#ifndef BST_VISUALIZER_H_
#define BST_VISUALIZER_H_

#include <string>
#include <set>

#include "ANode_s.h"
#include "BSTVisualizerInterface.h"

/**
 * @see https://en.wikipedia.org/wiki/File:Huffman_%28To_be_or_not_to_be%29.svg
 * @see http://eli.thegreenplace.net/2009/11/23/visualizing-binary-trees-with-graphviz/
 */
class BSTDOTVisualizer : public BSTVisualizerInterface {
	std::set<ANodePtr> nodesList;

	static std::string getDOTNodeOutgoingOnly(ANodePtr node);

	static void
	bstToList(std::set<ANodePtr>& inOutList, ANodePtr curr);
public:
	virtual ~BSTDOTVisualizer() {};

	virtual void readInBst(const ANodePtr bstRoot0);

	virtual void exportToFile(const std::string pathAndFilename) const;
};

#endif /* BST_VISUALIZER_H_ */
