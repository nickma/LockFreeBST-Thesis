#include "LockFreeBSTHelper.h"

#include <iostream>
#include <stdexcept>
#include <ostream>

#include "testHelper/TraceF_RAII.h"
#include "FlagNodePtr.hpp"

#define UNUSED(expr) do { (void)(expr); } while (0)

bool LockFreeBSTHelper::cas(std::atomic<FlagNodePtr<ANode_t>>& casObject,
		FlagNodePtr<ANode_t> expected, FlagNodePtr<ANode_t> desired,
		const IDType, const CASUId& casPos) {

	fi.handle({casPos, CASPrefix::PRE_CAS});
	const auto rc = casObject.compare_exchange_strong( expected, desired );
	fi.handle({casPos, CASPrefix::POST_CAS});
	return rc;
}
bool LockFreeBSTHelper::cas(std::atomic<ANodePtr>& casObject,
		ANodePtr expected, ANodePtr desired,
		const IDType, const CASUId& casPos) {
	fi.handle({casPos, CASPrefix::PRE_CAS});
	const auto rc = casObject.compare_exchange_strong( expected, desired );
	fi.handle({casPos, CASPrefix::POST_CAS});
	return rc;
}



cmpRC_e
LockFreeBSTHelper::locate(ANodePtr& prev, ANodePtr& curr, const EpsilonedIDType k) {
	const int threadAlias = fi.getThisThreadAlias();
	std::unique_ptr<TraceFMethodCall> traceFLog;
	if(_globalDebugManager.is_enContentionLogging()) {
		traceFLog.reset(new TraceFMethodCall(__func__, threadAlias, _globalDebugManager.getRecursionCounter(threadAlias)));
	}

	while (true) {
		cmpRC_e dir = cmp(k, curr->k);
		if (dir == cmpRC_e::EQUAL) {
			return dir;
		}

		auto nonAtomicNodePtr = curr->selectLRChild(dir).load(); //atomic load
		auto	   R = nonAtomicNodePtr.get();
		const bool m = nonAtomicNodePtr.isMarked();
		const bool t = nonAtomicNodePtr.isThreaded();
		if(_globalDebugManager.is_enContentionLogging()) {
			tracef("(%s_0:%d) prev:%d, curr:%d, epsilon:%d, dir:%d, R:%d, m:%d, t:%d",
				   __func__, threadAlias,
				   prev->k.getInt(), curr->k.getInt(), k._e, dir, R->k.getInt(), m, t);
		}
		/* If eager helping is required then help cleaning
		 * the current node if its right child pointer has been
		 * marked.
		 */
		if ((m == true) and (dir == cmpRC_e::GREATER)) {
			auto newPrev = prev->backLink.load();
			cleanMark(curr, dir); // paper says cleanMarkEd(prev, curr, dir); //not paper conform
			prev = newPrev;
			cmpRC_e pDir = cmp(k, prev->k);
			auto nonAtomicNodePtr = prev->selectLRChild(pDir).load(); //atomic load
			curr = nonAtomicNodePtr.get();
			continue;
		}
		if (t) {
			// Check the stopping criterion (CONDITION1)
			const IDType nextE = R->k;
			const bool isThreadedLeftLink = (dir == cmpRC_e::LESS);
			if (isThreadedLeftLink or (k < nextE)) {
				return dir;
			}
		}
		prev = curr;
		curr = R;
	}
	return cmpRC_e::ERROR;
}

cmpRC_e
LockFreeBSTHelper::locateConditionChecking(ANodePtr& prev, ANodePtr& curr, const IDType k) {
	auto dir = locate(prev, curr, k);

	// Condition from paper section 3.2.1
	bool condition1 = (prev->k <= k) && (k < curr->k);
	bool condition2 = (prev->k >= k) && (k > curr->k);
	if(!condition1 && !condition2)
		std::cout << "neither cond1 nor cond2 validate to true;";
		std::cout << "  prev->k:" << prev->k.getInt() << ", k:" << k << ", curr->k:" << curr->k.getInt() << std::endl;

	if ((curr->k != k) && !(curr->selectLRChild(dir).load().isThreaded()))
		std::cout << "LEMMA 1.a *not* met" << std::endl;

	return dir;
}

bool LockFreeBSTHelper::tryFlag(ANodePtr& prev, ANodePtr& curr, ANodePtr& back, bool isThread) {
	const int threadAlias = fi.getThisThreadAlias();
	std::unique_ptr<TraceFMethodCall> traceFLog;
	if(_globalDebugManager.is_enContentionLogging()) {
		traceFLog.reset(new TraceFMethodCall(__func__, threadAlias, _globalDebugManager.getRecursionCounter(threadAlias)));
	}

	CONTENTION_WHILE_COUNTER (true) {
        /* If cmp returns 2 then curr is the left link of prev;
         * so pDir is changed to 0. */
        const cmpRC_e rawPDir = cmp(curr->k, prev->k);
        const cmpRC_e pDir = (rawPDir == cmpRC_e::EQUAL) ? cmpRC_e::LEFT : rawPDir;

		if(_globalDebugManager.is_enContentionLogging()) {
            tracef("(%s_0:%d) prev:%d, curr:%d, back:%d, pDir:%d",
				   __func__, fi.getThisThreadAlias(),
				   prev->k.getInt(), curr->k.getInt(), back->k.getInt(), (int)pDir);
		}

		//(I) Flag the incoming order link & (V) comming from _cleanMark_right() cat 1&2
		const bool t = isThread;
		auto& casLink = prev->selectLRChild(pDir);
		FlagNodePtr<ANode_t> expectedFlagNodePtr {curr,0,0,t};
		bool result = cas(casLink, expectedFlagNodePtr, {curr,1,0,t}, prev->k, CASUId::FLAG_CHILDLNK);
		if (result) {
			return true;
		} else {
			/* The CAS fails, check if the link has been marked,
			 * flagged or the curr node got deleted.  If flagged,
			 * return false; if marked, first clean then proceed */
			auto nonAtomicNodePtr = prev->selectLRChild(pDir).load(); //atomic load
			ANodePtr newR = nonAtomicNodePtr.get();
			const bool f = nonAtomicNodePtr.isFlagged();
			const bool m = nonAtomicNodePtr.isMarked();
            if(_globalDebugManager.is_enContentionLogging()) {
                tracef("(%s_1:%d) newR:%d, f:%d, m:%d",
					   __func__, fi.getThisThreadAlias(), newR->k.getInt(), f, m);
            }
			if (newR == curr) {
				if (f) {
					// others have flagged the link -> return (l.50)
					return false;
				} else if (m) {
					// help within the mark process
					//cleanMark(prev, pDir); //papaer says pDir; MAJOR change, the original version is failing
				}

				prev = back;
				const cmpRC_e newRawpDir = cmp(curr->k, prev->k);
				const cmpRC_e newpDir = (newRawpDir == cmpRC_e::EQUAL) ? cmpRC_e::LEFT : newRawpDir;
				auto newCurr = prev->selectLRChild(newpDir).load().get();
				locate(prev, newCurr, curr->k);
				if(_globalDebugManager.is_enContentionLogging()) {
					tracef("(%s_2:%d) prePrev:%d, postPrev:%d, postNewCurr:%d",
							__func__, threadAlias, back->k.getInt(), prev->k.getInt(), newCurr->k.getInt());
				}
				if (newCurr != curr) //check if deleted
					return false;
				back = prev->backLink.load();
			} else {
				//not in the paper
				return false;
			}
		}
	}

	return false;
}

bool LockFreeBSTHelper::tryMark(ANodePtr& curr, cmpRC_e dir, ANodePtr firstNextMatcher) {
	const int threadAlias = fi.getThisThreadAlias();
	std::unique_ptr<TraceFMethodCall> traceFLog;
	if(_globalDebugManager.is_enContentionLogging()) {
		traceFLog.reset(new TraceFMethodCall(__func__, threadAlias, _globalDebugManager.getRecursionCounter(threadAlias)));
	}

	bool rc = true;
	CONTENTION_WHILE_COUNTER (true) {
		ANodePtr back = curr->backLink.load();
		auto nonAtomicNodePtr = curr->selectLRChild(dir).load(); //atomic load
		ANodePtr next = nonAtomicNodePtr.get();
		if(firstNextMatcher) { //not in the paper
			if(next != firstNextMatcher) {
				tracef("(%s_1:%d) next:%d did not match with firstNextMatcher:%d",
						__func__, threadAlias, next->k.getInt(), firstNextMatcher->k.getInt());
				break;
			}
			firstNextMatcher = nullptr;
		}
		const bool f = nonAtomicNodePtr.isFlagged();
		const bool m = nonAtomicNodePtr.isMarked();
		const bool t = nonAtomicNodePtr.isThreaded();

		if(_globalDebugManager.is_enContentionLogging()) {
			tracef("(%s_0:%d) curr:%d, dir:%d, back:%d, next:%d, f:%d, m:%d, t:%d",
				   __func__, threadAlias,
				   curr->k.getInt(), (int)dir, back->k.getInt(), next->k.getInt(), f,m,t);
		}

		if (m) {
			fi.handle({CASUId::TRYMARK_MARKED_POS, CASPrefix::ANY}, threadAlias);
			rc = false;
			break;
		} else if (f) {
			/**
			 * If the CAS to automatically mark a link fails due to
			 * 	a concurrent flagging of the link then TryMakr(..) helps
			 * 	the concurrent operation that flagged the link, *except*
			 * 	in the case when the link is a threaded left link. This
			 * 	is because a flagged and threaded left-link of a node
			 * 	indicates that the node is a category 1 node under REMOVE
			 * 	and we give priority to the operation that must have
			 * 	already *flagged* its right link to shift it. (p.327)
			 */
			if (not t) {
				cleanFlag(curr, next, back, false); //paper says false
				continue;
			} else if (t && (dir == cmpRC_e::RIGHT)){
				cleanFlag(curr, next, back, true); //paper says true
				continue;
			}
		}
		bool result = cas(curr->selectLRChild(dir), {next,0,0,t}, {next,0,1,t}, curr->k, CASUId::TRYMARK);
		if (result) {
			break;
		}
	}
	return rc;
}

void LockFreeBSTHelper::_cleanFlag_threaded(ANodePtr& prev, ANodePtr& curr, ANodePtr& back) {
	const int threadAlias = fi.getThisThreadAlias();
	std::unique_ptr<TraceFMethodCall> traceFLog;
	if(_globalDebugManager.is_enContentionLogging()) {
		traceFLog.reset(new TraceFMethodCall(__func__, threadAlias, _globalDebugManager.getRecursionCounter(threadAlias)));
	}

	// Cleaning a flagged order-link
    CONTENTION_WHILE_COUNTER (true) {
		//Set the preLink and mark the right-child link
		auto nonAtomicNodePtr = curr->rightChild.load(); //atomic load
		ANodePtr next = nonAtomicNodePtr.get();
		const bool f = nonAtomicNodePtr.isFlagged();
		const bool m = nonAtomicNodePtr.isMarked();
		const bool t = nonAtomicNodePtr.isThreaded();

        if(_globalDebugManager.is_enContentionLogging()) {
            tracef("(%s_0:%d) prev:%d, curr:%d, back:%d, next:%d, f:%d, m:%d, t:%d",
                   __func__, threadAlias,
				   prev->k.getInt(), curr->k.getInt(), back->k.getInt(),
                   next->k.getInt(), f, m, t );
        }

		if (m) {
			break;
		} else if (f) {
			//Help cleaning if right-child is flagged
			if (back == next) {
				// If back is getting deleted then move back
				back = back->backLink.load();
			}

			ANodePtr backNode = curr->backLink.load();
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			cleanFlag(curr, next, backNode, t); //paper says just 't'

			if (back == next) {
				auto prevPre = prev->k;
				auto pDir = cmp(prev->k, backNode->k);
				if (pDir == cmpRC_e::EQUAL) {
					continue;
				}
				prev = back->selectLRChild(pDir).load().get();

				if(_globalDebugManager.is_enContentionLogging()) {
					tracef("(%s_5) curr:%d, next:%d, backNode:%d, prev-pre:%d, prev-post:%d",
						   __func__, curr->k.getInt(), next->k.getInt(), backNode->k.getInt(),
						   prevPre, prev->k.getInt());
					prevPre=prevPre;
				}
			}
		} else {
            ANodePtr pre = curr->preLink.load(); // a nullptr preNode at this point is valid

			if (pre != prev) {
				// (II) set the prelink (the cas is equal to curr->preLink = prev
				cas(curr->preLink, pre, prev, curr->k, CASUId::CLEANFLAG_THR_PRELINK);
			}
			// (III) Mark the outgoing right-link
			const bool rightChildMarked = 0; //curr->rightChild.load().isMarked(); //not in the paper
			bool result = cas(curr->rightChild, {next,0,rightChildMarked,t}, {next,0,1,t}, curr->k, CASUId::CLEANFLAG_THR_RIGHT);
			if (result)
				break;
		}
	} //end while
	cleanMark(curr, cmpRC_e::GREATER);
}
void LockFreeBSTHelper::_cleanFlag_unThreaded(ANodePtr& prev, ANodePtr& curr, ANodePtr& back) {
	const int threadAlias = fi.getThisThreadAlias();
	std::unique_ptr<TraceFMethodCall> traceFLog;
	if(_globalDebugManager.is_enContentionLogging()) {
		traceFLog.reset(new TraceFMethodCall(__func__, threadAlias, _globalDebugManager.getRecursionCounter(threadAlias)));
	}

	// Cleaning a flagged parent-link
	auto nonAtomicNodePtr = curr->rightChild.load(); //atomic load
	ANodePtr right = nonAtomicNodePtr.get();
	const bool rF = nonAtomicNodePtr.isFlagged();
	const bool rM = nonAtomicNodePtr.isMarked();
	const bool rT = nonAtomicNodePtr.isThreaded();
	const bool isParentLink = rM; //!< the link is the parent-link of a node under remove
	const bool isPredecessorLink = (rT && rF); //!< the link is the parent-link of a predecessor of a node under remove

	if(_globalDebugManager.is_enContentionLogging()) {
		tracef("(%s_0:%d) prev(incoming parent):%d, curr:%d, back:%d, right:%d, rF:%d, rM:%d, rT:%d",
			   __func__, threadAlias,
			   prev->k.getInt(), curr->k.getInt(), back->k.getInt(),
			   right->k.getInt(), rF, rM, rT);
	}
	if (isParentLink) {
		auto nonAtomicNodePtr = curr->leftChild.load(); //atomic load
		ANodePtr left = nonAtomicNodePtr.get();
		//const bool lF = nonAtomicNodePtr.isFlagged();
		const bool lT = nonAtomicNodePtr.isThreaded();
		ANodePtr preNode = curr->preLink.load();
		if (left != preNode) {
			// (VI) mark the outgoing left links [p.327,s.3.2.2]
			// A category 3 node

			const bool rc = tryMark(curr, cmpRC_e::LEFT);

			//Start not in the paper
			auto preLinkp = curr->preLink.load();
			auto abovePreLinkP = preLinkp->backLink.load();
			auto pDir = cmp(preLinkp->k, abovePreLinkP->k);
			const bool abovePreToPreFlagged = abovePreLinkP->selectLRChild(pDir).load().isFlagged();
			if(_globalDebugManager.is_enContentionLogging()) {
				tracef("(%s_2:%d) pre:%d, abovePre:%d, pDir:%d, abovePreToPreFlagged:%d",
					   __func__, threadAlias,
					   preLinkp->k.getInt(), abovePreLinkP->k.getInt(), (int)pDir, abovePreToPreFlagged);
				UNUSED(abovePreToPreFlagged);
			}
			//END not in the paper

			if(rc) {
				cleanMark(curr, cmpRC_e::LEFT);
			}
		} else {
			// This is a category 1 or 2 node
			auto pDir = cmp(curr->k, prev->k);
			if (left == curr) {
				//category 1 node final pointer swapping according to p.327 (XXV)
				cas(prev->selectLRChild(pDir), {curr,1,0,0}, {right,0,0,rT}, prev->k, CASUId::CLEANFLAG_UTHR_CHILD0); //i had {curr,lF,0,0}
				if (!rT) {
					// (XXVI)
					cas(right->backLink, curr, prev, right->k, CASUId::CLEANFLAG_UTHR_BACK);
				}
			} else {
				// NODES at this point: prev ^= BackDelNode, preNode ^= OrderNode
				// A category 2 node (XX)
				cas(preNode->rightChild, {curr,1,0,1}, {right,0,0,rT}, preNode->k, CASUId::CLEANFLAG_UTHR_CHILD1);
				if (!rT) {
					// if a right child exists, the backLink of it has to be altered (XXI)
					//paper says prev @desired, collides with LEMMA 7 p.328
					cas(right->backLink, curr, preNode, right->k, CASUId::CLEANFLAG_UTHR_BACK);  //I had 'preNode' as desired
				}
				// (XXII)
				cas(prev->selectLRChild(pDir), {curr,1,0,0}, {preNode,0,0,lT}, prev->k, CASUId::CLEANFLAG_UTHR_CHILD2); //paper says {preNode,0,0,rT}
				// (XXIII)
				cas(preNode->backLink, curr, prev, preNode->k, CASUId::CLEANFLAG_UTHR_BACK);
			}
		}
	} else if (isPredecessorLink) {
		// The node is moving to replace its successor
		// (V) flag the incoming parent link [p.327,s.3.2.2]
		ANodePtr delNode = right;
		ANodePtr parent = nullptr;
		CONTENTION_WHILE_COUNTER (true) {
			parent = delNode->backLink.load();
			auto pDir = cmp(curr->k,prev->k);
			auto nonAtomicNodePtr = parent->selectLRChild(pDir).load(); //atomic load
			//const ANodePtr pR = nonAtomicNodePtr.get(); //not consistent with the paper
			const bool pF = nonAtomicNodePtr.isFlagged();
			const bool pM = nonAtomicNodePtr.isMarked();
			const bool pT = nonAtomicNodePtr.isMarked(); //not in the paper
			UNUSED(pT);
			curr = nonAtomicNodePtr.get(); //not consistent with the paper

			if(_globalDebugManager.is_enContentionLogging()) {
				tracef("(%s_1:%d) delNode:%d, parent:%d, pDir:%d, curr:%d, F:%d, M:%d, T:%d",
					   __func__, threadAlias,
					   delNode->k.getInt(), parent->k.getInt(), (int)pDir, curr->k.getInt(), pF, pM, pT);
			}

			if (pM)
				cleanMark(parent, pDir);
			else if (pF)
				break;
			else {
				auto result = cas(parent->selectLRChild(pDir), {curr,0,0,0}, {curr,1,0,0}, parent->k, CASUId::CLEANFLAG_UTHR_V); //paper says exp:{curr,0,0,0}
				if (result)
					break;
				//if(delNode == curr) //NOT paper conform
				//	return;cleanFlag(prev, curr, back, false);
			}
		}
		ANodePtr backNode = parent->backLink.load();
		cleanFlag(parent, curr, backNode, false); //paper says true (i had 'right' as curr)
	}
}

void LockFreeBSTHelper::cleanFlag(ANodePtr& prev, ANodePtr& curr, ANodePtr& back, bool isThread) {
	const int tmpThreadAlias = fi.getThisThreadAlias();
	_globalDebugManager.incRecursion(tmpThreadAlias);
	if (isThread)
		_cleanFlag_threaded(prev, curr, back);
	else {
		_cleanFlag_unThreaded(prev, curr, back);
	}
	_globalDebugManager.decRecursion(tmpThreadAlias);
}



void LockFreeBSTHelper::_cleanMark_right(ANodePtr &curr, FlagNodePtr<ANode_t> &nonAtomicLeftChild) {
	const int threadAlias = fi.getThisThreadAlias();
	std::unique_ptr<TraceFMethodCall> traceFLog;
	if(_globalDebugManager.is_enContentionLogging()) {
		traceFLog.reset(new TraceFMethodCall(__func__, threadAlias, _globalDebugManager.getRecursionCounter(threadAlias)));
	}

	ANodePtr left = nonAtomicLeftChild.get();
	auto& delNode = curr;

	/* The node is getting deleted itself.
	 * If it is category 1 or 2 node, flag the incoming parent link;
	 * if it is a category 3 node, flag the incoming parent link of
	 *  its predecessor/preNode */
	CONTENTION_WHILE_COUNTER (true) {
		ANodePtr preNode = delNode->preLink.load();
		if(_globalDebugManager.is_enContentionLogging()) {
			int preNodeK = preNode ? preNode->k.getInt() : -1;
			UNUSED(preNodeK);
			tracef("(%s_1:%d): curr:%d, leftChild:%d, preNode:%d",
				   __func__, threadAlias,
				   curr->k.getInt(), nonAtomicLeftChild.get()->k.getInt(), preNodeK);
		}

		if (preNode == nullptr) {
			//everything has been deleted -> return
			break; //not in the paper
		}

		if (preNode == left) { // Category 1 or 2 node.
			ANodePtr parent = delNode->backLink.load();
			auto back = parent->backLink.load();
            if(_globalDebugManager.is_enContentionLogging()) {
                tracef("(%s_2:%d): parent:%d, curr:%d, back:%d",
					   __func__, threadAlias,
                       parent->k.getInt(), curr->k.getInt(), back->k.getInt());
            }

			//(V) flag the incoming parent-link
			tryFlag(parent, curr, back, false); //paper says true
			fi.handle({CASUId::CLEANMARK_RIGHT_V, CASPrefix::POST_CAS}, threadAlias);

			//!< @note 'dir' at this position may be wrong, see unclear paper @l.129
			const auto pDir = cmp(curr->k, parent->k);
			const auto oldNodePtr = parent->selectLRChild(pDir).load().get();
			if (oldNodePtr == curr) {
				//approaching finals swapping of pointers
				cleanFlag(parent, curr, back, false); //paper says true
			}
            break; //NOT paper conform
        } else {
			// Category 3 node
			ANodePtr preParent = preNode->backLink.load();
			auto nonAtomicParent = preParent->rightChild.load(); //atomic load
			const bool pF = nonAtomicParent.isFlagged();
			const bool pM = nonAtomicParent.isMarked();
			const bool pT = nonAtomicParent.isThreaded(); //not in the paper
			ANodePtr backNode = preParent->backLink.load();
            if(_globalDebugManager.is_enContentionLogging()) {
                tracef("(%s_3:%d): parent:%d, pF:%d, pM:%d, backNode:%d",
					   __func__, threadAlias,
                       nonAtomicParent.get()->k.getInt(), pF, pM, backNode->k.getInt());
            }

			if (pM) {
				cleanMark(preParent, cmpRC_e::RIGHT);
			} else if (pF) {
				cleanFlag(preParent, preNode, backNode, false); //paper says true
				//ad true: false is correct because "not threaded the the link must bethe incoming parent-link of [..] its predecessor"
				break;
            } else if (pT) { //not in the paper
                break; //because some other thread finished another operation, time to check again
			} else {
				// (IV) flag the parent-link of the predecessor incoming to that
				//flag the incoming pointer to the node and its predecessor (cat3 nodes only) [p.324]
				//!< @note 'markDir' AND 'parent' at this position may be wrong, see unclear paper @loc.139; estimated: 99% correct
				const auto pDir = cmp(preNode->k, preParent->k);
				const auto result = cas(preParent->selectLRChild(pDir), {preNode,0,0,0}, {preNode,1,0,0}, preParent->k, CASUId::CM_FLAG_PRES_PARENT); //flag
				if (result) {
					cleanFlag(preParent, preNode, backNode, false); //paper says true
					break; //XXX this may be wrong
				}
			}
		}
	} //end while
}

void LockFreeBSTHelper::_cleanMark_otherwise(ANodePtr &curr,
		FlagNodePtr<ANode_t> &nonAtomicLeftChild,
		FlagNodePtr<ANode_t> &nonAtomicRightChild) {
	const int threadAlias = fi.getThisThreadAlias();
	std::unique_ptr<TraceFMethodCall> traceFLog;
	if(_globalDebugManager.is_enContentionLogging()) {
		traceFLog.reset(new TraceFMethodCall(__func__, threadAlias, _globalDebugManager.getRecursionCounter(threadAlias)));
	}
	ANodePtr left = nonAtomicLeftChild.get();
	const bool lF = nonAtomicLeftChild.isFlagged();
	const bool lM = nonAtomicLeftChild.isMarked();
	const bool lT = nonAtomicLeftChild.isThreaded();
	ANodePtr right = nonAtomicRightChild.get();
	const bool rF = nonAtomicRightChild.isFlagged();
	const bool rM = nonAtomicRightChild.isMarked();
	const bool rT = nonAtomicRightChild.isThreaded();

	if(_globalDebugManager.is_enContentionLogging()) {
		tracef("(%s_0:%d) curr:%d, left:%d, lF:%d, lM:%d, lT:%d, right:%d, rF:%d, rM:%d, rT:%d",
				__func__, threadAlias,
				curr->k.getInt(), left->k.getInt(), lF, lM, lT, right->k.getInt(), rF, rM, rT);
	}

	// The node is getting deleted or moved to replace its successor
	// A set mark bit at a left-link indicates, that it is either coming
	//  out from a category 3 node under REMOVE, or its predecessor
	// @note paper p.328 (left upper corner) says "A set mark-bit at a LEFT-link .."
	if (rM) {
		// (VII) clean its left marked link
		ANodePtr preNode = curr->preLink.load();

		//BEGIN not in the paper
		fi.handle({CASUId::TRYMARK_VII, CASPrefix::PRE_CAS}, threadAlias);
		tryMark(preNode, cmpRC_e::LEFT);
		fi.handle({CASUId::TRYMARK_VII, CASPrefix::POST_CAS}, threadAlias);

		//1. ensure that CLRMARK_O_XII hasn't happened yet, by checking if XI (or X) is *not done* yet
		const auto checkDir = cmp(curr->k, preNode->k);
		auto nonAtomicPreNodeCurrPointer = preNode->selectLRChild(checkDir).load();
		const bool targetStillThere = cmp(nonAtomicPreNodeCurrPointer.get()->k, curr->k) ==  cmpRC_e::EQUAL;
		const bool prePreNodeToPreNodeFlagged = preNode->backLink.load()->rightChild.load().isFlagged();
		const bool XalreadyDone = targetStillThere && !prePreNodeToPreNodeFlagged;

		if(_globalDebugManager.is_enContentionLogging()) {
			UNUSED(targetStillThere); UNUSED(prePreNodeToPreNodeFlagged);
			const bool targetThreaded = nonAtomicPreNodeCurrPointer.isThreaded();
			UNUSED(targetThreaded);
			tracef("(%s_1:%d) preNode:%d, checkDir:%d, targetNodeStillThere:%d, targetThreaded:%d, prePreNodeToPreNodeFlagged:%d",
					__func__, threadAlias,
				   preNode->k.getInt(), checkDir, targetStillThere, targetThreaded, prePreNodeToPreNodeFlagged);
		}
		//1.1 break otherwise
		//if(!XalreadyDone) {
		UNUSED(XalreadyDone);
			cleanMark(preNode, cmpRC_e::LEFT);
		//}
	} else if (rT and rF) {
		// change links accordingly
		ANodePtr delNode = right;
		ANodePtr delNodePa = delNode->backLink.load();
		ANodePtr preParent = curr->backLink.load();
		auto pDir = cmp(delNode->k, delNodePa->k);
		auto nonAtomicDelNodeL = delNode->leftChild.load();
		auto nonAtomicDelNodeR = delNode->rightChild.load();
		ANodePtr delNodeL = nonAtomicDelNodeL.get();
		ANodePtr delNodeR = nonAtomicDelNodeR.get();
		const bool drT = nonAtomicDelNodeR.isThreaded();

		// (X) relinks the 'preNode->backNode->rightLink' (pointing to preLink) to either
		//		a. a threaded up-link to 'current' (the new order link) or
		//		b. a non threaded link to the existing former left child of 'current'
		//auto preParentRightF = preParent->rightChild.load().isFlagged();
		cas(preParent->rightChild, {curr,1,0,0}, {left,lF,0,lT}, preParent->k, CASUId::CLRMARK_O_X);  //paper says lF @ expected
		if (!lT) {

			// (XI) sets 'preNode->leftNode->backLink' to 'parentNode->preNode' in case 'preNode->leftNode' exists - case (X.b)
			if (cmpRC_e::EQUAL != cmp(left->k, preParent->k))
				cas(left->backLink, curr, preParent, left->k, CASUId::CLRMARK_O_XI);
		}
		// (XII) relinks 'preNode->leftLink' to the former 'delNode->leftNode'
		cas(curr->leftChild, {left,0,1,lT}, {delNodeL,0,0,0}, curr->k, CASUId::CLRMARK_O_XII);
		// (XIII) relinks the backLink of the 'delNode->leftNode'
		cas(delNodeL->backLink, delNode, curr, delNodeL->k, CASUId::CLRMARK_O_XIII);
		// (XIV) relinks the 'delNode->preNode->rightLink' to the 'delNode->rightNode'
		//       appends the former delNode->rightNode to its predecessors right side (curr->rightChild)
		cas(curr->rightChild, {delNode,1,0,1}, {delNodeR,0,0,drT}, curr->k, CASUId::CLRMARK_O_XIV); //delNode==right
		if (!drT) {
			// (XV) relinks the 'delNode->rightNode->backLink' to the 'delNode->preLink'(a.k.a. curr)
			cas(delNodeR->backLink, delNode, curr, delNodeR->k, CASUId::CLRMARK_O_XV);
		}
		// (XVI) IMPORTANT relinks the actual 'delNode->childLink' to the 'delNode->preLink'(a.k.a. curr)
		cas(delNodePa->selectLRChild(pDir), {delNode,1,0,0}, {curr,0,0,0}, delNodePa->k, CASUId::CLRMARK_O_XVI);
		cas(curr->backLink, preParent, delNodePa, curr->k, CASUId::CLRMARK_O_XVII);

		//TODO propose for physical deletion (has to be acknowledged from ever other thread)
		tracef("(%s:%d) node %d deletion done", __func__, threadAlias, delNode->k.getInt());
		std::cout << "Node " << delNode->k << " logical deletion done" << std::endl;
	} else if (lM) { //not paper conform
		tracef("(%s_5:%d) left marked: curr:%d, left:%d, lF:%d, lM:%d, lT:%d, right:%d, rF:%d, rM:%d, rT:%d",
			   __func__, threadAlias,
			   curr->k.getInt(), left->k.getInt(), lF, lM, lT, right->k.getInt(), rF, rM, rT);

		ANodePtr preNode = curr->preLink.load();
		const int resolvedPreNodeK = (preNode) ? preNode->k.getInt() : -1;
		UNUSED(resolvedPreNodeK);
		ANodePtr backNode = curr->backLink.load();
		const auto pDir = cmp(curr->k, backNode->k);
		const bool backToCurrFlagged = backNode->selectLRChild(pDir).load().isFlagged();
		UNUSED(backToCurrFlagged);
		tracef("(%s_7:%d) maybe cleanFlag of the preNode:%d, backToCurrFlagged:%d",
			   __func__, threadAlias, resolvedPreNodeK, backToCurrFlagged);
		//@todo check existance of the nodes around??
		//  -- maybe reconnect its children
		//  -- maybe reconnect the mark link to pos_inf as an indicator for complete deletion?
	} else {
		tracef("(%s_X:%d) hit an unexpected case: left:%d, lF:%d, lM:%d, lT:%d, right:%d, rF:%d, rM:%d, rT:%d",
			   __func__, threadAlias,
			   left->k.getInt(), lF, lM, lT, right->k.getInt(), rF, rM, rT);
	}
}

void LockFreeBSTHelper::cleanMark(ANodePtr& curr, const cmpRC_e& markDir) {
	// the following two lines have to stay here
	auto nonAtomicLeftChild = curr->leftChild.load(); //atomic load
	auto nonAtomicRightChild = curr->rightChild.load(); //atomic load

	const int tmpThreadAlias = fi.getThisThreadAlias();
	_globalDebugManager.incRecursion(tmpThreadAlias);
	if (markDir == cmpRC_e::RIGHT) {
		_cleanMark_right(curr, nonAtomicLeftChild);
	} else {
		_cleanMark_otherwise(curr, nonAtomicLeftChild, nonAtomicRightChild);
	}
	_globalDebugManager.decRecursion(tmpThreadAlias);
}
