/*
 * IDType.cpp
 *
 *  Created on: Sep 5, 2015
 *      Author: nickma
 */

#include "IDType.h"

#include <cstdbool>

IDType::IDType(const int value) :
		_value(value) { }

const int IDType::getInt() const {
	return this->_value;
}

cmpRC_e IDType::cmp(const IDType& rhs) const {
	if (this->_value < rhs._value)
		return cmpRC_e::LESS;
	else if (this->_value > rhs._value)
		return cmpRC_e::GREATER;
	else
		return cmpRC_e::EQUAL;
}

cmpRC_e cmp(const IDType& lhs, const IDType& rhs) {
	auto result = lhs.cmp(rhs);
	return result;
}

//cmpRC_e operator==(const IDType& lhs, const IDType& rhs) {
//	if (lhs < rhs)
//		return cmpRC_e::LESS;
//	else if (lhs > rhs)
//		return cmpRC_e::GREATER;
//	else
//		return cmpRC_e::EQUAL;
//}
bool operator!=(const IDType& lhs, const IDType& rhs) {
	return cmpRC_e::EQUAL != lhs.cmp(rhs);
}
bool operator>(const IDType& lhs, const IDType& rhs) {
	return lhs._value > rhs._value;
}
bool operator<(const IDType& lhs, const IDType& rhs) {
	return lhs._value < rhs._value;
}
bool operator>=(const IDType& lhs, const IDType& rhs) {
	return lhs._value >= rhs._value;
}
bool operator<=(const IDType& lhs, const IDType& rhs) {
	return lhs._value <= rhs._value;
}

const bool IDType::isInfinite() const {
	return (_value == POS_INFITY) || (_value == NEG_INFITY);
}

std::ostream& operator<<(std::ostream& inout, const IDType& v) {
	if (&v == nullptr)
		return inout << "null";
	if (v._value == IDType::NEG_INFITY)
		inout << "-inf";
	else if (v._value == IDType::POS_INFITY)
		inout << "inf";
	else
		inout << v._value;

	return inout;
}

/// EpsilonedIDType
EpsilonedIDType::EpsilonedIDType(const int value, const epsilon_t epsilon) :
		IDType(value), _e(epsilon) { }
EpsilonedIDType::EpsilonedIDType(const IDType& v, const epsilon_t epsilon) :
		IDType(v.getInt()), _e(epsilon) { }

cmpRC_e cmp(const EpsilonedIDType& lhs, const IDType& rhs) {
	EpsilonedIDType epsilonedRhs{rhs};
	if (lhs < epsilonedRhs)
		return cmpRC_e::LESS;
	else if (lhs > epsilonedRhs)
		return cmpRC_e::GREATER;
	else
		return cmpRC_e::EQUAL;
}
bool operator<(const EpsilonedIDType& lhs, const EpsilonedIDType& rhs) {
	const bool epsilon_present = (lhs._e != ZERO) || (rhs._e != ZERO);
	if (epsilon_present) {
		auto lhsValue = lhs.isInfinite() ? lhs._value : lhs._value*10 + lhs._e;
		auto rhsValue = rhs.isInfinite() ? rhs._value : rhs._value*10 + rhs._e;
		return lhsValue < rhsValue;
	} else {
		return lhs._value < rhs._value;
	}
}
bool operator>(const EpsilonedIDType& lhs, const EpsilonedIDType& rhs) {
	const bool epsilon_present = (lhs._e != ZERO) || (rhs._e != ZERO);
	if (epsilon_present) {
		auto lhsValue = lhs.isInfinite() ? lhs._value : lhs._value*10 + lhs._e;
		auto rhsValue = rhs.isInfinite() ? rhs._value : rhs._value*10 + rhs._e;
		return lhsValue > rhsValue;
	} else {
		return lhs._value > rhs._value;
	}
}
bool operator<(const EpsilonedIDType& lhs, const IDType& rhs) {
	EpsilonedIDType rhs_epsiloned{rhs};
	return (lhs < rhs_epsiloned);
}
