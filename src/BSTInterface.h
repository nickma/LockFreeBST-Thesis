/*
 * BSTInterface.h
 *
 *  Created on: Oct 7, 2015
 *      Author: nickma
 */

#ifndef SRC_BSTINTERFACE_H_
#define SRC_BSTINTERFACE_H_

#include <cstdbool>

#include "IDType.h"


/*
Basic interface for .
A set may contain each item at most once.
Only for long datatype for now, genericity  will be added later
*/
class BSTInterface {
public:
	//destructor
	virtual ~BSTInterface(){};

	//adds an item to the set
	//returns true if successfull, false if the item already was in the set
	virtual bool add(const IDType item) = 0;

	//removes an item from the set
	//returns true if successfull, false if the item was not in the set
	virtual bool remove(const IDType item) = 0;

	//checks if an item is contained in a set
	//return true if the item is contained, false if not
	virtual bool contains(const IDType item) = 0;
};


#endif /* SRC_BSTINTERFACE_H_ */
