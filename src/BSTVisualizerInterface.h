/*
 * BSTVisualizerInterface.h
 *
 *  Created on: Jan 25, 2016
 *      Author: nickma
 */

#ifndef SRC_BSTVISUALIZERINTERFACE_H_
#define SRC_BSTVISUALIZERINTERFACE_H_

#include <string>

#include "ANode_s.h"


class BSTVisualizerInterface {
public:
	virtual ~BSTVisualizerInterface() {};

	virtual void readInBst(const ANodePtr bstRoot0) = 0;

	virtual void exportToFile(const std::string pathAndFilename) const = 0;
};


#endif /* SRC_BSTVISUALIZERINTERFACE_H_ */
