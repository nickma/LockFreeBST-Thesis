#include <atomic>
#include <iostream>
#include <string>
#include <vector>
#include <limits>
#include <sstream>

#include "greatest/greatest.h"
#include "ANode_s.h"
#include "FlagNodePtr.h"
#include "IDType.h"
#include "LockFreeBST.h"
#include "testHelper/ConcurrentExecuter.h"
#include "testHelper/PerThreadCounter.h"
#include "testHelper/NaivePerThreadValueStore.h"
#include "bstdot_visualizer.h"
#include "benchmark/SimpleCSVWriter.h"
#include "benchmark/GenerateRandomValues.h"
#include "benchmark/ResultStoreCSVConverter.h"
#include "LockBasedList.h"

class Benchmark {
public:

};

struct TestMetric {
    unsigned int insertThreadCount = 0;
    unsigned int containsThreadCount = 0;
    unsigned int removeThreadCount = 0;

    //random seed data
    unsigned int keyRange = 100;
    unsigned int keysToGenerateInRange = 1E4;

    //single test run data
    int testRepetitions = 20;

    TestMetric(const int baseThreadCount, const unsigned int keyRange)
            : insertThreadCount(baseThreadCount), containsThreadCount(baseThreadCount * 9), keyRange(keyRange) {}


};

TEST bench_insert_contains() {
    //prepare CSV output
    SimpleCSVWriter csvWriter;
    ResultStoreCSVConverter toCSVConverter(csvWriter);
    toCSVConverter.attachHeaderToCSV();

    //run it with different thread count
    std::vector<TestMetric> testMetrics{{1,  50000000},
                                        {3,  50000000},
                                        {6,  50000000},
                                        {9,  50000000},
                                        {12, 50000000},
                                        {15, 50000000},
                                        {1,  1000000},
                                        {3,  1000000},
                                        {6,  1000000},
                                        {9,  1000000},
                                        {12, 1000000},
                                        {15, 1000000},
                                        {1,  10000},
                                        {3,  10000},
                                        {6,  10000},
                                        {9,  10000},
                                        {12, 10000},
                                        {15, 10000}
    };
    for (auto &testMetric: testMetrics) {
        //run the same test multiple times
        std::vector<TimedSingleOpResult> resultStoreAllResults;
        const int testRepetitionCnt = 20;
        for (int i = 0; i < testRepetitionCnt; ++i) {
            std::vector<int> tmpMillionElements = GenerateRandomValues::getIntVector(testMetric.keysToGenerateInRange,
                                                                                     testMetric.keyRange);
            std::vector<IDType> millionIDTypes;
            millionIDTypes.insert(millionIDTypes.begin(), tmpMillionElements.begin(), tmpMillionElements.end());
            //debug output
            std::cout << "IDType size: " << millionIDTypes.size() << std::endl;
            auto uniqueElementCount = GenerateRandomValues::countUniqueElements(tmpMillionElements);
            std::cout << "millionElement vector contains " << uniqueElementCount << " unique Elements" << std::endl;

            LockFreeBST bst;
            //spawn the tasks
            ConcurrentExecuter concurrentAdd(bst, 0);
            std::vector<TimedSingleOpResult> resultStore =
                    concurrentAdd.addAndLocateRandom_withkeyUpperRange(millionIDTypes, millionIDTypes, {},
                                                                       testMetric.insertThreadCount,
                                                                       testMetric.containsThreadCount,
                                                                       testMetric.removeThreadCount,
                                                                       testMetric.keyRange);

            //add the result to the clobal result store
            resultStoreAllResults.insert(resultStoreAllResults.end(), resultStore.begin(), resultStore.end());
        }

// split up the combined resultStore for all results into OperationType oriented ones
        std::vector<TimedSingleOpResult> resultsInsert, resultsContains;
        for (auto &each: resultStoreAllResults) {
            if (each.perationType == TimerType::ID_INSERT) {
                resultsInsert.push_back(each);
            } else if (each.perationType == TimerType::ID_CONTAINS) {
                resultsContains.push_back(each);
            }
        }

//convert results
        const int threadSum =
                testMetric.insertThreadCount + testMetric.containsThreadCount + testMetric.removeThreadCount;
        std::stringstream operationsSplitting;
        operationsSplitting << testMetric.insertThreadCount << "i:"
                            << testMetric.containsThreadCount << "c:"
                            << testMetric.removeThreadCount << "r";
        toCSVConverter.attachSingleOPVectorToCSV(resultsInsert, operationsSplitting.str(), testMetric.keyRange,
                                                 threadSum);
        toCSVConverter.attachSingleOPVectorToCSV(resultsContains, operationsSplitting.str(), testMetric.keyRange,
                                                 threadSum, false);
        csvWriter.writeToFile("/tmp/lockfreebst_results.csv");
    }
            PASS();
}

TEST bench_insert_contains_lockbased() {
//prepare CSV output
    SimpleCSVWriter csvWriter;
    ResultStoreCSVConverter toCSVConverter(csvWriter);
    toCSVConverter.attachHeaderToCSV();

    //run it with different thread count
    std::vector<TestMetric> testMetrics{{1,  50000000},
                                        {3,  50000000},
                                        {6,  50000000},
                                        {9,  50000000},
                                        {12, 50000000},
                                        {15, 50000000},
                                        {1,  1000000},
                                        {3,  1000000},
                                        {6,  1000000},
                                        {9,  1000000},
                                        {12, 1000000},
                                        {15, 1000000},
                                        {1,  10000},
                                        {3,  10000},
                                        {6,  10000},
                                        {9,  10000},
                                        {12, 10000},
                                        {15, 10000}
    };
    for (auto &testMetric: testMetrics) {
//run the same test multiple times
        std::vector<TimedSingleOpResult> resultStoreAllResults;
        const int testRepetitionCnt = 20;
        for (int i = 0; i < testRepetitionCnt; ++i) {
            std::vector<int> tmpMillionElements = GenerateRandomValues::getIntVector(testMetric.keysToGenerateInRange,
                                                                                     testMetric.keyRange);
            std::vector<IDType> millionIDTypes;
            millionIDTypes.insert(millionIDTypes.begin(), tmpMillionElements.begin(), tmpMillionElements.end());
//debug output
            std::cout << "IDType size: " << millionIDTypes.size() << std::endl;
            auto uniqueElementCount = GenerateRandomValues::countUniqueElements(tmpMillionElements);
            std::cout << "millionElement vector contains " << uniqueElementCount << " unique Elements" << std::endl;

            LockBasedList bst;
//spawn the tasks
            ConcurrentExecuter concurrentAdd(bst, 0);
            std::vector<TimedSingleOpResult> resultStore =
                    concurrentAdd.addAndLocateRandom_withkeyUpperRange(millionIDTypes, millionIDTypes, {},
                                                                       testMetric.insertThreadCount,
                                                                       testMetric.containsThreadCount,
                                                                       testMetric.removeThreadCount,
                                                                       testMetric.keyRange);

//add the result to the clobal result store
            resultStoreAllResults.insert(resultStoreAllResults.end(), resultStore.begin(), resultStore.end());
        }

// split up the combined resultStore for all results into OperationType oriented ones
        std::vector<TimedSingleOpResult> resultsInsert, resultsContains;
        for (auto &each: resultStoreAllResults) {
            if (each.perationType == TimerType::ID_INSERT) {
                resultsInsert.push_back(each);
            } else if (each.perationType == TimerType::ID_CONTAINS) {
                resultsContains.push_back(each);
            }
        }

        //convert results and add them to the csv file
        const int threadSum =
                testMetric.insertThreadCount + testMetric.containsThreadCount + testMetric.removeThreadCount;
        std::stringstream operationsSplitting;
        operationsSplitting << testMetric.insertThreadCount << "i:"
                            << testMetric.containsThreadCount << "c:"
                            << testMetric.removeThreadCount << "r";
        toCSVConverter.attachSingleOPVectorToCSV(resultsInsert, operationsSplitting.str(), testMetric.keyRange,
                                                 threadSum);
        toCSVConverter.attachSingleOPVectorToCSV(resultsContains, operationsSplitting.str(), testMetric.keyRange,
                                                 threadSum, false);
        csvWriter.writeToFile("/tmp/lockbased_results.csv");
    }

            PASS();
}

TEST bench_foo() {
    LockBasedList lockList;
    ASSERT(lockList.add(3));
    ASSERT(lockList.contains(3));
    ASSERT_FALSE(lockList.contains(4));
    ASSERT_FALSE(lockList.add(3));

    LockBasedList bst;
    ConcurrentExecuter cEx(bst, 10);
    cEx.everyoneAddsSame({6});
    ASSERT(bst.contains(6));

    PASS();
}

SUITE (bechmark) {
    RUN_TEST(bench_insert_contains);
    RUN_TEST(bench_insert_contains_lockbased);
    RUN_TEST(bench_foo);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    GREATEST_MAIN_BEGIN();      /* command-line arguments, initialization. */
    greatest_info.flags |= GREATEST_FLAG_VERBOSE;    /* enable verbose mode by default */

    try {
        RUN_SUITE(bechmark);
    } catch (const std::exception &e) {
        std::cerr << " a standard exception was caught, with message '"
                  << e.what() << "'" << std::endl;
    }
    GREATEST_MAIN_END();        /* display results */
}
