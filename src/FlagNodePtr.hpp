/**
 * This is the template class implementation file of flagNodePtr.h
 * @see http://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file/495056#495056
 */
#pragma once

//template <typename T>
//void Foo<T>::doSomething(T param)
template <typename T>
FlagNodePtr<T>::FlagNodePtr(T* rawPointer, bool flag, bool mark, bool thread) noexcept {
	this->reset(rawPointer, flag, mark, thread);
}

template <typename T>
T* FlagNodePtr<T>::get() const {
	// SPARC specific restore of upper three bits
	//		(which should be equal to those three before the last three)
	const uint64_t upperThreeResetVal = ((uint64_t)_flaggedPtr & beforeLastThreBits_mask) << 3;
	const uint64_t clearFlagsMask = ~upperThreeBits_mask;
	return (T*) (((uint64_t) _flaggedPtr & clearFlagsMask) | upperThreeResetVal);
}

template <typename T>
bool FlagNodePtr<T>::isFlagged() const {
	return (uint64_t) _flaggedPtr & ((uint64_t) 1 << flagID::flag);
}
template <typename T>
bool FlagNodePtr<T>::isMarked() const {
	return (uint64_t) _flaggedPtr & ((uint64_t) 1 << flagID::mark);
}

template <typename T>
bool FlagNodePtr<T>::isThreaded() const {
	return (uint64_t) _flaggedPtr & ((uint64_t) 1 << flagID::thread);
}

template <typename T>
void FlagNodePtr<T>::reset(T* rawPointer, bool flag, bool mark, bool thread) {
	// detect whether the upper three bits are "active low" or "active high"
	//		to react appropriate (e.g. SPARC stack vs. heap allocated memory)
	const uint64_t clearFlagsMask = ~upperThreeBits_mask;
	uint64_t flaggedPtr = (uint64_t) rawPointer & clearFlagsMask;
	flaggedPtr |= (uint64_t) flag << flagID::flag;
	flaggedPtr |= (uint64_t) mark << flagID::mark;
	flaggedPtr |= (uint64_t) thread << flagID::thread;
	_flaggedPtr = (T*) flaggedPtr;
}
template <typename T>
void FlagNodePtr<T>::reset(T* rawPointer) {
	_flaggedPtr = rawPointer;
}

