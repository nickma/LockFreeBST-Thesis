/*
 * FaultCase.h
 *
 *  Created on: Jan 29, 2016
 *      Author: nickma
 */

#ifndef SRC_FAULTCASE_H_
#define SRC_FAULTCASE_H_

#include "FaultPos.h"

/**
 * A FaultCase is a FaultPosition personalized to a
 * thread ID.
 * It also hold additional information for the faultProducer
 * such as the delay to be injected.
 *
 * Usage: {{CASUId::ADD, CASPrefix::PRE_CAS}, 0}
 */
class FaultCase {
public:
	//std::function<bool(const CASPosition& pos)> _positionCondition;
	const FaultPos positionFilter;

	/**
	 * thread alias
	 */
	const int threadAlias;
	const unsigned int delayTimeMS = 100;

	/**
	 * @TODO for flexible filter conditions, move the constructor with std::function
	 * 		to a child.
	 *  		-- introduce an interface "bool matching(position, alias)"
	 * 		-- introduce a method handle(...)
	 * FaultCase(const std::function<bool(const CASPosition& pos)> &posFilter, const int& threadAlias);
	 *
	 * @var[in] delayTimeMS defines the timespan for which the fault is
	 * going to block the concerning threadAlias. 100ms by default
	 */
	FaultCase(const FaultPos& pos, const int& threadAlias);
	FaultCase(const FaultPos& pos, const int& threadAlias, const unsigned int delayTimeMS);

	friend bool operator==(const FaultCase& lhs, const FaultCase& rhs);
};

#endif /* SRC_FAULTCASE_H_ */
