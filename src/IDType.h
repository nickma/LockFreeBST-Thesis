#ifndef SRC_IDTYPE_H_
#define SRC_IDTYPE_H_

#include <iostream>
#include <limits>
#include <stdexcept>

enum cmpRC_e : int {
	LESS = 0,
	LEFT = 0,
	GREATER = 1,
	RIGHT = 1,
	EQUAL	= 2,

	ERROR = -1,
};

/**
 * IDType is the type used to identify a node, doing so it holds
 * the data element as the ID
 */
class IDType {
public:
	static constexpr int POS_INFITY	= std::numeric_limits<int>::max();
	static constexpr int NEG_INFITY = std::numeric_limits<int>::min();
	/**
	 * Allows instantiation with an integer as input
	 * e.g: {1} or {IDType::NEG_INFITY}
	 */
	IDType(const int value);

	//permit typecasts
	virtual operator int() = delete;

	/**
	 * returns the value kept by IDType
	 */
	const int getInt() const;

	/**
	 * paper conform compare operation
	 * @return {0,1,2} depending on whether lhs is less than,
	 * 					greater than or equal to rhs
	 */
	cmpRC_e cmp(const IDType& rhs) const;
	friend cmpRC_e cmp(const IDType& lhs, const IDType& rhs);
	friend bool operator!=(const IDType& lhs, const IDType& rhs);
	friend bool operator>(const IDType& lhs, const IDType& rhs);
	friend bool operator>=(const IDType& lhs, const IDType& rhs);
	friend bool operator<(const IDType& lhs, const IDType& rhs);
	friend bool operator<=(const IDType& lhs, const IDType& rhs);

	friend std::ostream& operator<<(std::ostream& inout, const IDType& v);
protected:
	int _value; //!< the held value

	/**
	 * @returns true if the value is *either*
	 * 		POS_INFITY or NEG_INFITY
	 */
	const bool isInfinite() const;
};

enum epsilon_t : int {
	NEGATIVE = -1,
	ZERO = 0,
	POSITIVE = 1
};

/**
 * EpsilonedIDType adds the ability of fuzzy matching an IDType.
 *
 * Means that cmp is given an offset and in combination with
 * bst::locate looking for the offset value, it should never be
 * able to find it exactly but exit with the resulting location
 * inbetween.
 */
class EpsilonedIDType : public IDType {
public:
	EpsilonedIDType(const int value, const epsilon_t epsilon = ZERO);
	EpsilonedIDType(const IDType& v, const epsilon_t epsilon = ZERO);

	//permit typecasts
	virtual operator int() = delete;

	friend cmpRC_e cmp(const EpsilonedIDType& lhs, const IDType& rhs);
	friend bool operator<(const EpsilonedIDType& lhs, const IDType& rhs);
	friend bool operator>(const EpsilonedIDType& lhs, const EpsilonedIDType& rhs);
	friend bool operator<(const EpsilonedIDType& lhs, const EpsilonedIDType& rhs);
	const epsilon_t _e; //!< offset epsilon

};

#endif /* SRC_IDTYPE_H_ */
