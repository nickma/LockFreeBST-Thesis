#ifndef LOCKFREEBSTHELPER_H_
#define LOCKFREEBSTHELPER_H_

#include <atomic>
#include <cstdbool>
#include <chrono>

#include "ANode_s.h"
#include "FaultInjector.h"
#include "FaultPos.h"
#include "FlagNodePtr.h"
#include "IDType.h"
#include "testHelper/PerThreadCounter.h"
#include "testHelper/optionalTraceF.h"
#include "testHelper/NaivePerThreadValueStore.h"

class DebugDataPerLoop {
public:
	int contentionCounter = 0;

	void increaseLoopCounter() {
		contentionCounter++;
	}
};
/**
 * Same data for all Threads (because one bst instance exists)
 */
class GlobalDebugManager {
	bool _contentionLogging = true; //set the default value

	PerThreadCounter _recursionThreadCounter;
	//per Thread value stores
	PerThreadValueStore<bool> _recursionDeepnessReached = PerThreadValueStore<bool>(20, false);
	PerThreadValueStore<bool> _contentionLimitReached = PerThreadValueStore<bool>(20, false);
public:
	const int contentionLimit = 10000;
	const int contentionDebugLimit = (int)(contentionLimit * 0.8);
	const int recursionLimit   = 5000;
	const int recursionDebugLimit = (int)(recursionLimit * 0.8);

	void detectContention(const int& threadAlias, const DebugDataPerLoop &perLoopData) {
		if(!_contentionLimitReached.get(threadAlias)) {
			//in here for performance reasons and because contentionLogging must always occure
			// before contentionLimit
			if(!_contentionLogging) {
				//atomic variable negligible, because the variable is going to be set to true *only*
				_contentionLogging |= perLoopData.contentionCounter > contentionDebugLimit;
				_contentionLogging |= _recursionThreadCounter.get(threadAlias) > recursionDebugLimit;
			}

			const bool contentionLimitExceeded = perLoopData.contentionCounter > contentionLimit;
			if (contentionLimitExceeded) {
				_contentionLimitReached.set(threadAlias, true);
				tracef("(thr:%d) Contention limit exceeding detected", threadAlias);
			}
		}
		if(!_recursionDeepnessReached.get(threadAlias)) {
			const bool  recursionDeepnessExceeded = _recursionThreadCounter.get(threadAlias) > recursionLimit;
			if (recursionDeepnessExceeded) {
				_recursionDeepnessReached.set(threadAlias,true);
			}
		}
	}

	bool is_enContentionLogging() const {
		// thread independent
		return _contentionLogging;
	}
	bool is_contentionLimitExceeded(const int& threadAlias) {
		return _contentionLimitReached.get(threadAlias);
	}
	bool is_recursionDeepnessExceeded(const int &threadAlias) const {
		return _recursionDeepnessReached.get(threadAlias);
	}

	// per Thread
	void incRecursion(const int& threadAlias) {
		_recursionThreadCounter.inc(threadAlias);
	}
	void decRecursion(const int& threadAlias) {
		_recursionThreadCounter.dec(threadAlias);
	}
	int getRecursionCounter(const int& threadAlias) {
		return _recursionThreadCounter.get(threadAlias);
	}
	void increaseLoopCounter(DebugDataPerLoop &perLoopData) {
		perLoopData.increaseLoopCounter();
	}
	/**
	 * global
	 */
	void setDebugLogging(const bool value) {
		_contentionLogging = value;
	}
};

/**
 * this 'while' wrapper has the same syntax as the normal
 * 'while' argument, but it decreases the internal contentionCounter
 * every time a new loop cycle is started.
 * After the predefined contentionCounter value of repititions it throws
 * a runtime_error
 *
 * @note to use it as a function, move the looping Part inside a method
 * 		and pass this method to the contention_while
 *
 * @pre class in which it is used has to provide the variable
 * 		a (GlobalDebugManager)_globalDebugManager
 */
#define CONTENTION_WHILE_COUNTER(while_arg)\
	const int tmpThreadAlias = fi.getThisThreadAlias(); \
    std::stringstream tmpOutt, tmpOutt2; \
	tmpOutt << __func__; tmpOutt << ": endless looping detected, Thread:" << tmpThreadAlias; \
    std::string loopErrStr(tmpOutt.str()); \
	tmpOutt2 << __func__; tmpOutt2 << ": endless recursion detected, Thread:" << tmpThreadAlias; \
    std::string recursionErrStr(tmpOutt2.str()); \
    \
	DebugDataPerLoop perLoopDebugData; \
	auto contention_while_arg = [&](const bool default_bool_arg) { \
		if(_globalDebugManager.is_contentionLimitExceeded(tmpThreadAlias)) { \
            tracef(loopErrStr.c_str()); throw std::runtime_error(loopErrStr); }\
	    if(_globalDebugManager.is_recursionDeepnessExceeded(tmpThreadAlias)) { \
            tracef(recursionErrStr.c_str()); throw std::runtime_error(recursionErrStr); }\
	    \
        _globalDebugManager.increaseLoopCounter(perLoopDebugData); \
	    _globalDebugManager.detectContention(tmpThreadAlias, perLoopDebugData); \
		\
		return default_bool_arg;\
	};\
	while(contention_while_arg(while_arg))

#define CONTENTION_WHILE(while_arg)\
	std::stringstream tmpOutt; \
	tmpOutt << __func__; \
	tmpOutt << ": endless looping detected, Thread:" << fi.getThisThreadAlias(); \
    std::string outt(tmpOutt.str()); \
	auto _startTime = std::chrono::steady_clock::now(); \
    const int contentionLimitMS = 300; \
    \
	bool contentionDetected = false;\
	auto contention_while_arg = [&](const bool default_bool_arg) {\
		if(contentionDetected) { \
            tracef(outt.c_str()); throw std::runtime_error(outt); }\
		\
	    const auto _deltaFromStart = std::chrono::steady_clock::now() - _startTime;\
	    contentionDetected = _deltaFromStart > std::chrono::milliseconds(contentionLimitMS);\
		return default_bool_arg;\
	};\
	while(contention_while_arg(while_arg))

class LockFreeBSTHelper {
protected:
	/**
	 * @var[in] srcNodeID is for debugging/mocking reasons only
	 * @var[in] position  is for debugging/mocking reasons only
	 * @return true if the cas has been successful, false otherwise
	 */
	virtual bool cas(std::atomic<FlagNodePtr<ANode_t>>& casObject,
			FlagNodePtr<ANode_t> expected, FlagNodePtr<ANode_t> desired,
			const IDType srcNodeID, const CASUId& position);
	virtual bool cas(std::atomic<ANodePtr>& casObject,
			ANodePtr expected, ANodePtr desired,
			const IDType srcNodeID, const CASUId& position);

	//debug data
	GlobalDebugManager _globalDebugManager;
public:
	virtual ~LockFreeBSTHelper() {};
	FaultProducer fi; //!< Fault injector

	/**
	 * LOCATE is used to perform a predecessor query using
	 * a given key k to locate an interval [k_i, k_j],
	 * s.t. either {k_i <= k < k_j} or {k_i >= k > k_j}
	 * where x(k_i) and x(k_j) are two nodes in the BST.
	 *
	 * The function LOCATE is used for that, which starts
	 * from a specified set of two consecutive nodes
	 * {#prev, #curr}.
	 * @inv  CONDITION1
	 * @return {0,1,2} depending on whether k is less
	 * 			than, greater than or equal to curr->k.
	 */
	cmpRC_e locate(ANodePtr& prev, ANodePtr& curr, const EpsilonedIDType k);
	cmpRC_e locateConditionChecking(ANodePtr& prev, ANodePtr& curr, const IDType k);

	/**
	 * Tries to flag a desired link.
	 * The desired is the one going from prev -> curr, where
	 * 		suc(x_prev)(dir) is curr
	 * It is the child of prev which is chosen by (curr->k == prev->k)
	 * if the flagging fails at the first time, TODO
	 *
	 * @inv isThread has to be set correctly, if endless looping is possible
	 * @var[in] isThread flag whether the link is supposed to be threaded or not
	 */
	bool tryFlag(ANodePtr& prev, ANodePtr& curr, ANodePtr& back, bool isThread);

	void _cleanFlag_threaded(ANodePtr& prev, ANodePtr& curr, ANodePtr& back);
	void _cleanFlag_unThreaded(ANodePtr& prev, ANodePtr& curr, ANodePtr& back);
	/**
	 * unThreaded cleanFlag also INCLUDES the final swapping of pointers
	 *   for cat1 & cat2 nodes
	 *
	 * @note is also used to clean the flag of the parent-link of a node,
	 * 		therefor a boolean variable is passed to it to inform about
	 * 		the thread-bit of the lfagged link. (@see p.326, sec.3.2.2)
	 * @note also marks the right-link ??
	 * @var[in] isThread==false then prev must be the
	 * 			incoming parent-link (@see p.326, sec.3.2.2) of a node under REMOVE
	 *
	 * 			if true it cleans a flagged order-link, if false it cleans a flagged
	 * 			parent-link
	 */
	void cleanFlag(ANodePtr& prev, ANodePtr& curr, ANodePtr& back, bool isThread);

	/**
	 * Tries to set the mark Flag on the child link
	 * of curr->selectLRChild(dir)
	 */
	bool tryMark(ANodePtr& curr, cmpRC_e dir, ANodePtr firstNextMatcher = nullptr);

	void _cleanMark_right(ANodePtr &curr, FlagNodePtr<ANode_t> &nonAtomicLeftChild);
	void _cleanMark_otherwise(ANodePtr &curr, FlagNodePtr<ANode_t> &nonAtomicLeftChild,
                                  FlagNodePtr<ANode_t> &nonAtomicRightChild);
	void cleanMark(ANodePtr& curr, const cmpRC_e& markDir);
};



#endif /* LOCKFREEBSTHELPER_H_ */
