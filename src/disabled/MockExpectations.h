#ifndef EFFICIENT_LF_BST_MOCKEXPECTATIONS_H
#define EFFICIENT_LF_BST_MOCKEXPECTATIONS_H

#include "MockedLockFreeBST.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

testing::ExpectationSet expect_addNode(MockedLockFreeBST& bst,
                                       const int backLinkK,
                                       const int newK,
                                       const int rightDesired);

testing::ExpectationSet expect_rmMark_ItoIII(MockedLockFreeBST& bst,
                                             const int rmK,
                                             const int orderNodeK,
                                             const int preLinkTargetK,
                                             const int rightK);

testing::ExpectationSet
        expect_rmCat2(MockedLockFreeBST& bst,
                      const int kToBeRemoved,
                      const int kOrderNode = 1,
                      const bool kHasRightChild = true,
                      const int kRightChild = 3,
                      const int kBackLink = 6)

testing::ExpectationSet expect_rmMark_IV(MockedLockFreeBST& bst,
                                         const int kPreNode,
                                         const int kPreNodesParent);

testing::ExpectationSet expect_rmMark_VItoVII(MockedLockFreeBST& bst,
                                              const int kToBeRemoved,
                                              const int kLeftLinkOfRemove,
                                              const int kPredecessor,
                                              const int kLeftLinkOfPredecessor);

testing::ExpectationSet expect_rmCat3(MockedLockFreeBST& bst,
                                      const int kPreParent,	// delete->preLink->BackLink
                                      const int kPre,			// delete->preLink
                                      const int kPreLeft,
                                      const int kDelete,		//delNode
                                      const int kDeleteParent,//delNode->backLink
                                      const int kLeft,		//left of delNode
                                      const int kRight		//right of delNode
);


#endif //EFFICIENT_LF_BST_MOCKEXPECTATIONS_H
