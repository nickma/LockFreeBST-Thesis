//
// Created by nickma on 11.03.16.
//

#include "MockExpectations.h"

#include "MockMatcher.h"
#include "gtest/gtest.h"

const auto pos_inf = std::numeric_limits<int>::max();
const auto neg_inf = std::numeric_limits<int>::min();

