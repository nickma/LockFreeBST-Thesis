#ifndef FLAGNODEPTR_H_
#define FLAGNODEPTR_H_

#include <stdint.h>
#include <cstdbool>
#include <cstddef>

/**
 * FlagNodePtr is intended to provide an easy access to a manipulable
 * pointer.
 * Manipulable in a way s.t. the upper three bit of an 64bit pointer
 * are used for boolean flag injection (such as: flag, mark, thread).
 * One is able to obtain the original pointer by calling get().
 *
 * @pre 64bit architecture AND compiled with -m64
 */
template<typename T>
class FlagNodePtr {
public:
	/**
	 * Bit position indicator
	 */
	enum flagID : size_t {
		flag	= 61,
		mark	= 62,
		// during a link update; copy of the one being updated [p.324 right up]
		thread	= 63
	};

	FlagNodePtr() noexcept {};
	FlagNodePtr(T* rawPointer, bool flag, bool mark, bool thread) noexcept;

	/**
	 * resets the pointer to the given value and all flags to false
	 */
	void reset(T* rawPointer);
	void reset(T* rawPointer, bool flag, bool mark, bool thread);

	/**
	 * returns the clean pointer
	 */
	T* get() const;
	//TODO implement the operator->(..) and operator*(..) methods

	/**
	 * flag getter
	 */
	bool isFlagged() const;
	bool isMarked() const;
	bool isThreaded() const;

	/**
	 * compare operator
	 * @return returns true if lhs and rhs are equivalent, false otherwise
	 */
	friend bool operator==(const FlagNodePtr<T>& lhs,
			const FlagNodePtr<T>& rhs)
	{
		return (lhs._flaggedPtr == rhs._flaggedPtr);
	}

	friend std::ostream& operator<<(std::ostream& inout, const FlagNodePtr& v) {
		auto aNodePtr = v.get();
		const std::string isFlagged = v.isFlagged() ?"F":"0";
		const std::string isMarked  = v.isMarked()  ?"M":"0";
		const std::string isThreaded= v.isThreaded()?"T":"0";
		inout << aNodePtr->k << ";" << isFlagged << isMarked << isThreaded;
		return inout;
	}
private:
	static constexpr uint64_t upperThreeBits_mask =
			(uint64_t)0xE0 << (sizeof(void*)*8-8);
	static constexpr uint64_t beforeLastThreBits_mask =
			(uint64_t)0x1C << (sizeof(void*)*8-8);

	/**
	 * flag encoded pointer type T
	 */
	T* _flaggedPtr {nullptr};
};

/**
 * This is the class implementation inclusion
 * @see http://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file/495056#495056
 */
#include "FlagNodePtr.hpp"

#endif /* FLAGNODEPTR_H_ */
