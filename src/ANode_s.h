#ifndef BST_NODE_H_
#define BST_NODE_H_

#include <atomic>
#include <cstdint>
#include <stdexcept>
#include <string>

#include "FlagNodePtr.h"
#include "IDType.h"

typedef struct ANode_s ANode_t;
typedef ANode_t* ANodePtr;		//TODO make it a parent of FlagNodePtr<>
/**
 * This is the atomic abstraction of a BST node
 *
 * - Access pointer of std::atomic<FlagNodePtr<ANode_t>> via .load().get()
 * - Access pointer of std::atomic<ANode_t*> via .load()
 *
 * @usage new ANode_t{k, {nullptr,0,0,1}, {nullptr,0,0,1}, &root[1], nullptr}
 * Ad initializing:
 * @see http://stackoverflow.com/questions/21142542/initializing-a-class-or-struct-with-an-atomic-array
 */
struct ANode_s {
	IDType k;
	std::atomic<FlagNodePtr<ANode_t>> leftChild;
	std::atomic<FlagNodePtr<ANode_t>> rightChild;
	std::atomic<ANodePtr> backLink; //!< always points to the element one ahead
	std::atomic<ANodePtr> preLink;  //!< only during remove procedure points to the logical predecessor

	ANode_s(IDType k,
			FlagNodePtr<ANode_t> leftCh, FlagNodePtr<ANode_t> rightCh,
			ANodePtr backLink, ANodePtr preLink) noexcept;
	ANode_s(const ANode_s&) = delete;

	/**
	 * @return leftChild on dir==0; rightChild otherwise
	 */
	std::atomic<FlagNodePtr<ANode_t>>& selectLRChild(const cmpRC_e dir);

	/**
	 * @return the nodes category as integer in the range of one to three
	 */
	int getNodeCategory();

	friend std::ostream& operator<<(std::ostream& inout, const ANode_s& v);
};
extern std::ostream& operator<<(std::ostream& inout, const ANode_s& v);

typedef struct node_s node_t;
/**
 * "structure consisting of five memory-words" [Efficient Lock-free BST, p.322]
 * @ref https://en.wikipedia.org/wiki/Typedef#Using_typedef_with_structure_pointers
 */
struct node_s {
	IDType k;
	//Three booleans overlap with three unused bits of the pointer ref
	// child[0] == left, child[1] == right
	FlagNodePtr<node_s> leftChild;
	FlagNodePtr<node_s> rightChild;
	node_t *backLink;
	node_t *preLink;

	/**
 	 * @return leftChild on dir==0; rightChild otherwise
 	 */
	FlagNodePtr<node_s> &selectLRChild(const uint8_t dir) {
		return (dir == 0) ? leftChild : rightChild;
	}
};

#endif /* BST_NODE_H_ */
