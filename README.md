This is an implementation of the Algorithm proposed in the following paper: <br/>
Bapi Chatterjee, Nhan Nguyen, and Philippas Tsigas. Efficient Lock-free Bi-
nary Search Trees. In Proceedings of the 2014 ACM Symposium on Principles
of Distributed Computing (PODC), pages 322–331. ACM, 2014.

The status is that the _Remove_ operation does not work properly.

## Setup
**PreRequirements**
Debian package:
libcurl4-openssl-dev cmakebcurl3 python3-babeltrace

## Build
mkdir build; cd build <br/>
cmake ../ <br/>
make VERBOSE=1

### Change build options
After issuing "cmake ../" you are able to alter
build options in the file ./build/CMakeCache.txt

Main options for this project are:
INCLUDE_GMOCK:BOOL and INCLUDE_LTTNG_UST:BOOL to enable the build
of these separate executables.